/**
 * Thrift DAPI Protocol
 */

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

using Thrift.Transport;
using System.Globalization;

namespace Thrift.Protocol
{
	/// <summary>
	/// Thrift DAPI Protocol
	/// </summary>
	public class TDAPIProtocol : TJSONProtocol
	{
		private new const double VERSION = 1.2;

		private bool startedParse = false;
		private bool startedWrite = false;
		private bool startedDAPIRequest = false;
		private bool startedArguments = false;
		public object appToken = null;
		public object userToken = null;
		public string stringToken = null;

		// Stack of field names to keep track of when parsing
		protected Stack<string> currField = new Stack<string>();

		private static Dictionary<string, string> DAPIAliases;

		private static byte[] BOOL_VAL = new byte[] { (byte)'t', (byte)'f', (byte)'T', (byte)'F' };
		private static byte[] BOOL_END = new byte[] { (byte)'e', (byte)'E' };

		/// <summary>
		/// TDAPIProtocol Constructor
		/// </summary>
		public TDAPIProtocol(TTransport trans) : base(trans)
		{
			DAPIAliases = new Dictionary<string, string>
			{
				{ "payload", "p" },
				{ "version", "v" },
				{ "calls", "c" },
				{ "token", "t" },
				{ "method", "m" },
				{ "argz", "al" }, // note that 'args', represented by 'argz' in struct
				{ "name", "n" },
				{ "appId", "a" },
				{ "snId", "n" },
				{ "userId", "u" },
				{ "session", "s" },
				{ "secret", "as" },
				{ "userToken", "t" },
				{ "appToken", "t" },
				{ "stringToken", "t" }
			};
		}

		public override TMessage ReadMessageBegin()
		{
			this.startedParse = false;
			TMessage message = new TMessage();
			message.Name = null;
			message.Type = 0;
			message.SeqID = 0;
			return message;
		}

		public override void ReadMessageEnd()
		{
			return;
		}

		public override TStruct ReadStructBegin()
		{
			if (!this.startedParse)
			{
				return new TStruct();
			}
			ReadJSONObjectStart();
			return new TStruct();
		}

		public override void ReadStructEnd()
		{
			if (!this.startedParse)
			{
				return;
			}
			bool atEndOfStream = isAtEndOfStream(reader);
			if (atEndOfStream)
			{
				return;
			}
			ReadJSONObjectEnd();
		}

		public override TField ReadFieldBegin()
		{
			TField field = new TField();
			if (!this.startedParse)
			{
				this.startedParse = true;
				field.Name = null;
				field.Type = TType.Struct;
				field.ID = 0;
				return field;
			}

			byte ch = 0;
			bool endOfStream = isAtEndOfStream(reader);
			if (!endOfStream)
			{
				ch = reader.Peek();
			}

			if (endOfStream || ch == RBRACE[0] || ch == RBRACKET[0])
			{
				field.Name = null;
				field.Type = TType.Stop;
				field.ID = 0;
				return field;
			}

			byte[] fnameBuf = ReadJSONString(false);
			string fname = utf8Encoding.GetString(fnameBuf, 0, fnameBuf.Length);
			short fid = -1;
			TType ftype = TType.Stop;

			if (fname == "calls" || fname == "c")
			{
				ftype = TType.List;
				fid = 1;
			}
			else if (fname == "error")
			{
				if (this.contextStack.Count == 1)
				{
					fid = 2;
				}
				else if (this.contextStack.Count == 3)
				{
					fid = 3;
				}
				else
				{
					throw new TProtocolException(TProtocolException.INVALID_DATA, "error field not in expected context");
				}
				ftype = TType.Struct;
			}
			else if (fname == "token" || fname == "t")
			{
				ftype = TType.String;
				fid = 3;
			}
			else if (fname == "zid")
			{
				ftype = TType.String;
				fid = 4;
			}
			else if (fname == "name" || fname == "n")
			{
				ftype = TType.String;
				fid = 1;
			}
			else if (fname == "data")
			{
				if (this.contextStack.Count == 3)
				{
					fid = 2;
				}
				else if (this.contextStack.Count == 4)
				{
					fid = 3;
				}
				else if (this.contextStack.Count == 2)
				{
					fid = 3;
				}
				else
				{
					throw new TProtocolException(TProtocolException.INVALID_DATA, "data field not in expected context");
				}
				ftype = TType.String;
			}
			else if (fname == "message")
			{
				ftype = TType.String;
				fid = 1;
			}
			else if (fname == "type")
			{
				ftype = TType.String;
				fid = 2;
			}
			else
			{
				throw new TProtocolException(TProtocolException.INVALID_DATA, "field " + fname + " not supported for readFieldBegin");
			}
			this.currField.Push(fname);

			field.Name = fname;
			field.Type = ftype;
			field.ID = fid;
			return field;
		}

		public override void ReadFieldEnd()
		{
			if (this.currField.Count == 0)
			{
				return;
			}

			string fname = this.currField.Pop();
			if (fname == "calls" || fname == "c")
			{
				return;
			}
			else if (fname == "error")
			{
				return;
			}
			else if (fname == "token" || fname == "t")
			{
				return;
			}
			else if (fname == "zid")
			{
				return;
			}
			else if (fname == "name" || fname == "n")
			{
				return;
			}
			else if (fname == "data")
			{
				return;
			}
			else if (fname == "message")
			{
				return;
			}
			else if (fname == "type")
			{
				return;
			}
			else
			{
				throw new TProtocolException(TProtocolException.INVALID_DATA, "field " + fname + " not supported for readFieldEnd");
			}
		}

		public override TList ReadListBegin()
		{
			TList list = new TList();
			ReadJSONArrayStart();
			list.ElementType = TType.Struct;
			list.Count = 1; // hack for now
			return list;
		}

		public override void ReadListEnd()
		{
			ReadJSONArrayEnd();
		}

		public override TSet ReadSetBegin()
		{
			TSet set = new TSet();
			ReadJSONArrayStart();
			set.ElementType = TType.Struct;
			set.Count = 1; // hack for now
			return set;
		}

		public override void ReadSetEnd()
		{
			ReadJSONArrayEnd();
		}

		public override string ReadString()
		{
			context.Read();
			byte ch = reader.Peek();
			if (ch == LBRACE[0])
			{
				return ConsumeObject();
			}
			else if (ch == LBRACKET[0])
			{
				return ConsumeArray();
			}
			else if (ch == BOOL_VAL[0] || ch == BOOL_VAL[2])
			{
				return ConsumeBool();
			}
			else if (ch == BOOL_VAL[1] || ch == BOOL_VAL[3])
			{
				return ConsumeBool();
			}

			var buf = ReadJSONString(true);
			return utf8Encoding.GetString(buf, 0, buf.Length);
		}

		private string ConsumeObject()
		{
			MemoryStream str_list = new MemoryStream();
			byte currChar = reader.Read();
			Stack<byte> charStack = new Stack<byte>();
			charStack.Push(currChar);
			str_list.Write(new byte[] { (byte)currChar }, 0, 1);
			while (charStack.Count > 0)
			{
				currChar = reader.Read();
				str_list.Write(new byte[] { (byte)currChar }, 0, 1);
				if (currChar == RBRACE[0])
				{
					charStack.Pop();
				}
				else if (currChar == LBRACE[0])
				{
					charStack.Push(currChar);
				}
			}
			var buf = str_list.ToArray();
			return utf8Encoding.GetString(buf, 0, buf.Length);
		}

		private string ConsumeArray()
		{
			MemoryStream str_list = new MemoryStream();
			byte currChar = reader.Read();
			Stack<byte> charStack = new Stack<byte>();
			charStack.Push(currChar);
			while (charStack.Count > 0)
			{
				currChar = reader.Read();
				str_list.Write(new byte[] { (byte)currChar }, 0, 1);
				if (currChar == RBRACE[0])
				{
					charStack.Pop();
				}
				else if (currChar == LBRACE[0])
				{
					charStack.Push(currChar);
				}
			}
			var buf = str_list.ToArray();
			return utf8Encoding.GetString(buf, 0, buf.Length);
		}

		private string ConsumeBool()
		{
			MemoryStream str_list = new MemoryStream();
			byte currChar = 0;

			while (currChar != BOOL_END[0] && currChar != BOOL_END[1])
			{
				currChar = reader.Read();
				str_list.Write(new byte[] { (byte)currChar }, 0, 1);
			}
			var buf = str_list.ToArray();
			return utf8Encoding.GetString(buf, 0, buf.Length);
		}

		public override void WriteMessageBegin(TMessage message)
		{
			WriteBasicString("v=");
			WriteBasicNumber(VERSION);
			WriteBasicString("&p=");

			// short-circuit the pseudo-DAPI wrapper if performing a raw DAPI call
			if (message.ServiceName == "DAPI")
			{
				this.startedDAPIRequest = true;
				return;
			}

			// Since most components in the DAPI protocol are
			// not ordered, just pass in basic data
			TField field = new TField();
			field.Type = TType.Void;
			field.ID = -1;
			TStruct tstruct = new TStruct();

			this.startedWrite = true;
			this.WriteStructBegin(tstruct);
			// DAPIRequest {
			//   userToken : { ... },
			if (this.userToken != null)
			{
				field.Name = "userToken";
				this.WriteFieldBegin(field);
				//TODO: Map out the Dictionary<string, object> in the userToken field of this class.
				this.WriteFieldEnd();
			}
			//   appToken : { ... },
			if (this.appToken != null)
			{
				field.Name = "appToken";

				this.WriteFieldBegin(field);
				this.WriteStructBegin(tstruct);
				foreach (KeyValuePair<String,String> entry in this.appToken as Dictionary<string, string>)
				{
					field.Name = entry.Key;
					WriteFieldBegin(field);
					WriteString(entry.Value);
					WriteFieldEnd();
				}
				this.WriteStructEnd();
				this.WriteFieldEnd();
			}
			//   stringToken : "...",
			if (this.stringToken != null)
			{
				field.Name = "stringToken";
				this.WriteFieldBegin(field);
				this.WriteString(this.stringToken);
				this.WriteFieldEnd();
			}

			//    calls : [ ...
			field.Name = "calls";
			this.WriteFieldBegin(field);

			TList list = new TList();
			this.WriteListBegin(list);

			//        { 'method' : name,
			this.WriteStructBegin(tstruct);
			field.Name = "method";
			this.WriteFieldBegin(field);
			this.WriteString(message.ServiceName.ToLower() + "." + message.Name);
			this.WriteFieldEnd();

			//          'args', { ...
			field.Name = "argz";
			this.WriteFieldBegin(field);
			this.startedArguments = true;
		}

		public override void WriteMessageEnd()
		{
			// short-circuit the message end if performing a raw DAPI call
			if (this.startedDAPIRequest == true)
			{
				this.startedDAPIRequest = false;
				return;
			}

			//       ...}
			this.WriteFieldEnd();

			//   ...} ]
			this.WriteStructEnd();
			this.WriteListEnd();
			this.WriteFieldEnd();

			// } // end DAPIRequest
			this.WriteStructEnd();
			this.startedArguments = false;
		}

		public override void WriteStructBegin(TStruct str)
		{
			if (!this.startedWrite)
			{
				return;
			}
			WriteJSONObjectStart();
		}

		public override void WriteStructEnd()
		{
			if (!this.startedWrite)
			{
				return;
			}
			if (this.contextStack.Count == 0)
			{
				return;
			}
			WriteJSONObjectEnd();
		}

		public override void WriteFieldBegin(TField field)
		{
			if (!this.startedWrite)
			{
				this.startedWrite = true;
				return;
			}
			string fname = field.Name;
			if (!this.startedArguments && DAPIAliases.ContainsKey(fname))
			{
				fname = DAPIAliases[fname];
			}
			WriteString(fname);
		}

		public override void WriteFieldEnd()
		{
			return;
		}

		public override void WriteMapBegin(TMap map)
		{
			WriteJSONObjectStart();
		}

		public override void WriteMapEnd()
		{
			WriteJSONObjectEnd();
		}

		public override void WriteListBegin(TList list)
		{
			WriteJSONArrayStart();
		}

		public override void WriteListEnd()
		{
			WriteJSONArrayEnd();
		}

		public override void WriteSetBegin(TSet set)
		{
			WriteJSONArrayStart();
		}

		public override void WriteSetEnd()
		{
			WriteJSONArrayEnd();
		}

		private void WriteBasicString(string str)
		{
			byte[] b = utf8Encoding.GetBytes(str);
			this.trans.Write(b);
		}

		private void WriteBasicNumber(double number)
		{
			string str = number.ToString(CultureInfo.InvariantCulture);
			byte[] b = utf8Encoding.GetBytes(str);
			this.trans.Write(b);
		}

		private bool isAtEndOfStream(LookaheadReader reader)
		{
			bool result = false;
			try
			{
				reader.Peek();
			}
			catch (TTransportException e)
			{
				if (e.Type == TTransportException.ExceptionType.EndOfFile)
				{
					//Console.WriteLine("Reached End of Stream");
					result = true;
				}
				else
				{
					throw e;
				}
			}
			return result;
		}

		/// <summary>
		/// Factory for DAPI protocol objects
		/// </summary>
		public new class Factory : TProtocolFactory
		{
			public TProtocol GetProtocol(TTransport trans)
			{
				return new TDAPIProtocol(trans);
			}
		}
	}

	/// <summary>
	/// Simple DAPI protocol implementation for thrift.
	/// </summary>
	public class TSimpleProtocol : TProtocol
	{
		private const double VERSION = 1.2;
		private bool finishedRead = false;

		// Default encoding
		protected Encoding utf8Encoding = UTF8Encoding.UTF8;

		public TSimpleProtocol(TTransport trans) : base(trans)
		{

		}

		public override TMessage ReadMessageBegin()
		{
			TMessage message = new TMessage();
			message.Name = null;
			message.Type = 0;
			message.SeqID = 0;
			return message;
		}

		public override String ReadString()
		{
			MemoryStream buffer = new MemoryStream();
			byte[] data = new byte[1];
			try 
			{
				while (true)
				{
					this.trans.ReadAll(data, 0, 1);
					byte ch = data[0];
					buffer.Write(new byte[] { (byte)ch }, 0, 1);
				}
			}
			catch (TTransportException e)
			{
				if (e.Type == TTransportException.ExceptionType.EndOfFile)
				{
					//Console.WriteLine("Reached End of Stream");
				}
				else
				{
					throw e;
				}
			}
			
			var buf = buffer.ToArray();

			this.finishedRead = true;
			return utf8Encoding.GetString(buf, 0, buf.Length);
		}

		public override TField ReadFieldBegin()
		{
			TField field = new TField();
			TType ttype = TType.Stop;
			if (!this.finishedRead)
			{
				ttype = TType.String;
			}
			field.Name = null;
			field.Type = ttype;
			field.ID = 0;
			return field;
		}

		public override void WriteMessageBegin(TMessage message)
		{
			WriteString("v=");
			WriteNumber(VERSION);
			WriteString("&p=");
		}

		public override void WriteString(string str)
		{
			byte[] b = utf8Encoding.GetBytes(str);
			this.trans.Write(b);
		}

		private void WriteNumber(double number)
		{
			string str = number.ToString(CultureInfo.InvariantCulture);
			byte[] b = utf8Encoding.GetBytes(str);
			this.trans.Write(b);
		}

		public override void WriteMessageEnd() {}
		public override void WriteStructBegin(TStruct struc) {}
		public override void WriteStructEnd() {}
		public override void WriteFieldBegin(TField field) {}
		public override void WriteFieldEnd() {}
		public override void WriteFieldStop() {}
		public override void WriteMapBegin(TMap map) {}
		public override void WriteMapEnd() {}
		public override void WriteListBegin(TList list) {}
		public override void WriteListEnd() {}
		public override void WriteSetBegin(TSet set) {}
		public override void WriteSetEnd() {}
		public override void WriteBool(bool b) {}
		public override void WriteByte(byte b) {}
		public override void WriteI16(short i16) {}
		public override void WriteI32(int i32) {}
		public override void WriteI64(long i64) {}
		public override void WriteDouble(double d) {}
		public override void WriteBinary(byte[] b) {}

		public override void ReadMessageEnd() {}
		public override TStruct ReadStructBegin() { return new TStruct(); }
		public override void ReadStructEnd() {}
		public override void ReadFieldEnd() {}
		public override TMap ReadMapBegin() { return new TMap(); }
		public override void ReadMapEnd() {}
		public override TList ReadListBegin() { return new TList(); }
		public override void ReadListEnd() {}
		public override TSet ReadSetBegin() { return new TSet(); }
		public override void ReadSetEnd() {}
		public override bool ReadBool() { return false; }
		public override byte ReadByte() { return 0; }
		public override short ReadI16() { return 0; }
		public override int ReadI32() { return 0; }
		public override long ReadI64() { return 0; }
		public override double ReadDouble() { return 0.0; }
		public override byte[] ReadBinary() { return null; }

		/// <summary>
		/// Factory for Simple DAPI protocol objects
		/// </summary>
		public class Factory : TProtocolFactory
		{
			public TProtocol GetProtocol(TTransport trans)
			{
				return new TSimpleProtocol(trans);
			}
		}
	}
}
