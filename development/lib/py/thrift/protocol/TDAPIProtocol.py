#
# Thrift DAPI Protocol
#

from TProtocol import TType

from TJSONProtocol import TProtocolBase, TJSONProtocol, LBRACE, RBRACE, LBRACKET, RBRACKET
 
__all__ = ['TDAPIProtocol',
           'TDAPIProtocolFactory']

#
# Thrift DAPI Protocol
#
class TDAPIProtocol(TJSONProtocol):

  VERSION = 1.2

  startedParse = False
  startedWrite = False
  startedDAPIRequest = False
  startedArguments = False
  appToken = None
  userToken = None
  stringToken = None

  # Stack of field names to keep track of when parsing
  currField = []

  DAPIAliases = {}

  def __init__(self, protocol):
    TJSONProtocol.__init__(self, protocol)

    self.DAPIAliases['payload'] = 'p'
    self.DAPIAliases['version'] = 'v'
    self.DAPIAliases['calls'] = 'c'
    self.DAPIAliases['token'] = 't'
    self.DAPIAliases['method'] = 'm'
    self.DAPIAliases['argz'] = 'al' # note that 'args', represented by 'argz' in struct
    self.DAPIAliases['name'] = 'n'
    self.DAPIAliases['appId'] = 'a'
    self.DAPIAliases['snId'] = 'n'
    self.DAPIAliases['userId'] = 'u'
    self.DAPIAliases['session'] = 's'
    self.DAPIAliases['secret'] = 'as'
    self.DAPIAliases['userToken'] = 't'
    self.DAPIAliases['appToken'] = 't'
    self.DAPIAliases['stringToken'] = 't'

  def readMessageBegin(self):
    self.resetReadContext()
    self.startedParse = False
    name = ''
    typen = 0
    seqid = 0
    return (name, typen, seqid)

  def readMessageEnd(self):
    pass

  def readStructBegin(self):
    if not self.startedParse:
      return
    self.readJSONObjectStart()

  def readStructEnd(self):
    if not self.startedParse:
      return
    atEndOfStream = self.isAtEndOfStream()
    if atEndOfStream:
      return
    self.readJSONObjectEnd()

  def readFieldBegin(self):
    if not self.startedParse:
      self.startedParse = True
      return (None, TType.STRUCT, 0)

    character = None
    atEndOfStream = self.isAtEndOfStream()
    if not atEndOfStream:
      character = self.reader.peek()

    if atEndOfStream or character == RBRACE or character == RBRACKET:
      return (None, TType.STOP, 0)
    
    fname = self.readJSONString(False)
    fid = -1
    ftype = TType.STOP

    if fname == 'calls' or fname == 'c':
      ftype, fid = TType.LIST, 1
    elif fname == 'error':
      if len(self.contextStack) == 2:
        fid = 2
      elif len(self.contextStack) == 4:
        fid = 3
      else:
        raise Exception('error field not in expected context')
      ftype = TType.STRUCT
    elif fname == 'token' or fname == 't':
      ftype, fid = TType.STRING, 3
    elif fname == 'zid':
      ftype, fid = TType.STRING, 4
    elif fname == 'name' or fname == 'n':
      ftype, fid = TType.STRING, 1
    elif fname == 'data':
      if len(self.contextStack) == 4:
        fid = 2
      elif len(self.contextStack) == 5:
        fid = 3
      elif len(self.contextStack) == 3:
        fid = 3
      else:
        raise Exception('data field not in expected context')
      ftype = TType.STRING
    elif fname == 'message':
      ftype, fid = TType.STRING, 1
    elif fname == 'type':
      ftype, fid = TType.STRING, 2
    else:
      raise Exception('field ' + fname + ' not supported for readFieldBegin')
    self.currField.append(fname)

    return (fname, ftype, fid)

  def readFieldEnd(self):
    if len(self.currField) == 0:
      return
    fname = self.currField.pop()
    if fname == 'calls' or fname == 'c':
      return
    elif fname == 'error':
      return
    elif fname == 'token' or fname == 't':
      return
    elif fname == 'zid':
      return
    elif fname == 'name' or fname == 'n':
      return
    elif fname == 'data':
      return
    elif fname == 'message':
      return
    elif fname == 'type':
      return
    else:
      raise Exception('field ' + fname + ' not supported for readFieldEnd')

  def readCollectionBegin(self):
    self.readJSONArrayStart()
    elemType = TType.STRUCT
    size = 1 # hack for now
    return (elemType, size)
  readListBegin = readCollectionBegin
  readSetBegin = readCollectionBegin

  def readCollectionEnd(self):
    self.readJSONArrayEnd()
  readSetEnd = readCollectionEnd
  readListEnd = readCollectionEnd

  def readString(self):
    self.context.read()
    character = self.reader.peek()
    if character == LBRACE:
      return self.consumeObject()
    elif character == LBRACKET:
      return self.consumeArray()
    elif character == 't' or character == 'T':
      return self.consumeBool()
    elif character == 'f' or character == 'F':
      return self.consumeBool()

    return self.readJSONString(True)

  def consumeObject(self):
    str_list = []
    currChar = self.reader.read()
    charStack = [currChar]
    str_list.append(currChar)
    while (len(charStack) > 0):
      currChar = self.reader.read()
      str_list.append(currChar)
      if currChar == RBRACE:
        charStack.pop()
      elif currChar == LBRACE:
        charStack.append(currChar)
    return ''.join(str_list)

  def consumeArray(self):
    str_list = []
    currChar = self.reader.read()
    charStack = [currChar]
    str_list.append(currChar)
    while (len(charStack) > 0):
      currChar = self.reader.read()
      str_list.append(currChar)
      if currChar == RBRACKET:
        charStack.pop()
      elif currChar == LBRACKET:
        charStack.append(currChar)
    return ''.join(str_list)

  def consumeBool(self):
    str_list = []
    currChar = ''

    while (currChar != 'e' and currChar != 'E'):
      currChar = self.reader.read()
      str_list.append(currChar)
    return ''.join(str_list)

  def writeMessageBegin(self, name, request_type, seqid, service_name):
    self.writeBasicString('v=')
    self.writeBasicNumber(self.VERSION)
    self.writeBasicString('&p=')
    
    ## short-circuit the pseudo-DAPI wrapper if performing a raw DAPI call 
    if service_name == 'DAPI':
      self.startedDAPIRequest = True
      return

    ## Since most components in the DAPI protocol are
    ## not ordered, just pass in basic data
    ttype = TType.VOID
    id = -1

    self.startedWrite = True
    self.writeStructBegin('DAPIRequest')
    ## DAPIRequest {
    ##   userToken : { ... },
    if self.userToken is not None:
      self.writeFieldBegin('userToken', ttype, id)
      ## TODO: Map out the dict<string, object> in the userToken field of this class.
      self.writeFieldEnd()
    ##   appToken : { ... },
    if self.appToken is not None:
      self.writeFieldBegin('appToken', ttype, id)
      self.writeStructBegin(None);
      for key in self.appToken:
        value = self.appToken[key]
        self.writeFieldBegin(key, ttype, id)
        self.writeString(value)
        self.writeFieldEnd()
      self.writeStructEnd();
      self.writeFieldEnd()
    ##   stringToken : "...",
    if self.stringToken is not None:
      self.writeFieldBegin('stringToken', ttype, id)
      self.writeString(self.stringToken)
      self.writeFieldEnd()

    ##    calls : [ ...
    self.writeFieldBegin('calls', ttype, id)
    self.writeListBegin(ttype, id)
   
    ##        { 'method' : name, 
    self.writeStructBegin('CallRequest')
    self.writeFieldBegin('method', ttype, id)
    self.writeString('{0}.{1}'.format(service_name.lower(), name))
    self.writeFieldEnd() 

    ##          'args', { ... 
    self.writeFieldBegin('argz', TType.MAP, 2)
    self.startedArguments = True
    
  def writeMessageEnd(self):
  
    ## short-circuit the message end if performing a raw DAPI call 
    if self.startedDAPIRequest == True:
      self.startedDAPIRequest = False
      return
 
    ##       ...}
    self.writeFieldEnd()

    ##   ...} ]
    self.writeStructEnd()
    self.writeListEnd()
    self.writeFieldEnd()

    ## } // end DAPIRequest
    self.writeStructEnd()
    self.startedArguments = False

  def writeStructBegin(self, name):
    if not self.startedWrite:
      return
    self.writeJSONObjectStart()

  def writeStructEnd(self):
    if not self.startedWrite:
      return
    if len(self.contextStack) == 1:
      return
    self.writeJSONObjectEnd()

  def writeFieldBegin(self, name, ttype, id):
    if not self.startedWrite:
      self.startedWrite = True
      return

    fname = name
    if not self.startedArguments and name in self.DAPIAliases:
      fname = self.DAPIAliases[name]
    
    self.writeString(fname)

  def writeFieldEnd(self):
    pass

  def writeMapBegin(self, ktype, vtype, size):
    self.writeJSONObjectStart()

  def writeMapEnd(self):
    self.writeJSONObjectEnd()
    
  def writeListBegin(self, etype, size):
    self.writeJSONArrayStart()
    
  def writeListEnd(self):
    self.writeJSONArrayEnd()

  def writeSetBegin(self, etype, size):
    self.writeJSONArrayStart()
    
  def writeSetEnd(self):
    self.writeJSONArrayEnd()

  def writeBasicString(self, string):
    self.trans.write(string)

  def writeBasicNumber(self, number):
    num = str(number)
    self.trans.write(num)

  def isAtEndOfStream(self):
    result = False;
    character = self.reader.peek()
    if character == '':
      result = True
    return result

class TDAPIProtocolFactory:

  def getProtocol(self, trans):
    return TDAPIProtocol(trans)

#
# Simple DAPI protocol implementation for thrift.
#
class TSimpleProtocol(TProtocolBase):
  """ Reads string off the wire. """

  VERSION = 1.2
  finishedRead = False

  def readMessageBegin(self):
    return (None, 0, 0)

  def readString(self):
    self.finishedRead = True
    return self.trans.read(-1)

  def readFieldBegin(self):
    id = 0
    ttype = TType.STOP
    if not self.finishedRead:
      ttype = TType.STRING
    return (None, ttype, id)
  
  def writeMessageBegin(self, name, request_type, seqid, service_name):
    self.writeString('v=')
    self.writeNumber(self.VERSION)
    self.writeString('&p=')

  def writeString(self, string):
    self.trans.write(string)

  def writeNumber(self, number):
    jsNumber = str(number)
    self.trans.write(jsNumber)


class TSimpleProtocolFactory(object):

  def getProtocol(self, trans):
      return TSimpleProtocol(trans)
