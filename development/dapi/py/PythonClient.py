#!/usr/bin/env python

import os
import sys
import json
import traceback
from random import choice

pwd = os.path.dirname(__file__)
sys.path.append(pwd + '/gen-py')
sys.path.append(pwd + '/../../lib/py/thrift')

from dapi import DAPI
from dapi import auth
from dapi import account
from dapi import friends
from dapi import identities
from dapi.constants import *
from dapi.ttypes import *

from thrift import Thrift
from thrift.transport import THttpModClient, TTransport
from thrift.protocol import TDAPIProtocol

TEST_EMAIL = 'jdao@zynga.com'
TEST_APPID = '5001263'
TEST_APPSECRET = '6ea1577c3e6b8678c10af38d26fa09fd'
TEST_ZID = '34852696123'
TEST_FRIEND_ZID = '31111262053'


class Client:

    protocol = None

    def __init__(self):
        pass

    def run(self):
        try:
            # manual invocation of DAPI
            print "Testing Plain DAPI Interface"
            self.testPlainDAPIInterface()

            # invocation of dapi via service endpoints
            print "\nTesting Web Service-based DAPI Interface"
            self.testWebServices()

            print "\nDONE"

        except Thrift.TException, tx:
            tb = traceback.format_exc()
            print "%s" % (tx.message)
            print tb

    # Test manual invocation of DAPI
    def testPlainDAPIInterface(self):
        self.runDAPITestSimple()

        method = None
        arguments = None
        data = None
        snid = '24'
        zid = None
        userId = None
        generatedPassword = self.getRandomHexString(32)
        appToken = AppToken(TEST_APPID, snid, TEST_APPSECRET)
        dapiResponse = None

        method = 'account.exists'
        print "\n--" + method + "()"
        arguments = {
            'email': TEST_EMAIL
        }
        dapiResponse = self.runDAPICall(method, arguments)
        data = self.getString(dapiResponse)

        method = 'auth.registerDevice'
        print "\n--" + method + "()"
        arguments = {
            'snid': snid,
            'password': generatedPassword
        }
        dapiResponse = self.runDAPICall(method, arguments)
        data = self.getDict(dapiResponse)
        userId = data['userId']
        zid = data['zid']

        method = 'auth.issueToken'
        print "\n--" + method + "()"
        arguments = {
            'appId': TEST_APPID,
            'password': generatedPassword,
            'userId': userId,
            'zid': zid
        }
        dapiResponse = self.runDAPICall(method, arguments)
        data = self.getDict(dapiResponse)
        token = data['token']

        method = 'friends.areFriends'
        print "\n--" + method + "()"
        arguments = {
            'snid': snid,
            'zid': TEST_ZID,
            'friendZid': TEST_FRIEND_ZID
        }
        dapiResponse = self.runDAPICall(method, arguments, _appToken=appToken)
        data = self.getString(dapiResponse)

        method = 'identities.get'
        print "\n--" + method + "()"
        arguments = {
            'zids': TEST_ZID,
        }
        dapiResponse = self.runDAPICall(method, arguments, _stringToken=token)
        data = self.getString(dapiResponse)

    # Example testing DAPI endpoints using raw JSON string
    def runDAPITestSimple(self):
        print 'Testing DAPI endpoints using raw JSON string'
        transport = THttpModClient.THttpModClient('https://api.zynga.com')
        transport = TTransport.TBufferedTransport(transport)
        protocol = TDAPIProtocol.TSimpleProtocol(transport)
        client = DAPI.Client(protocol)
        transport.open()
        requestBody = '{"c": [{"m":"account.exists", "al": {"email": "' + TEST_EMAIL + '"}}]}'
        exists_simple = client.call_simple(requestBody)
        print '  %s' % exists_simple
        transport.close()

    # Example testing DAPI endpoints using single DAPI Class
    def runDAPICall(self, method, args, _userToken=None, _appToken=None, _stringToken=None):
        transport = THttpModClient.THttpModClient('https://api.zynga.com')
        transport = TTransport.TBufferedTransport(transport)
        protocol = TDAPIProtocol.TDAPIProtocol(transport)
        client = DAPI.Client(protocol)
        transport.open()
        dapiCalls = [CallRequest(method, args)]
        dapiRequest = DAPIRequest(dapiCalls, userToken=_userToken, appToken=_appToken, stringToken=_stringToken)
        dapiResponse = client.call(dapiRequest)
        transport.close()
        return dapiResponse

    # Example testing Web Service-based DAPI Interface
    def testWebServices(self):
        snid = '24'
        zid = None
        userId = None
        generatedPassword = self.getRandomHexString(32)
        appToken = AppToken(TEST_APPID, snid, TEST_APPSECRET)
        dapiResponse = None

        auth = self.getWebService('auth')
        account = self.getWebService('account')
        friends = self.getWebService('friends')
        identities = self.getWebService('identities')

        print "\n--account.exists()"
        dapiResponse = account.exists(TEST_EMAIL)
        data = self.getString(dapiResponse)

        print "\n--auth.registerDevice()"
        dapiResponse = auth.registerDevice(snid, generatedPassword)
        data = self.getDict(dapiResponse)
        userId = data['userId']
        zid = data['zid']

        print "\n--auth.issueToken()"
        dapiResponse = auth.issueToken(TEST_APPID, zid, userId, generatedPassword)
        data = self.getDict(dapiResponse)
        token = data['token']

        appTokenDict = {
            'appId' : appToken.appId,
            'snId' : appToken.snId,
            'secret' : appToken.secret
        }
        self.protocol.appToken = appTokenDict

        print "\n--friends.areFriends()"
        dapiResponse = friends.areFriends(snid, TEST_ZID, TEST_FRIEND_ZID)
        data = self.getString(dapiResponse)

        self.protocol.appToken = None
        self.protocol.stringToken = token

        print "\n--identities.get()"
        dapiResponse = identities.get([zid])
        data = self.getString(dapiResponse)

        self.protocol.trans.close()

    def getWebService(self, service_name):
        if self.protocol is None:
            transport = THttpModClient.THttpModClient('https://api.zynga.com')
            transport = TTransport.TBufferedTransport(transport)
            protocol = TDAPIProtocol.TDAPIProtocol(transport)
            self.protocol = protocol
            transport.open()

        Client = getattr(sys.modules[__name__], service_name)
        return Client.Client(self.protocol)

    def getString(self, dapiResponse):
        result = None
        if dapiResponse.error:
            print 'ERROR in DAPI request, dapiResponse.error %s' % dapiResponse.error
        else:
            call = dapiResponse.calls[0]
            if call.error is not None:
                print 'ERROR in DAPI request, dapiResponse.calls.error %s' % call.error
            else:
                result = call.data
        print '  %s' % result
        return result

    def getDict(self, dapiResponse):
        val = self.getString(dapiResponse)
        if val is None:
            val = "{}"
        return json.loads(val)

    def getRandomHexString(self, length):
        result = ''
        hex = '0123456789abcdef'
        for i in range(length):
            result = result + choice(hex)
        return result

# Run Tests
c = Client()
c.run()
