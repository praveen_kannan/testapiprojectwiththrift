using MiniJSON;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Thrift;
using Thrift.Protocol;
using Thrift.Server;
using Thrift.Transport;

public class CSharpClient
{
	public const string TEST_EMAIL = "jdao@zynga.com";
	public const string TEST_APPID = "5001263";
	public const string TEST_APPSECRET = "6ea1577c3e6b8678c10af38d26fa09fd";
	public const string TEST_ZID = "34852696123";
	public const string TEST_FRIEND_ZID = "31111262053";

	private static Random random = new Random();

	private TDAPIProtocol protocol = null;

	public static void Main()
	{
		try
		{
			CSharpClient c = new CSharpClient();

			// manual invocation of DAPI
			Console.WriteLine("Testing Plain DAPI Interface");
			c.testPlainDAPIInterface();

			// invocation of dapi via service endpoints
			Console.WriteLine("Testing Web Service-based DAPI Interface\n");
			c.testWebServices();

			Console.WriteLine("DONE");
		}
		catch (TApplicationException x)
		{
			Console.WriteLine(x.StackTrace);
		}
	}

	/// <summary>
	/// Test manual invocation of DAPI
	/// </summary>
	public void testPlainDAPIInterface()
	{
		runDAPITestSimple();

		string method = null;
		Dictionary<string, string> arguments = null;
		object data;
		string snid = "24";
		string zid = null;
		string userId = null;
		string generatedPassword = GetRandomHexString(32);
		AppToken appToken = new AppToken();
		appToken.AppId = TEST_APPID;
		appToken.SnId = snid;
		appToken.Secret = TEST_APPSECRET;
		DAPIResponse dapiResponse = null;


		method = "account.exists";
		Console.WriteLine("--" + method + "()");
		arguments = new Dictionary<string, string>()
		{
			{ "email", TEST_EMAIL }
		};
		dapiResponse = runDAPICall(method, arguments);
		data = getString(dapiResponse);


		method = "auth.registerDevice";
		Console.WriteLine("--" + method + "()");
		arguments = new Dictionary<string, string>()
		{
			{ "snid", snid },
			{ "password", generatedPassword }
		};
		dapiResponse = runDAPICall(method, arguments);
		data = getDict(dapiResponse);
		userId = (string) (data as Dictionary<string, object>)["userId"];
		zid = ((long) (data as Dictionary<string, object>)["zid"]).ToString();


		method = "auth.issueToken";
		Console.WriteLine("--" + method + "()");
		arguments = new Dictionary<string, string>()
		{
			{ "appId", TEST_APPID },
			{ "password", generatedPassword },
			{ "userId", userId },
			{ "zid", zid }
		};
		dapiResponse = runDAPICall(method, arguments);
		data = getDict(dapiResponse);
		var token = (string) (data as Dictionary<string, object>)["token"];


		method = "friends.areFriends";
		Console.WriteLine ("--" + method + "()");
		arguments = new Dictionary<string, string>()
		{
			{ "snid", snid },
			{ "zid", TEST_ZID },
			{ "friendZid", TEST_FRIEND_ZID }
		};
		dapiResponse = runDAPICall(method, arguments, null, appToken);
		data = getString(dapiResponse);


		method = "identities.get";
		Console.WriteLine ("--" + method + "()");
		arguments = new Dictionary<string, string>()
		{
			{ "zids", TEST_ZID }
		};
		dapiResponse = runDAPICall(method, arguments, null, null, token);
		data = getString(dapiResponse);
	}

	/// <summary>
	/// Example testing DAPI endpoints using raw JSON string
	/// </summary>
	public void runDAPITestSimple()
	{
		Console.WriteLine("Testing DAPI endpoints using raw JSON string");
		Uri u = new Uri("https://api.zynga.com");
		TTransport transport = new THttpModClient(u);
		TProtocol protocol = new TSimpleProtocol(transport);
		DAPI.Client client = new DAPI.Client(protocol);

		transport.Open();

		string requestBody = "{\"c\": [{\"m\":\"account.exists\", \"al\": {\"email\": \"" + TEST_EMAIL + "\"}}]}";
		var exists_simple = client.call_simple(requestBody);
		Console.WriteLine("  " + exists_simple + "\n");

		transport.Close();
	}

	/// <summary>
	/// Example testing DAPI endpoints using Classes
	/// </summary>
	public DAPIResponse runDAPICall(string method, Dictionary<string, string> args, UserToken userToken = null, AppToken appToken = null, string stringToken = null)
	{
		Uri u = new Uri("https://api.zynga.com");
		TTransport transport = new THttpModClient(u);
		TProtocol protocol = new TDAPIProtocol(transport);
		DAPI.Client client = new DAPI.Client(protocol);

		transport.Open();

		List<CallRequest> dapiCalls = new List<CallRequest>();
		CallRequest callRequest = new CallRequest();
		callRequest.Method = method;
		callRequest.Argz = args;
		dapiCalls.Add(callRequest);

		DAPIRequest dapiRequest = new DAPIRequest();
		dapiRequest.Calls = dapiCalls;
		dapiRequest.UserToken = userToken;
		dapiRequest.AppToken = appToken;
		dapiRequest.StringToken = stringToken;

		DAPIResponse dapiResponse = client.call(dapiRequest);

		transport.Close();

		return dapiResponse;
	}

	/// <summary>
	/// Example testing Web Service-based DAPI Interface
	/// </summary>
	public void testWebServices()
	{
		object data;
		string snid = "24";
		string zid = null;
		string userId = null;
		string generatedPassword = GetRandomHexString(32);
		AppToken appToken = new AppToken();
		appToken.AppId = TEST_APPID;
		appToken.SnId = snid;
		appToken.Secret = TEST_APPSECRET;
		DAPIResponse dapiResponse = null;

		auth.Client Auth = getWebService(typeof(auth.Client)) as auth.Client;
		account.Client Account = getWebService(typeof(account.Client)) as account.Client;
		friends.Client Friends = getWebService(typeof(friends.Client)) as friends.Client;
		identities.Client Identities = getWebService(typeof(identities.Client)) as identities.Client;

		Console.WriteLine("--account.exists()");
		dapiResponse = Account.exists(TEST_EMAIL);
		data = getString(dapiResponse);

		Console.WriteLine("--auth.registerDevice()");
		dapiResponse = Auth.registerDevice(snid, generatedPassword);
		data = getDict(dapiResponse);
		userId = (string) (data as Dictionary<string, object>)["userId"];
		zid = ((long) (data as Dictionary<string, object>)["zid"]).ToString();

		Console.WriteLine("--auth.issueToken()");
		dapiResponse = Auth.issueToken(TEST_APPID, zid, userId, generatedPassword);
		data = getDict(dapiResponse);
		var token = (string) (data as Dictionary<string, object>)["token"];

		Dictionary<string, string> appTokenDict = new Dictionary<string, string>();
		appTokenDict["appId"] = appToken.AppId;
		appTokenDict["snId"] = appToken.SnId;
		appTokenDict["secret"] = appToken.Secret;
		this.protocol.appToken = appTokenDict;

		Console.WriteLine("--friends.areFriends()");
		dapiResponse = Friends.areFriends(snid, TEST_ZID, TEST_FRIEND_ZID);
		data = getString(dapiResponse);

		this.protocol.appToken = null;
		this.protocol.stringToken = token;

		Console.WriteLine("--identities.get()");
		dapiResponse = Identities.get(new List<string>() { zid });
		data = getString(dapiResponse);

		this.protocol.Transport.Close();
	}

	public object getWebService(Type clazz)
	{
		if (this.protocol == null)
		{
			Uri u = new Uri("https://api.zynga.com");
			TTransport transport = new THttpModClient(u);
			TDAPIProtocol protocol = new TDAPIProtocol(transport);
			this.protocol = protocol;
			transport.Open();
		}
		Type[] types = new Type[1];
		types[0] = typeof(TProtocol);
		return clazz.GetConstructor(types).Invoke(new object[]{ this.protocol });
	}

	public string getString(DAPIResponse d)
	{
		string result = null;
		if (d.Error != null)
		{
			Console.WriteLine("ERROR in DAPI request, dapiResponse.error: " + d.Error.Message + "\n");
		}
		else
		{
			CallResult c = d.Calls[0];
			if (c.Error != null)
			{
				Console.WriteLine("ERROR in DAPI request, dapiResponse.calls.error: " + c.Error.Message + "\n");
			}
			else
			{
				string data = c.Data;
				Console.WriteLine("  " + data + "\n");
				result = data;
			}
		}
		return result;
	}

	public Dictionary<string, object> getDict(DAPIResponse d)
	{
		Dictionary<string, object> result = null;
		string val = getString(d);
		if (val == null)
		{
			result = new Dictionary<string, object>();
		}
		else
		{
			result = Json.Deserialize(val) as Dictionary<string,object>;;
		}
		return result;
	}

	public static string GetRandomHexString(int length)
	{
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < length; i++)
		{
			builder.Append(random.Next(16).ToString("X"));
		}
		return builder.ToString();
	}
}
