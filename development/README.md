thrift-dapi
--

This prototype explores using Apache Thrift to generate simple, language 
specific clients for use with Zynga web services, DAPI in particular.

The prototype generates a client for raw or batched communications
with DAPI, and a set of clients for five specific web service end-points.
This mix of endpoints spans the use cases commonly found in games,
as they allow developers to easily invoke existing APIs or manually
invoke new ones.

#### Apache Thrift
An open-source library available from Apache and originally created by
Facebook. For more information see http://thrift.apache.org and
http://diwakergupta.github.com/thrift-missing-guide/.  If you would like
to generate the web service endpoints you will need to build Thrift
from source, but the prototype included in the repo will run right
out of the box.  Running the prototype without invoking Thrift demonstrates
how the game team would only need to use a set of generated endpoints, which
could be published by a central developer.

## Dependencies
* thrift - http://thrift.apache.org/ (build from source [here](https://github-ca.corp.zynga.com/midcore/thrift))
* python - http://www.python.org/
* mono - http://www.mono-project.com/

## Building
There is a top level Makefile used for building all the components in this 
project. Build everything simply with the command "make":
```
make
```

To see a list of the make targets, use can use the following commands:
```
make help
make help_verbose
```

To build everything in parallel:
```
make -j
```

## Running the Demos
After you have built the necessary targets, you can run the demos:

### DAPI
```bash
# python
python dapi/py/PythonClient.py

# csharp
mono .build/DAPICsharpClient.exe
```

### HelloWorld
```bash
# python
python helloworld/py/PythonServer.py
python helloworld/py/PythonClient.py # in another tab/window

# csharp
mono .build/HelloWorldCsharpServer.exe
mono .build/HelloWorldCsharpClient.exe # in another tab/window
```

## Main Files and Directories

Since this project explores different languages with DAPI, for simplicity, we 
will denote "XX" in this README a wildcard for language file extensions. The 
code examples in this README are written in Python, since it reads like 
pseudocode. The project also includes a working example of Python communicating
with DAPI, providing a clear set of conceptual porting instructions to other
languages.

### dapi.thrift
File where the structs and services for the DAPI examples are defined using 
Thrift Interface Description Language
([Thrift IDL](http://thrift.apache.org/docs/idl/)).

### gen-XX/
Auto-generated source. To generate it, make sure you have Thrift installed from 
source (http://thrift.apache.org/docs/install/). The prototype was generated 
from a version of Thrift with minor modifications to the method signatures of 
the Python Thrift libraries.  Build and install Thrift from the 
[Midcore Thrift Repo](https://github-ca.corp.zynga.com/midcore/thrift).

Once Thrift is installed, and if you make changes to the *.thrift IDL file or
modify the thrift code-generation source, you can regenerate the files in
gen-py/* with the following command:

```bash
thrift --gen XX dapi.thrift
```

If you are experimenting with other languages, you can pass in a different
argument to the --gen option. For a list of all supported languages as well
as options, be sure to check out the thrift help page ```thrift --help```.

If there are no errors, the generated code will be created.


### lib/XX/thrift/
Folder containing all the thrift library code, with a few extra files listed 
below. The changes to these files are also present in the 
[Midcore Thrift Repo](https://github-ca.corp.zynga.com/midcore/thrift).

### TDAPIProtocol.XX
Adheres to DAPI Protocol v1.2:
https://zyntranet.apps.corp.zynga.com/display/DAPI/Protocol+v1.2

The request protocol is not fully JSON, but rather it is url-encoded values,
the version & payload, where the value of the payload is JSON. The response
protocol from DAPI is all JSON, but does not quite follow the thrift JSON
protocol (e.g. no numbered fields, JSON response is an Object not an Array,
etc.). ```TDAPIProtocol.XX``` was implemented to address these needs.


### THttpModClient.XX
A fork of THttpClient.XX but instead of using the HTTP 'Content-Type' header
```application/x-thrift```, which is what THttpClient.XX uses,
THttpModClient.XX uses ```application/x-www-form-urlencoded header```.


### Client.XX

##### DAPI Clients
For each of the five DAPI endpoints invoked, we demonstrate how to make
the call using the raw client and the specific client for each web service
endpoint.

#### [account.exists](https://developers-internal.zynga.com/docs/api/Account#exists)
Tests the ```account``` client's ```exists()``` method, and performs the same
operation by passing a raw JSON string to the ```DAPI``` client's ```call_simple()```
method.

##### Specific Call
```python
...
account = self.getWebService('account')
response = account.exists(email='jdao@zynga.com')
data = self.getString(response)
...
```

##### Raw Call
```python
...
method = 'account.exists'
arguments = {
    'email': 'jdao@zynga.com'
}
response = self.runDAPICall(method, arguments)
data = self.getString(response)
...
```

##### Output
Request Body: 
```
v=1.2&p={"c":[{"m":"account.exists","al": {"email":"jdao@zynga.com"}}]}
```

Response Body: 
```
{"calls":[{"data":{"gwf":true,"wfn":true,"reset":false}}]}
```

Test cURL call for comparison:
```
curl -k -g -d v=1.2 -d 'p={"c":[{"m":"account.exists","al":{"email":"jdao@zynga.com"}}]}' https://api.zynga.com
```

--

#### [auth.registerDevice](https://developers-internal.zynga.com/docs/api/Auth#registerDevice)
Tests the ```auth``` client's ```registerDevice``` method and performs the same
operation using the raw client.  The ```generatedPassword``` field is a random 32
character hex string.

##### Specific Call
```python
...
auth = self.getWebService('auth')
response = auth.registerDevice(snId='24', password=self.generatedPassword)
data = self.getDict(response)
userId = data['userId']
zid = data['zid']
...
```

##### Raw Call
```python
...
method = 'auth.registerDevice'
arguments = {
  'snid': '24',
  'password': self.generatedPassword
}
response = self.runDAPICall(method, arguments)
data = self.getDict(response)
userId = data['userId']
zid = data['zid']
...
```

##### Output
Request Body: 
```
v=1.2&p={"c":[{"m":"auth.registerDevice","al":{"snid":"24","password":"f759b782462afaac61fdd72a8847b4c7"}}]}
```

Response Body: 
```
{"calls":[{"data":{"userId":"24:3fe55cc47a0916a9ad5555d2e439a5bb","zid":34894816320}}]}
```

Test cURL call for comparison:
```
curl -k -g -d v=1.2 -d 'p={"c":[{"m":"auth.registerDevice","al":{"snid":"24","password":"f759b782462afaac61fdd72a8847b4c7"}}]}' https://api.zynga.com
```

--

#### [auth.issueToken](https://developers-internal.zynga.com/docs/api/Auth#issueToken)
The typical auth flow for new mobile users is anonymous.  This is a two step
process where first the device is regsitered if has not been already and then
a session token is issued. This method uses the same password used with the previous
example as well as the userId and zid generated by the auth.registerDevice() response.

##### Specific Call
```python
...
response = auth.issueToken(appId, zid, userId, self.generatedPassword)
data = self.getDict(response)
stringToken = data['token']
...
```

##### Raw Call
```python
...
method = 'auth.issueToken'
arguments = {
  'appId': appId,
  'password': password,
  'userId': userId,
  'zid': zid
}
response = self.runDAPICall(method, arguments)
data = self.getDict( response )
stringToken = data['token']
...
```

##### Output
Request Body: 
```
v=1.2&p={"c":[{"m":"auth.issueToken","al":{"appId":"5000663","password":"f759b782462afaac61fdd72a8847b4c7","userId":"24:ff7b430705d4e1ba286d8b4f6fc1e3e5","zid":"34852696123"}}]}
```

Response Body: 
```
{"calls":[{"data":{"zid":34852696123,"token":"NWIxMjlhZGJkZGFiYmFhNzlhMGVjZDc1MWU4NzdkNGM1MDExZjg0ZTJiMDg5MGZiZmQ1YjZiYjIxN2NlNmNlNnxhfDM0ODUyNjk2MTIzfDUwMDA2NjN8MTM2MTQxNDQzMQ","expires":1361414431}}]}
```

Test cURL call for comparison:
```
curl -k -g -d v=1.2 -d 'p={"c":[{"m":"auth.issueToken","al":{"appId":"5000663","password":"f759b782462afaac61fdd72a8847b4c7","userId":"24:ff7b430705d4e1ba286d8b4f6fc1e3e5","zid":"34852696123"}}]}' https://api.zynga.com
```

--

#### [friends.areFriends](https://developers-internal.zynga.com/docs/api/Friends#areFriends)
This call demonstrates how the Thrift protocol can be modified to handle appending 
a token to requests without altering the method signature of the specific client.
In this case the friends.areFriends() method requires the appToken with the
application secret in it. See below in the "Next Steps" section on a note about
improving the token handling flow.

##### Specific Call
```python
...
self.protocol.appToken = AppToken(appid, snid, appsecret)
friends = self.getWebService('friends')
response = friends.areFriends(snid, zid, friendZid)
areFriends_specific = self.getString(response)
...
```

##### Raw Call
```python
...
method = 'friends.areFriends'
arguments = {
  'snid': snid,
  'zid': zid,
  'friendZid': friendZid
}
appToken = AppToken(appid, snid, appsecret)
response = self.runDAPICall(method, arguments, _appToken=appToken)
areFriends_raw = self.getString(response)
...
```

##### Output
Request Body: 
```
v=1.2&p={"c":[{"m":"friends.areFriends","al":{"snid":"24","zid":"34852696123","friendZid":"31111262053"}}],"t":{"a":"5001263","n":"24","as":"6ea1577c3e6b8678c10af38d26fa09fd"}}
```

Response Body: 
```
{"calls":[{"data":false}]}
```

Test cURL call for comparison:
```
curl -k -g -d v=1.2 -d 'p={"c":[{"m":"friends.areFriends","al":{"snid":"24","zid":"34852696123","friendZid":"31111262053"}}],"t":{"a":"5001263","n":"24","as":"6ea1577c3e6b8678c10af38d26fa09fd"}}' https://api.zynga.com
```

--

#### [identities.get](https://developers-internal.zynga.com/docs/api/Identities#get)
This call obtains a list of identities for a given set of zids.  In the example,
we unset the ```appToken``` before setting the ```stringToken``` to avoid causing
serialization confusion in the ```TDAPIProtocol``` class. 

##### Specific Call
```python
...
self.protocol.appToken = None
self.protocol.stringToken = token
response = ids.get([zid])
ids_specific = self.getString( response )
...
```

##### Raw Call
```python
...
method = 'identities.get'
arguments = {
  'zids': [zid],
}
response = self.runDAPICall(method, arguments, _stringToken=anonToken)
ids_raw = self.getString(response)
...
```  

##### Output
Request Body: 
```
v=1.2&p={"c":[{"m":"identities.get","al":{"zids":["31111262148"]}}],"t":"ZmI2MjdiM2FkMmFhNjlhNmY4NGYzYjI4MjgxZTE3NGEyYmE0YTUxNTZjZmYyODUwNDhhZWRiMWUxYjY5NTgxNXxhfDM0ODg4ODUzOTg4fDUwMDA2NjN8MTM2MTM0ODg3Mg"}
```

Response Body: 
```
{"calls":[{"data":{"mappings":{"34888853988":[]}}}]}
```

Test cURL call for comparison:
```
curl -k -g -d v=1.2 -d 'p={"c":[{"m":"identities.get","al":{"zids":["31111262148"]}}],"t":"ZmI2MjdiM2FkMmFhNjlhNmY4NGYzYjI4MjgxZTE3NGEyYmE0YTUxNTZjZmYyODUwNDhhZWRiMWUxYjY5NTgxNXxhfDM0ODg4ODUzOTg4fDUwMDA2NjN8MTM2MTM0ODg3Mg"}' https://api.zynga.com
```

--


#### Helper Methods

##### runDAPICall()
This function marshalls Python objects into the correct format for use
with the ```DAPI``` client's ```call``` method. Tthe code for marshalling 
is auto-generated using thrift, and found in gen-py/.
```python
runDAPICall(method, args)
  transport = THttpModClient.THttpModClient('https://api.zynga.com')
  transport = TTransport.TBufferedTransport(transport)
  protocol = TDAPIProtocol.TDAPIProtocol(transport)
  client = DAPI.Client(protocol)
  transport.open()
  dapiCalls = [CallRequest(method, args)]
  dapiRequest = DAPIRequest(dapiCalls, userToken=_userToken, appToken=_appToken, stringToken=_stringToken)
  dapiResponse = client.call(dapiRequest)
  transport.close()
```

##### getWebService()
This function instantiates a client by class name.  A similar approach
could be taken in a statically typed language, the caller would just
need to cast the result of the call to the appropriate client type.
```python
getWebService( serviceName )
  if self.protocol is None:
     transport = THttpModClient.THttpModClient('https://api.zynga.com')
     transport = TTransport.TBufferedTransport(transport)
     protocol = TDAPIProtocol.TDAPIProtocol(transport)
     self.protocol = protocol
     ## up to the caller to close after all calls are complete
     ## could be better abstracted in a production release
     transport.open()

  Client = getattr(sys.modules[__name__], service_name)
  return Client.Client(self.protocol)
```

##### getString()
Since the specific endpoint clients cannot invoke endpoints in batches
the DAPIResponse object will only ever contain a single call object.  
This method inspects the call object for any errors and if none are
found then returns the string to the call.  This method is used by the
```getDict()``` helper method to fetch input for a json deserialize call.
```python
getString( dapiResponse )
  result = None
  if dapiResponse.error:
    print 'ERROR in DAPI request, dapiResponse.error %s' % dapiResponse.error
  else:
    call = dapiResponse.calls[0]
    if call.error is not None:
      print 'ERROR in DAPI request, dapiResponse.calls.error %s' % call.error
    else:
      result = call.data
  return result  
```


#### HelloWorld
The HelloWorld tests included in this project demonstrate how Thrift is 
typically used to ease client-server communciations.

##### runHelloWorldExample()
Test example which communicates with local Python server over HTTP. Sample calls
listed below.
```python
runHelloWorldExample()
  client.ping()
  ## request body: [1,"ping",1,0,{}]
  ## response body: [1,"ping",2,0,{}]

  msg = client.sayHello()
  ## request body: [1,"sayHello",1,0,{}]
  ## response body: [1,"sayHello",2,0,{"0":{"str":"say hello from Server"}}]

  msg = client.sayMsg(HELLO_IN_KOREAN)
  ## request body: [1,"sayMsg",1,0,{"1":{"str":"an-nyoung-ha-se-yo"}}]
  ## response body: [1,"sayMsg",2,0,{"0":{"str":"say an-nyoung-ha-se-yo from Server"}}]

  struct = client.structTest(HELLO_IN_JAPANESE)
  ## request body: [1,"structTest",1,0,{"1":{"str":"konichiwa!"}}]
  ## response body: [1,"structTest",2,0,{"0":{"rec":{"1":{"str":"konichiwa!"}}}}]
```

##### runHelloWorldSocketExample()
Test the HelloWorld example using TSocket. If you want to test this, call
this function from the main run() function. You will also need to run the
runServerSocket() function in the Server.XX runner code.
```python
runHelloWorldSocketExample()
```

### Server.XX
Test Server which runs (locally), and accepts requests from
```runHelloWorldExample()``` and ```runHelloWorldSocketExample()``` above.
Replace ```runHttpServer()``` with ```runServerSocket()``` if you want to see
the HelloWorld example run using TServerSocket instead of THttpServer.


## Next Steps
Need to extend thrift code generators to output code with async callbacks. Not
all languages currently support it. See the thrift help page for details.

It should be noted that in the prototype there are three attributes on the 
```self.protocol``` object that do not play nicely together: ```userToken```, 
```appToken```, and ```stringToken```.  You must unset one before using another.
Also we do not want TDAPIProtocol to depend on code generated from thrift (i.e. 
AppToken and UserToken), when setting these tokens. Right now, the AppToken 
(and soon to be UserToken) objects get converted to a more generic dictionary
when setting them on ```self.protocol```. This is an area for improvement.

There are places in the DAPI protocol that are dynamic (all the places defined
as ```data: <mixed>```). For these cases, there are custom parsed: refer to
```readString()``` function in ```TDAPIProtocol.XX```. We detect what JSON type
we are currently reading, and parse it accordingly. Currently, bools get parsed
into either "true" or "false" string values.

Also, it should be noted that if doc comments are placed in the Thrift IDL file
those comments will be included in the generated source.  Being able to access
document comments within an IDE can often be a helpful tool for developers
trying to leverage an interface from an external team.
