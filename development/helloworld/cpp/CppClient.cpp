#include "gen-cpp/HelloWorld.h"
#include "gen-cpp/helloworld_constants.h"
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TJSONProtocol.h>
#include <thrift/transport/THttpClient.h>
#include <thrift/transport/TSocket.h>

using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;

using boost::shared_ptr;

void runHelloWorldExample() {
  shared_ptr<TTransport> transport(new THttpClient("localhost", 9090, "/"));
  //shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));
  shared_ptr<TProtocol> protocol(new TJSONProtocol(transport));
  HelloWorldClient client(protocol);

  transport->open();

  client.ping();
  printf("ping()\n");

  string msg;
  client.sayHello(msg);
  printf("%s\n", msg.c_str());

  client.sayMsg(msg, g_helloworld_constants.HELLO_IN_KOREAN);
  printf("%s\n", msg.c_str());

  HelloStruct helloStruct;
  client.structTest(helloStruct, g_helloworld_constants.HELLO_IN_JAPANESE);
  printf("%s\n", helloStruct.id.c_str());

  transport->close();
}

void runHelloWorldSocketExample() {
  shared_ptr<TTransport> socket(new TSocket("localhost", 9090));
  shared_ptr<TTransport> transport(new TBufferedTransport(socket));
  shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));
  HelloWorldClient client(protocol);

  transport->open();

  client.ping();
  printf("ping()\n");

  string msg;
  client.sayHello(msg);
  printf("%s\n", msg.c_str());

  client.sayMsg(msg, g_helloworld_constants.HELLO_IN_KOREAN);
  printf("%s\n", msg.c_str());

  HelloStruct helloStruct;
  client.structTest(helloStruct, g_helloworld_constants.HELLO_IN_JAPANESE);
  printf("%s\n", helloStruct.id.c_str());

  transport->close();
}

int main(int argc, char** argv) {
  try {
	//runHelloWorldSocketExample();
	runHelloWorldExample();
    printf("DONE\n");
  } catch (TException &tx) {
    printf("ERROR: %s\n", tx.what());
  }

}
