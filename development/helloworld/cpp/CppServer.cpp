#include "gen-cpp/HelloWorld.h"
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TJSONProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/THttpServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

using boost::shared_ptr;

class HelloWorldHandler : virtual public HelloWorldIf {
 public:
  HelloWorldHandler() {
    // Your initialization goes here
  }

  void ping() {
    printf("ping\n");
  }

  void sayHello(std::string& _return) {
    printf("sayHello()\n");
    _return = "say hello from Server";
  }

  void sayMsg(std::string& _return, const std::string& msg) {
    printf("sayMsg(%s)\n", msg.c_str());
    _return = msg;
  }

  void zip() {
    printf("zip\n");
  }

  void structTest(HelloStruct& _return, const std::string& msg) {
	  printf("structTest(%s)\n", msg.c_str());
	  HelloStruct result;
	  result.id = msg;
	  _return = result;
  }

};

void runHttpServer() {
  int port = 9090;
  shared_ptr<HelloWorldHandler> handler(new HelloWorldHandler());
  shared_ptr<TProcessor> processor(new HelloWorldProcessor(handler));
  shared_ptr<TServerTransport> serverSocket(new TServerSocket(port));
  shared_ptr<TTransportFactory> transportFactory(new THttpServerTransportFactory());
  //shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());
  shared_ptr<TProtocolFactory> protocolFactory(new TJSONProtocolFactory());

  TSimpleServer server(processor, serverSocket, transportFactory, protocolFactory);
  printf("Starting http server...\n");
  server.serve();
  printf("done!\n");
}

void runSocketServer() {
  int port = 9090;
  shared_ptr<HelloWorldHandler> handler(new HelloWorldHandler());
  shared_ptr<TProcessor> processor(new HelloWorldProcessor(handler));
  shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));
  shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
  shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

  TSimpleServer server(processor, serverTransport, transportFactory, protocolFactory);
  printf("Starting simple socket server...\n");
  server.serve();
  printf("done!\n");
}

int main(int argc, char **argv) {
  //runSocketServer();
  runHttpServer();
  return 0;
}
