using System;
using Thrift;
using Thrift.Protocol;
using Thrift.Server;
using Thrift.Transport;

namespace CSharpTutorial
{
	public class CSharpClient
	{
		public static void Main()
		{
			try
			{
				//runHelloWorldExample();
				runHelloWorldSocketExample();
				Console.WriteLine("DONE");
			}
			catch (TApplicationException x)
			{
				Console.WriteLine(x.StackTrace);
			}
		}

		public static void runHelloWorldExample() {
			Uri u = new Uri("http://localhost:9090");
			TTransport transport = new THttpClient(u);

			TProtocol protocol = new TBinaryProtocol(transport);
			HelloWorld.Client client = new HelloWorld.Client(protocol);

			transport.Open();

			client.ping();
			Console.WriteLine("ping()");

			string msg = client.sayHello();
			Console.WriteLine(msg);

			msg = client.sayMsg(Constants.HELLO_IN_KOREAN);
			Console.WriteLine(msg);

			HelloStruct hellostruct = client.structTest(Constants.HELLO_IN_JAPANESE);
			Console.WriteLine(hellostruct.Id);

			transport.Close();
		}

		public static void runHelloWorldSocketExample() {
			TTransport transport = new TSocket("localhost", 9090);
			TProtocol protocol = new TBinaryProtocol(transport);
			HelloWorld.Client client = new HelloWorld.Client(protocol);

			transport.Open();

			client.ping();
			Console.WriteLine("ping()");

			string msg = client.sayHello();
			Console.WriteLine(msg);

			msg = client.sayMsg(Constants.HELLO_IN_KOREAN);
			Console.WriteLine(msg);

			HelloStruct hellostruct = client.structTest(Constants.HELLO_IN_JAPANESE);
			Console.WriteLine(hellostruct.Id);

			transport.Close();
		}
	}
}
