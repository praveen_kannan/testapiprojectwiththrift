using System;
using System.Collections.Generic;
using Thrift.Server;
using Thrift.Transport;

namespace CSharpTutorial
{
    public class HelloWorldHandler : HelloWorld.Iface
    {
        public void ping()
        {
            Console.WriteLine("ping()");
        }

        public string sayHello()
        {
            Console.WriteLine("sayHello()");
            return "say hello from Server";
        }

        public string sayMsg(string msg)
        {
            Console.WriteLine("sayMsg(" + msg + ")");
            return "say " + msg + " from Server";
        }

        public HelloStruct structTest(string msg)
        {
            Console.WriteLine("structTest(" + msg + ")");
            HelloStruct res = new HelloStruct();
            res.Id = msg;
            return res;
        }

        public void zip()
        {
            Console.WriteLine("zip()");
        }
    }

    public class CSharpServer
    {
        public static void Main()
        {
            try
            {
                runSocketServer();
            }
            catch (Exception x)
            {
                Console.WriteLine(x.StackTrace);
            }
            Console.WriteLine("done.");
        }

        public static void runSocketServer() {
            HelloWorldHandler handler = new HelloWorldHandler();
            HelloWorld.Processor processor = new HelloWorld.Processor(handler);
            TServerTransport serverTransport = new TServerSocket(9090);
            TServer server = new TSimpleServer(processor, serverTransport);

            // Use this for a multithreaded server
            // server = new TThreadPoolServer(processor, serverTransport);

            Console.WriteLine("Starting the server...");
            server.Serve();
        }
    }
}
