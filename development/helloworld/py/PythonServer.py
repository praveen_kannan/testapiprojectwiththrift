#!/usr/bin/env python

import os
import sys

pwd = os.path.dirname(__file__)
sys.path.append(pwd + '/gen-py')
sys.path.append(pwd + '/../../lib/py/thrift')

from helloworld import HelloWorld
from helloworld.ttypes import *

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.server import THttpServer
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TJSONProtocol
from thrift.server import TServer


class HelloWorldHandler:
    def __init__(self):
        self.log = {}

    def ping(self):
        print "ping()"

    def sayHello(self):
        print "sayHello()"
        return "say hello from Server"

    def sayMsg(self, msg):
        print "sayMsg(" + msg + ")"
        return "say " + msg + " from Server"

    def structTest(self, msg):
        print "structTest(" + msg + ")"
        return HelloStruct(msg)


def runHttpServer():
    handler = HelloWorldHandler()
    processor = HelloWorld.Processor(handler)

    #pfactory = TBinaryProtocol.TBinaryProtocolFactory()
    pfactory = TJSONProtocol.TJSONProtocolFactory()

    server = THttpServer.THttpServer(processor, ('', 30303), pfactory)

    print 'Starting http server...'
    server.serve()
    print "done!"


def runSimpleServer():
    handler = HelloWorldHandler()
    processor = HelloWorld.Processor(handler)

    transport = TSocket.TServerSocket(port=30303)
    tfactory = TTransport.TBufferedTransportFactory()
    #pfactory = TBinaryProtocol.TBinaryProtocolFactory()
    pfactory = TJSONProtocol.TJSONProtocolFactory()

    server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)

    print "Starting simple socket server..."
    server.serve()
    print "done!"

runHttpServer()
