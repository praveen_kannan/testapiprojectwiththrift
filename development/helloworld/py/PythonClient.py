#!/usr/bin/env python

import os
import sys
import traceback

pwd = os.path.dirname(__file__)
sys.path.append(pwd + '/gen-py')
sys.path.append(pwd + '/../../lib/py/thrift')

from helloworld import HelloWorld
from helloworld.constants import *
from helloworld.ttypes import *

from thrift import Thrift
from thrift.transport import THttpClient, TTransport, TSocket
from thrift.protocol import TJSONProtocol


class Client:

    protocol = None
    generatedPassword = ''

    def __init__(self):
        pass

    # Helloworld example testing Python Client/Server w/ THttpClient
    def runHelloWorldExample(self):
        transport = THttpClient.THttpClient('http://localhost:30303')
        transport = TTransport.TBufferedTransport(transport)
        # protocol = TBinaryProtocol.TBinaryProtocol(transport)
        protocol = TJSONProtocol.TJSONProtocol(transport)
        client = HelloWorld.Client(protocol)
        transport.open()
        client.ping()
        print "ping()"
        msg = client.sayHello()
        print msg
        msg = client.sayMsg(HELLO_IN_KOREAN)
        print msg
        struct = client.structTest(HELLO_IN_JAPANESE)
        print struct.id
        transport.close()

    # Helloworld example testing Python Client/Server w/ TSocket
    def runHelloWorldSocketExample(self):
        transport = TSocket.TSocket('localhost', 30303)
        transport = TTransport.TBufferedTransport(transport)
        # protocol = TBinaryProtocol.TBinaryProtocol(transport)
        protocol = TJSONProtocol.TJSONProtocol(transport)
        client = HelloWorld.Client(protocol)
        transport.open()
        client.ping()
        print "ping()"
        msg = client.sayHello()
        print msg
        msg = client.sayMsg(HELLO_IN_KOREAN)
        print msg
        struct = client.structTest(HELLO_IN_JAPANESE)
        print struct.id
        transport.close()

    def run(self):
        try:
            self.runHelloWorldExample()
            print 'DONE'

        except Thrift.TException, tx:
            tb = traceback.format_exc()
            print "%s" % (tx.message)
            print tb

# Run Tests
c = Client()
c.run()
