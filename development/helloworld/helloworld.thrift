const string HELLO_IN_KOREAN = "an-nyoung-ha-se-yo"
const string HELLO_IN_FRENCH = "bonjour!"
const string HELLO_IN_JAPANESE = "konichiwa!"

struct HelloStruct {
  1: string id
}

service HelloWorld {
  void ping()
  string sayHello()
  string sayMsg(1:string msg)
  oneway void zip()
  HelloStruct structTest(1:string msg)
}