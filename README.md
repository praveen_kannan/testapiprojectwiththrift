**ZyngaAPI-Thrift Readme**
=========================
Provides APIs to call DAPI modules via thrift generated code. Currently only the Track module is supported.

Folder structure overview:
---------------------------------------------------------

**/build:**
Top level build scripts for each platform. As ZyngaAPI-Thrift and its dependencies are being built, the output products will be copied to the exported folder. After all outputs are created they will be placed in a tar file in the release directory.

**/development:**
Internal projects and support files for testing features or new languages. Not related to end-use output product creation.

**/lib**
**/boost**
**/openssl:**
Supporting libraries, hierarchy needs to be cleaned up (consolidated).

**/thrift:**
Generated thrift support code for using thrift at runtime.

**/ZyngaAPI-Thrift:**
Generated code for DAPI modules (Track, Auth, etc), and management code.

To build:
---------------------------------------------------------
1) Go to ZyngaAPI-Thrift/IDL and run build_all.sh - this will generate source code that is not checked into the git repository and move it to the correct location. This assumes you have installed the  STG version of thrift, see: https://zyntranet.apps.corp.zynga.com/display/Mobile/Thift+Installation+Guide

2) Go to /build/&lt;platform> and run buld_all.sh to run the whole build pipeline for a given platform.

3) You will find build files in /build/&lt;platform>/exported and they are packaged as a .tgz file in /build&lt;platform>/release

How to Use:
---------------------------------------------------------
**iOS:**
Add thrift.framework, ZyngaAPI-Thrift.framework to your xcode project. You will also need libcrypto.a and libssl.a and will need to add the included openssl headers to your search path. Also requires boost headers to be added to your project search paths.

The following C++ project settings should be used in your project's xcode build settings for "Apple LLVM compiler 4.2 - Language":
* C Language Dialect: C99
* C++ Language Dialect GNU++11 [-std=gnu++11]
* C++ Standard Library: libc++ (LLVMC++ standard)

**Android:**
Using with ndk-build:
Point an existing ndk-build's NDK\_MODULE\_PATH to the unpacked released ZyngaAPI-Thrift folder. The prebuilt library's Android.mk will pull in packaged headers.

You will need the following includes packaged alongside the libzyngaapi-thrift_static.a library. Note that boost headers have been included because the underlying thrift code requires boost, but on Android these headers do not compile without fixup.

Requires building Application.mk with:
* APP_PLATFORM=android-9
* APP_STL := gnustl_static

Example usage in client code:
---------------------------------------------------------

	#include <ZyngaAPI-Thrift/DapiMgr.h>
	#include <ZyngaAPI-Thrift/AuthMgr.h>
	#include <ZyngaAPI-Thrift/gen-cpp/include/Track.h>

	DapiMgr::Create();
	AuthMgr::Create(appId, "2");

	std::map<std::string, std::string> sessionMap = ZyngaMiSocial::getSessionMap(Anonymous);

	// AuthMgr needs to be given an AuthContext before making your calls. One way to get this information
	// would be to call getSessionMap as shown above.
	AuthMgr::AuthContext context;
	context.mAppId = appId;
	context.mSnid = "24";
	context.mZid = sessionMap["zid"];
	context.mAuthToken = sessionMap["access_token"];
	context.mUserId = sessionMap["user_id"];

	AuthMgr::Get().InitializeAuth(context);


	// This is the typical response object that will be filled in when making a dapi call via a thrift module
	// Its format is described here for reference:
	// https://zyntranet.apps.corp.zynga.com/display/DAPI/Protocol+v1.2
	DAPIResponse response;


	// Each call has a unique arg struct with required and optional parameters
	// For more information see the documentation at: https://developers-internal.zynga.com/apps
	TrackLogCountArgs args;
	// Required arguments may be set via standard assignment operator
	args.counter = "CXX_testCounter";
	// IMPORTANT: Optional arguments must be set via __set accessor or they will not be sent over the wire
	args.__set_value(2); 

	// The response is returned immediately (blocking operation in this release)
	DapiMgr::Get().GetTrackClient()->LogCount(response, args);
	DapiMgr::Get().GetStringFromDapiResponse(stringResponse,response);
	CCLog("Called track and got response: %s", stringResponse.c_str());
	
Known Issues and Todos
---------------------------------------------------------
* Service calls are blocking operations.
* Exception thrown when service calls are used with no connectivity.