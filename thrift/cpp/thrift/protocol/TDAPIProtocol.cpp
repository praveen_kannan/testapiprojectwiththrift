#include "TDAPIProtocol.h"
#include "dapi_types.h"
#include "AuthMgr.h"
#include "DapiMgr.h"

#include <iostream>
#include <algorithm>
#include <string>
#include <boost/lexical_cast.hpp>
#include <thrift/transport/TTransportException.h>

using namespace apache::thrift::transport;

namespace apache { namespace thrift { namespace protocol {

// Static data
static const double kDAPIVersion = 1.2;

static const uint8_t kBoolVal[0x4] = { 't', 'f', 'T', 'F' };
static const uint8_t kBoolEnd[0x2] = { 'e', 'E' };

static const uint8_t kJSONObjectStart = '{';
static const uint8_t kJSONObjectEnd = '}';
static const uint8_t kJSONArrayStart = '[';
static const uint8_t kJSONArrayEnd = ']';
static const uint8_t kJSONNewline = '\n';
static const uint8_t kJSONPairSeparator = ':';
static const uint8_t kJSONElemSeparator = ',';
static const uint8_t kJSONBackslash = '\\';
static const uint8_t kJSONStringDelimiter = '"';
static const uint8_t kJSONZeroChar = '0';
static const uint8_t kJSONEscapeChar = 'u';

static const std::string kThriftNan("NaN");
static const std::string kThriftInfinity("Infinity");
static const std::string kThriftNegativeInfinity("-Infinity");
static const uint8_t kStringDelimiter = '"';

static map<string,string> create_map() {
    map<string,string> m;
    m["payload"] = "p";
    m["version"] = "v";
    m["calls"] = "c";
    m["token"] = "t";
    m["method"] = "m";
    m["argz"] = "al";
    m["name"] = "n";
    m["appId"] = "a";
    m["snId"] = "n";
    m["userId"] = "u";
    m["session"] = "s";
    m["secret"] = "as";
    m["userToken"] = "t";
    m["appToken"] = "t";
    m["stringToken"] = "t";
    return m;
}

static map<string,string> DAPIAliases = create_map();

TDAPIProtocol::TDAPIProtocol(boost::shared_ptr<TTransport> trans) :
	TVirtualProtocol<TDAPIProtocol, TJSONProtocol>(trans),
	bUserTokenSet(false),
	bAppTokenSet(false)
	{}

TDAPIProtocol::~TDAPIProtocol() {}

uint32_t TDAPIProtocol::readMessageBegin(std::string& name, TMessageType& messageType, int32_t& seqid) {
    startedParse = false;
    name = callName;
    messageType = T_REPLY;
    seqid = static_cast<int32_t>(0);
    return 0;
}

uint32_t TDAPIProtocol::readMessageEnd() {
    return 0;
}

uint32_t TDAPIProtocol::readStructBegin(std::string& name) {
    (void) name;
    if (!startedParse) {
        return 0;
    }
    return readJSONObjectStart();
}

uint32_t TDAPIProtocol::readStructEnd() {
    if (!startedParse) {
        return 0;
    }
    bool atEndOfStream = isAtEndOfStream(reader_);
    if (atEndOfStream) {
        return 0;
    }
    return readJSONObjectEnd();
}

uint32_t TDAPIProtocol::readFieldBegin(std::string& name, TType& fieldType, int16_t& fieldId) {
    (void) name;
    uint32_t result = 0;
    if (!startedParse) {
        startedParse = true;
        fieldType = T_STRUCT;
        fieldId = static_cast<int32_t>(0);
        return result;
    }

    uint8_t ch = 0;
    bool endOfStream = isAtEndOfStream(reader_);
    if (!endOfStream) {
        ch = reader_.peek();
    }

    if (endOfStream || ch == kJSONObjectEnd || ch == kJSONArrayEnd) {
        fieldType = apache::thrift::protocol::T_STOP;
        fieldId = static_cast<int32_t>(0);
        return result;
    }

    std::string fname;
    uint64_t fid = 0;
    result += readJSONString(fname);

    if (fname == "calls" || fname == "c") {
        fieldType = apache::thrift::protocol::T_LIST;
        fid = 1;
    }
    else if (fname == "error") {
        if (contexts_.size() == 1) {
            fid = 2;
        }
        else if (contexts_.size() == 3) {
            fid = 3;
        }
        else {
            throw TProtocolException(TProtocolException::INVALID_DATA, "error field not in expected context.");
        }
        fieldType = apache::thrift::protocol::T_STRUCT;
    }
    else if (fname == "token" || fname == "t") {
        fieldType = apache::thrift::protocol::T_STRING;
        fid = 3;
    }
    else if (fname == "zid") {
        fieldType = apache::thrift::protocol::T_STRING;
        fid = 4;
    }
    else if (fname == "name" || fname == "n") {
        fieldType = apache::thrift::protocol::T_STRING;
        fid = 1;
    }
    else if (fname == "data") {
        if (contexts_.size() == 3) {
            fid = 2;
        }
        else if (contexts_.size() == 4) {
            fid = 3;
        }
        else if (contexts_.size() == 2) {
            fid = 3;
        }
        else {
            throw TProtocolException(TProtocolException::INVALID_DATA, "data field not in expected context.");
        }
        fieldType = apache::thrift::protocol::T_STRING;
    }
    else if (fname == "message") {
        fieldType = apache::thrift::protocol::T_STRING;
        fid = 1;
    }
    else if (fname == "type") {
        fieldType = apache::thrift::protocol::T_STRING;
        fid = 2;
    }
    else {
        throw TProtocolException(TProtocolException::INVALID_DATA, "field " + fname + " not supported for readFieldBegin");
    }
    currField_.push(fname);

    fieldId = static_cast<int16_t>(fid);
    return result;
}

uint32_t TDAPIProtocol::readFieldEnd() {
    uint32_t result = 0;
    if (currField_.size() == 0) {
        return result;
    }
    string fname = currField_.top();
    currField_.pop();
    if (fname == "calls" || fname == "c") {
        return result;
    }
    else if (fname == "error") {
        return result;
    }
    else if (fname == "token" || fname == "t") {
        return result;
    }
    else if (fname == "zid") {
        return result;
    }
    else if (fname == "name" || fname == "n") {
        return result;
    }
    else if (fname == "data") {
        return result;
    }
    else if (fname == "message") {
        return result;
    }
    else if (fname == "type") {
        return result;
    }
    else {
        throw TProtocolException(TProtocolException::INVALID_DATA, "field " + fname + " not supported for readFieldEnd");
    }
}

uint32_t TDAPIProtocol::readMapBegin(TType& keyType, TType& valType, uint32_t& size) {
    return TJSONProtocol::readMapBegin(keyType, valType, size);
}

uint32_t TDAPIProtocol::readMapEnd() {
    return TJSONProtocol::readMapEnd();
}

uint32_t TDAPIProtocol::readListBegin(TType& elemType, uint32_t& size) {
    elemType = apache::thrift::protocol::T_STRUCT;
    size = static_cast<uint32_t>(1); // hack for now
    return readJSONArrayStart();
}

uint32_t TDAPIProtocol::readListEnd() {
    return readJSONArrayEnd();
}

uint32_t TDAPIProtocol::readSetBegin(TType& elemType, uint32_t& size) {
    elemType = apache::thrift::protocol::T_STRUCT;
    size = static_cast<uint32_t>(1); // hack for now
    return readJSONArrayStart();
}

uint32_t TDAPIProtocol::readSetEnd() {
    return readJSONArrayEnd();
}

uint32_t TDAPIProtocol::readBool(bool& value) {
    return TJSONProtocol::readBool(value);
}

uint32_t TDAPIProtocol::readByte(int8_t& byte) {
    return TJSONProtocol::readByte(byte);
}

uint32_t TDAPIProtocol::readI16(int16_t& i16) {
    return TJSONProtocol::readI16(i16);
}

uint32_t TDAPIProtocol::readI32(int32_t& i32) {
    return TJSONProtocol::readI32(i32);
}

uint32_t TDAPIProtocol::readI64(int64_t& i64) {
    return TJSONProtocol::readI64(i64);
}

uint32_t TDAPIProtocol::readDouble(double& dub) {
    return TJSONProtocol::readDouble(dub);
}

uint32_t TDAPIProtocol::readString(std::string &str) {
    uint32_t result = context_->read(reader_);
    uint8_t ch = reader_.peek();
    if (ch == kJSONObjectStart) {
        return result + consumeObject(str);
    }
    else if (ch == kJSONArrayStart) {
        return result + consumeArray(str);
    }
    else if (ch == kBoolVal[0] || ch == kBoolVal[2]) {
        return result + consumeBool(str);
    }
    else if (ch == kBoolVal[1] || ch == kBoolVal[3]) {
        return result + consumeBool(str);
    }
    return readJSONString(str, true);
}

uint32_t TDAPIProtocol::readBinary(std::string &str) {
    return TJSONProtocol::readBinary(str);
}

uint32_t TDAPIProtocol::writeMessageBegin(const std::string& name, const TMessageType messageType, const int32_t seqid, const std::string& serviceName) {
    callName = name;
    this->serviceName = serviceName;
	uint32_t result = 0;
    result += writeBasicString("v=");
    result += writeBasicDouble(kDAPIVersion);
    result += writeBasicString("&p=");
	
	// short-circuit the pseudo-DAPI wrapper if performing a raw DAPI call
	if (serviceName == "DAPI")
	{
		startedDAPIRequest = true;
		return 0; // This is void in C#...
	}
	
	// Since most components in the DAPI protocol are
	// not ordered, just pass in basic data
	
	const TType defaultType = apache::thrift::protocol::T_VOID;
	const int16_t defaultFieldId = -1;
	
	startedWrite = true;
	
	writeStructBegin("");
	
	// TODO bmorishita - Need to have a discussion about how the UserToken, AppToken, and stringToken relate
	// Previously they were all parsed sequentially, but this doesn't make sense if multiple tokens have been filled.
	// If only one should be active at a time, we need to enforce that logic through accessors.
	//
	// Then clean up this code if possible!
	
	// DAPIRequest {
	//   userToken : { ... },
	if(serviceName == "Track")
	{
		UserToken userToken;
		ZyngaAPI::DapiMgr::Get();
		ZyngaAPI::AuthMgr::Get().GetUserToken(userToken);
		WriteUserToken(userToken);
	}
	else if (serviceName == "Auth" || serviceName == "friends")
	{
		AppToken appToken;
		if(ZyngaAPI::AuthMgr::Get().GetAppToken(appToken))
		{
			WriteAppToken(appToken);
		}
	}
	else if (serviceName == "identities")
	{
		string stringToken;
		if(ZyngaAPI::AuthMgr::Get().GetStringToken(stringToken))
		{
			WriteStringToken(stringToken);
		}
	}
	else if (bUserTokenSet)
	{
		WriteUserToken(userToken);
	}
	//   appToken : { ... },
	else if (bAppTokenSet)
	{
		WriteAppToken(appToken);
	}
	//   stringToken : "...",
	else if (stringToken != "")
	{
		WriteStringToken(stringToken);
	}
	
	//    calls : [ ...
	writeFieldBegin("calls", defaultType, defaultFieldId);
	
	writeListBegin(defaultType, defaultFieldId); // both params unused
	
	//        { 'method' : name,
	writeStructBegin("");
	writeFieldBegin("method", defaultType, defaultFieldId);
	
	string lowercaseServiceName = serviceName;
	std::transform(lowercaseServiceName.begin(), lowercaseServiceName.end(), lowercaseServiceName.begin(), ::tolower);
	writeString(lowercaseServiceName + "." + name);
	writeFieldEnd();
	
	//          'args', { ...
	writeFieldBegin("argz", defaultType, defaultFieldId);
	startedArguments = true;
	

	
	// The caller will then write out method specific arguments as dictated by generated code
    return result;
}

uint32_t TDAPIProtocol::writeMessageEnd() {
	// short-circuit the message end if performing a raw DAPI call
	if (startedDAPIRequest == true)
	{
		startedDAPIRequest = false;
		return 0;
	}
	
	//       ...}
	writeFieldEnd();
	
	//   ...} ]
	writeStructEnd();
	writeListEnd();
	writeFieldEnd();
	
	// } // end DAPIRequest
	writeStructEnd();
	startedArguments = false;
	wroteMethodArgumentPrefixData = false;
	return 0;
}

uint32_t TDAPIProtocol::writeStructBegin(const char* name) {
    (void) name;
    if (!startedWrite) {
        return 0;
    }
	
	int nReturnValue = writeJSONObjectStart();
	
	// This conditional state check sneakily allows us to add to prefix data into the
	// method arguments without modifying the generator code.
	if(startedArguments && !wroteMethodArgumentPrefixData)
	{
		nReturnValue += writeMethodArgumentPrefixData();
	}
    return nReturnValue;
}

uint32_t TDAPIProtocol::writeStructEnd() {
    if (!startedWrite) {
        return 0;
    }
    if (contexts_.size() == 0) {
        return 0;
    }
    return writeJSONObjectEnd();
}

uint32_t TDAPIProtocol::writeFieldBegin(const char* name, const TType fieldType, const int16_t fieldId) {
    if (!startedWrite) {
        (void) name;
        startedWrite = true;
        return 0;
    }
	
	string fname = name;
	
	// 'class_' is the zdk alias for 'class' -- Thrift reserves class as a keyword and cannot
	// be used as an IDL field name, forcing us to use this "alias".
	if(strcmp(name, "class_") == 0)
	{
		fname = "class";
	}
    if (!startedArguments && DAPIAliases.find(fname) != DAPIAliases.end()) {
        fname = DAPIAliases[fname];
    }
	
    return writeString(fname);
}

uint32_t TDAPIProtocol::writeFieldEnd() {
    return 0;
}

uint32_t TDAPIProtocol::writeFieldStop() {
    return 0;
}

uint32_t TDAPIProtocol::writeMapBegin(const TType keyType, const TType valType, const uint32_t size) {
	return writeJSONObjectStart();
}

uint32_t TDAPIProtocol::writeMapEnd() {
    return writeJSONObjectEnd();
}

uint32_t TDAPIProtocol::writeListBegin(const TType elemType, const uint32_t size) {
    return writeJSONArrayStart();
}

uint32_t TDAPIProtocol::writeListEnd() {
    return writeJSONArrayEnd();
}

uint32_t TDAPIProtocol::writeSetBegin(const TType elemType, const uint32_t size) {
    return writeJSONArrayStart();
}

uint32_t TDAPIProtocol::writeSetEnd() {
    return writeJSONArrayEnd();
}

uint32_t TDAPIProtocol::writeBool(const bool value) {
    return TJSONProtocol::writeBool(value);
}

uint32_t TDAPIProtocol::writeByte(const int8_t byte) {
    return TJSONProtocol::writeByte(byte);
}

uint32_t TDAPIProtocol::writeI16(const int16_t i16) {
    return TJSONProtocol::writeI16(i16);
}

uint32_t TDAPIProtocol::writeI32(const int32_t i32) {
    return TJSONProtocol::writeI32(i32);
}

uint32_t TDAPIProtocol::writeI64(const int64_t i64) {
    return TJSONProtocol::writeI64(i64);
}

uint32_t TDAPIProtocol::writeDouble(const double dub) {
    return TJSONProtocol::writeDouble(dub);
}

uint32_t TDAPIProtocol::writeString(const std::string& str) {
    return TJSONProtocol::writeString(str);
}

uint32_t TDAPIProtocol::writeBinary(const std::string& str) {
    return TJSONProtocol::writeBinary(str);
}

uint32_t TDAPIProtocol::skip(TType type) {
    return TJSONProtocol::skip(type);
}

uint32_t TDAPIProtocol::consumeObject(std::string &str) {
    uint32_t result = 0;
    std::stack<uint8_t> charStack;
    uint8_t currChar = reader_.read();
    ++result;
    charStack.push(currChar);
    str += currChar;

    while (charStack.size() > 0) {
        currChar = reader_.read();
        ++result;
        str += currChar;
        if (currChar == kJSONObjectEnd) {
            charStack.pop();
        }
        else if (currChar == kJSONObjectStart) {
            charStack.push(currChar);
        }
    }
    return result;
}

uint32_t TDAPIProtocol::consumeArray(std::string &str) {
    uint32_t result = 0;
    str.clear();
    std::stack<uint8_t> charStack;
    uint8_t currChar = reader_.read();
    ++result;
    charStack.push(currChar);
    str += currChar;

    while (charStack.size() > 0) {
        currChar = reader_.read();
        ++result;
        str += currChar;
        if (currChar == kJSONArrayEnd) {
            charStack.pop();
        }
        else if (currChar == kJSONArrayStart) {
            charStack.push(currChar);
        }
    }
    return result;
}

uint32_t TDAPIProtocol::consumeBool(std::string &str) {
    uint32_t result = 0;
    uint8_t currChar = 0;
    str.clear();

    while (currChar != kBoolEnd[0] && currChar != kBoolEnd[1]) {
        currChar = reader_.read();
        ++result;
        str += currChar;
    }
    return result;
}

uint32_t TDAPIProtocol::writeBasicString(const std::string &str) {
    uint32_t result = 0;
    std::string::const_iterator iter(str.begin());
    std::string::const_iterator end(str.end());
    while (iter != end) {
        uint8_t ch = *iter++;
        trans_->write(&ch, 1);
        result++;
    }
    return result;
}

uint32_t TDAPIProtocol::writeBasicDouble(const double num) {
    uint32_t result = 0;
    std::string val(boost::lexical_cast<std::string>(num));

    // Normalize output of boost::lexical_cast for NaNs and Infinities
    bool special = false;
    switch (val[0]) {
    case 'N':
    case 'n':
        val = kThriftNan;
        special = true;
        break;
    case 'I':
    case 'i':
        val = kThriftInfinity;
        special = true;
        break;
    case '-':
        if ((val[1] == 'I') || (val[1] == 'i')) {
            val = kThriftNegativeInfinity;
            special = true;
        }
        break;
    }

    bool escapeNum = special;
    if (escapeNum) {
        trans_->write(&kStringDelimiter, 1);
        result += 1;
    }
    if(val.length() > (std::numeric_limits<uint32_t>::max)())
        throw TProtocolException(TProtocolException::SIZE_LIMIT);
    trans_->write((const uint8_t *)val.c_str(), static_cast<uint32_t>(val.length()));
    result += static_cast<uint32_t>(val.length());
    if (escapeNum) {
        trans_->write(&kStringDelimiter, 1);
        result += 1;
    }
    return result;
}

bool TDAPIProtocol::isAtEndOfStream(TJSONProtocol::LookaheadReader &reader) {
    bool result = false;
    if (startedParse && contexts_.size() == 0) {
        //printf("Reached End of Stream");
        result = true;
    }
    return result;
}
	
void TDAPIProtocol::SetAppToken(AppToken& appToken)
{
	bAppTokenSet = true;
	this->appToken = appToken;
}
	
void TDAPIProtocol::SetUserToken(UserToken& userToken)
{
	bUserTokenSet = true;
	this->userToken = userToken;
}
	
// Add argument data to prefix a module's generated method arguments section
uint32_t TDAPIProtocol::writeMethodArgumentPrefixData()
{
	uint32_t xfer = 0;
	
	const TType defaultType = apache::thrift::protocol::T_VOID;
	const int16_t defaultFieldId = -1;
	
	// Track method calls must be prefixed with required clientId and zid
	// which are maintained in zynga api for the client's convenience.
	if(serviceName == "Track")
	{
		UserToken userToken;
		if(ZyngaAPI::AuthMgr::Get().GetUserToken(userToken))
		{
			xfer += writeFieldBegin("clientId", defaultType, defaultFieldId);
			xfer += writeString("2");
			xfer += writeFieldEnd();
			
			xfer += writeFieldBegin("zid", defaultType, defaultFieldId);
			xfer += writeString(userToken.userId);
			xfer += writeFieldEnd();
		}
	}
	
	wroteMethodArgumentPrefixData = true;
	return xfer;
}
	
void TDAPIProtocol::WriteAppToken(const AppToken& appToken)
{
	// For writing we don't care about type and field id
	const TType defaultType = apache::thrift::protocol::T_VOID;
	const int16_t defaultFieldId = -1;
	
	writeFieldBegin("appToken", defaultType, defaultFieldId);
	writeStructBegin("");

	// Iterate over appToken members and write out fields for them
	writeFieldBegin("appId", defaultType, defaultFieldId);
	writeString(appToken.appId);
	writeFieldEnd();

	writeFieldBegin("snId", defaultType, defaultFieldId);
	writeString(appToken.snId);
	writeFieldEnd();

	writeFieldBegin("secret", defaultType, defaultFieldId);
	writeString(appToken.secret);
	writeFieldEnd();

	writeStructEnd();
	writeFieldEnd();
}

void TDAPIProtocol::WriteUserToken(const UserToken& userToken)
{
	// For writing we don't care about type and field id
	const TType defaultType = apache::thrift::protocol::T_VOID;
	const int16_t defaultFieldId = -1;
	
	writeFieldBegin("userToken", defaultType, defaultFieldId);
	writeStructBegin("");
	
	// Iterate over the UserToken fields and write them out
	/*
	 UserToken:
	 std::string appId;
	 std::string snId;
	 std::string userId;
	 Session session;
	 {
	 std::string access_token;
	 std::string user_id;
	 std::string oauth_token;
	 std::string oauth_token_secret;
	 }
	 */
	writeFieldBegin("appId", defaultType, defaultFieldId);
	writeString(userToken.appId);
	writeFieldEnd();
	
	writeFieldBegin("snId", defaultType, defaultFieldId);
	writeString(userToken.snId);
	writeFieldEnd();
	
	writeFieldBegin("userId", defaultType, defaultFieldId);
	writeString(userToken.userId);
	writeFieldEnd();
	
	writeFieldBegin("session", defaultType, defaultFieldId);
	writeString(userToken.session.access_token);
	writeFieldEnd();
	
	writeStructEnd();
	writeFieldEnd();
}

void TDAPIProtocol::WriteStringToken(const string& stringToken)
{
	// For writing we don't care about type and field id
	const TType defaultType = apache::thrift::protocol::T_VOID;
	const int16_t defaultFieldId = -1;
	
	writeFieldBegin("stringToken", defaultType, defaultFieldId);
	writeString(stringToken);
	writeFieldEnd();
}

TSimpleProtocol::TSimpleProtocol(boost::shared_ptr<TTransport> ptrans) :
    TVirtualProtocol<TSimpleProtocol>(ptrans),
    trans_(ptrans.get()) {
}

TSimpleProtocol::~TSimpleProtocol() {}

uint32_t TSimpleProtocol::readMessageBegin(std::string& name, TMessageType& messageType, int32_t& seqid) {
    (void) name;
    messageType = T_CALL;
    seqid = static_cast<int32_t>(0);
    return 0;
}

uint32_t TSimpleProtocol::readString(std::string &str) {
    uint32_t result = 0;
    uint8_t currChar;
    str.clear();

    std::vector<uint8_t> brace_stack_;
    
    brace_stack_.push_back(currChar);
    trans_->readAll(&currChar, 1);
    ++result;
    str += currChar;

    while (!brace_stack_.empty()) {
        trans_->readAll(&currChar, 1);
        ++result;
        str += currChar;
        switch (currChar) {
            case '}':
                brace_stack_.pop_back();
                break;
            case '{':
                brace_stack_.push_back(currChar);
                break;
        }
    }
    finishedRead = true;
    return result;
}

uint32_t TSimpleProtocol::readFieldBegin(std::string& name, TType& fieldType, int16_t& fieldId) {
    (void) name;
    uint32_t result = 0;
    fieldId = static_cast<int16_t>(0);
    
    fieldType = apache::thrift::protocol::T_STOP;
    if (!finishedRead) {
    	fieldType = apache::thrift::protocol::T_STRING;
    }
    
    return result;
}

uint32_t TSimpleProtocol::writeMessageBegin(const std::string& name, const TMessageType messageType, const int32_t seqid) {
    (void) name;
    uint32_t result = 0;
    result += writeString("v=");
    result += writeDouble(kDAPIVersion);
    result += writeString("&p=");
    return result;
}
	
uint32_t TSimpleProtocol::writeMessageBegin(const std::string& name, const TMessageType messageType, const int32_t seqid, const std::string& serviceName) {
	return writeMessageBegin(name, messageType, seqid);
}

uint32_t TSimpleProtocol::writeDouble(const double num) {
    uint32_t result = 0;
    std::string val(boost::lexical_cast<std::string>(num));

    // Normalize output of boost::lexical_cast for NaNs and Infinities
    bool special = false;
    switch (val[0]) {
    case 'N':
    case 'n':
        val = kThriftNan;
        special = true;
        break;
    case 'I':
    case 'i':
        val = kThriftInfinity;
        special = true;
        break;
    case '-':
        if ((val[1] == 'I') || (val[1] == 'i')) {
            val = kThriftNegativeInfinity;
            special = true;
        }
        break;
    }

    bool escapeNum = special;
    if (escapeNum) {
        trans_->write(&kStringDelimiter, 1);
        result += 1;
    }
    if(val.length() > (std::numeric_limits<uint32_t>::max)())
        throw TProtocolException(TProtocolException::SIZE_LIMIT);
    trans_->write((const uint8_t *)val.c_str(), static_cast<uint32_t>(val.length()));
    result += static_cast<uint32_t>(val.length());
    if (escapeNum) {
        trans_->write(&kStringDelimiter, 1);
        result += 1;
    }
    return result;
}

uint32_t TSimpleProtocol::writeString(const std::string &str) {
    uint32_t result = 0;
    std::string::const_iterator iter(str.begin());
    std::string::const_iterator end(str.end());
    while (iter != end) {
        result += writeChar(*iter++);
    }
    return result;
}

uint32_t TSimpleProtocol::writeChar(uint8_t ch) {
    trans_->write(&ch, 1);
    return 1;
}

// uint32_t TSimpleProtocol::readMessageBegin(std::string& name, TMessageType& messageType, int32_t& seqid) { return 0; }
uint32_t TSimpleProtocol::readMessageEnd() { return 0; }
uint32_t TSimpleProtocol::readStructBegin(std::string& name) { return 0; }
uint32_t TSimpleProtocol::readStructEnd() { return 0; }
// uint32_t TSimpleProtocol::readFieldBegin(std::string& name, TType& fieldType, int16_t& fieldId) { return 0; }
uint32_t TSimpleProtocol::readFieldEnd() { return 0; }
uint32_t TSimpleProtocol::readMapBegin(TType& keyType, TType& valType, uint32_t& size) { return 0; }
uint32_t TSimpleProtocol::readMapEnd() { return 0; }
uint32_t TSimpleProtocol::readListBegin(TType& elemType, uint32_t& size) { return 0; }
uint32_t TSimpleProtocol::readListEnd() { return 0; }
uint32_t TSimpleProtocol::readSetBegin(TType& elemType, uint32_t& size) { return 0; }
uint32_t TSimpleProtocol::readSetEnd() { return 0; }
uint32_t TSimpleProtocol::readBool(bool& value) { return 0; }
uint32_t TSimpleProtocol::readBool(std::vector<bool>::reference value) { return 0; }
uint32_t TSimpleProtocol::readByte(int8_t& byte) { return 0; }
uint32_t TSimpleProtocol::readI16(int16_t& i16) { return 0; }
uint32_t TSimpleProtocol::readI32(int32_t& i32) { return 0; }
uint32_t TSimpleProtocol::readI64(int64_t& i64) { return 0; }
uint32_t TSimpleProtocol::readDouble(double& dub) { return 0; }
// uint32_t TSimpleProtocol::readString(std::string& str) { return 0; }
uint32_t TSimpleProtocol::readBinary(std::string& str) { return 0; }
// uint32_t TSimpleProtocol::writeMessageBegin(const std::string& name, const TMessageType messageType, const int32_t seqid) { return 0; }
uint32_t TSimpleProtocol::writeMessageEnd() { return 0; }
uint32_t TSimpleProtocol::writeStructBegin(const char* name) { return 0; }
uint32_t TSimpleProtocol::writeStructEnd() { return 0; }
uint32_t TSimpleProtocol::writeFieldBegin(const char* name, const TType fieldType, const int16_t fieldId) { return 0; }
uint32_t TSimpleProtocol::writeFieldEnd() { return 0; }
uint32_t TSimpleProtocol::writeFieldStop() { return 0; }
uint32_t TSimpleProtocol::writeMapBegin(const TType keyType, const TType valType, const uint32_t size) { return 0; }
uint32_t TSimpleProtocol::writeMapEnd() { return 0; }
uint32_t TSimpleProtocol::writeListBegin(const TType elemType, const uint32_t size) { return 0; }
uint32_t TSimpleProtocol::writeListEnd() { return 0; }
uint32_t TSimpleProtocol::writeSetBegin(const TType elemType, const uint32_t size) { return 0; }
uint32_t TSimpleProtocol::writeSetEnd() { return 0; }
uint32_t TSimpleProtocol::writeBool(const bool value) { return 0; }
uint32_t TSimpleProtocol::writeByte(const int8_t byte) { return 0; }
uint32_t TSimpleProtocol::writeI16(const int16_t i16) { return 0; }
uint32_t TSimpleProtocol::writeI32(const int32_t i32) { return 0; }
uint32_t TSimpleProtocol::writeI64(const int64_t i64) { return 0; }
// uint32_t TSimpleProtocol::writeDouble(const double dub) { return 0; }
// uint32_t TSimpleProtocol::writeString(const std::string& str) { return 0; }
uint32_t TSimpleProtocol::writeBinary(const std::string& str) { return 0; }
uint32_t TSimpleProtocol::skip(TType type) { return 0; }

}}} // apache::thrift::protocol
