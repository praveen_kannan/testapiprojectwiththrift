#!/bin/sh

set -e
set -u

echo "Copy resources to ${ZDK_BUILD_TMP_DIR}"

if [ -d "${TARGET_BUILD_DIR}/${PRODUCT_NAME}.bundle" ]
then
	mkdir -p "${ZDK_BUILD_TMP_DIR}/resources/${PROJECT_NAME}"
	cp -a "${TARGET_BUILD_DIR}/${PRODUCT_NAME}.bundle" "${ZDK_BUILD_TMP_DIR}/resources/${PROJECT_NAME}/${PRODUCT_NAME}.bundle"
fi
