#! /bin/sh

iosdir=$1
proj=$2
targetloc=$3
target=$4

logfile=/var/tmp/client-mobile_build_$target.txt
echo "Building unit test $target.app"
GHUNIT_CLI=1 xcodebuild -project $iosdir/$proj -target $target -configuration Debug -sdk iphonesimulator clean > /dev/null 2>&1
GHUNIT_CLI=1 xcodebuild -project $iosdir/$proj -target $target -configuration Debug -sdk iphonesimulator build > $logfile 2>&1
if grep -q "BUILD SUCCEEDED" $logfile
then
	echo "Status: OK"
	rm -rf $logfile > /dev/null 2>&1
else
	echo "Status: FAILED. see $logfile"
	exit 1
fi
echo "Done Building unit test $target.app"

logfile=/var/tmp/client-mobile_run_$target.txt
echo "Running unit test $target.app"
$iosdir/build-scripts/ios-sim launch $iosdir/$targetloc/build/Debug-iphonesimulator/$target.app --sdk 6.0 --family iphone --setenv GHUNIT_AUTORUN=1 --setenv GHUNIT_AUTOEXIT=1 --setenv GHUNIT_CLI=1 --setenv UIApplicationExitsOnSuspend=YES 2>&1 | tee $logfile | grep -e "\[DEBUG\]  " -e "Starting" -e "GHTestFailureException" -e "Executed"
failcnt=`grep -c "0 failure" $logfile`
if [ $failcnt -ne 0 ]
then
	echo "Status: OK"
	rm -rf $logfile > /dev/null 2>&1
else
	echo "Status: FAILED. see $logfile"
fi
echo "Done Running unit test $target.app"
