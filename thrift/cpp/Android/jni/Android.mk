# Workaround for http://gcc.gnu.org/bugzilla/show_bug.cgi?id=42748
TARGET_CFLAGS += -Wno-psabi

LOCAL_PATH := $(call my-dir)/../../thrift
MY_THRIFT_PATH := $(LOCAL_PATH)
MY_BOOST_PATH := $(call my-dir)/../../../../boost/Android
MY_OPENSSL_PATH := $(call my-dir)/../../../../openssl/Android
MY_EXPORT_PATH := $(LOCAL_PATH)/../../../build/Android/exported

# Convenience function to copy files, while creating missing directories if needed
# TODO: RM the includes folder before calling
define COPY_FILE
$(1): ;
	$(eval destination = $(patsubst $(MY_THRIFT_PATH)/%, $(MY_EXPORT_PATH)/thrift/includes/$(2)/%, $(1)))
	$(eval destinationDir = $(dir ${destination}))
	$(shell test -d $(destinationDir) || mkdir -p $(destinationDir))
	$(shell rsync -drHav $(1) $(destinationDir))
endef

define COPY_BOOST_HEADERS_TO_EXPORT
: ;
	$(shell rsync -drHav $(MY_BOOST_PATH)/include/boost/ $(MY_EXPORT_PATH)/thrift/includes/boost/)
endef


getSourceFiles = \
	$(patsubst $(LOCAL_PATH)/%, %, $(wildcard $(LOCAL_PATH)/**/*.cpp) $(wildcard $(LOCAL_PATH)/*.cpp))

MY_SRC_FILES = $(call getSourceFiles)

# Remove problematic source files -- these are also disabled in the xcode builds
MY_SRC_FILES := $(filter-out async/TEvhttpClientChannel.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out async/TEvhttpServer.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out concurrency/BoostMutex.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out concurrency/BoostMonitor.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out concurrency/BoostThreadFactory.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out protocol/TDAPIProtocol.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out qt/TQIODeviceTransport.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out qt/TQTcpServer.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out server/TNonblockingServer.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out transport/TZlibTransport.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out windows/GetTimeOfDay.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out windows/SocketPair.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out windows/StdAfx.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out windows/TWinsockSingleton.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out windows/WinFcntl.cpp,$(MY_SRC_FILES))

# Remove problematic source files -- these are only disabled for osx scode builds

# Remove problematic source files -- these are only disabled for android (for convenience)
MY_SRC_FILES := $(filter-out transport/TFileTransport.cpp,$(MY_SRC_FILES))

MY_HEADER_PATHS := $(LOCAL_PATH) \
				$(LOCAL_PATH)/.. \
				$(MY_OPENSSL_PATH)/include \
				$(MY_BOOST_PATH)/include \
				$(MY_BOOST_PATH)/include/boost \
				$(MY_BOOST_PATH)/include/boost/tr1
#$(warning MY_HEADER_PATHS are: $(MY_HEADER_PATHS))


# COPY THE THRIFT HEADERS TO THE EXPORT FOLDER
MY_EXPORT_HEADER_PATHS = $(sort $(dir $(wildcard $(LOCAL_PATH)/*/ $(LOCAL_PATH)/*/*/)))
MY_EXPORT_HEADER_FILES = $(foreach path, $(MY_EXPORT_HEADER_PATHS)/, $(wildcard $(path)*.h))
$(foreach srcFilePath,$(MY_EXPORT_HEADER_FILES),$(call COPY_FILE,$(srcFilePath),thrift))

#COPY THE BOOST HEADERS TO THE EXPORT FOLDER
$(call COPY_BOOST_HEADERS_TO_EXPORT)


include $(CLEAR_VARS)
LOCAL_MODULE := thrift_static
LOCAL_SRC_FILES := $(MY_SRC_FILES)
LOCAL_LDLIBS := -llog -lstdc++ $(MY_OPENSSL_PATH)/libssl.so $(MY_OPENSSL_PATH)/libcrypto.so
LOCAL_C_INCLUDES := $(MY_HEADER_PATHS)
LOCAL_EXPORT_C_INCLUDES := $(MY_HEADER_PATHS)
LOCAL_CFLAGS    := -DZDK_ANDROID_PLATFORM -DHAVE_CONFIG_H
LOCAL_CPPFLAGS := -fexceptions -frtti
LOCAL_EXPORT_LDLIBS := -llog $(MY_OPENSSL_PATH)/libssl.so $(MY_OPENSSL_PATH)/libcrypto.so
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := thrift_shared
LOCAL_SRC_FILES := $(MY_SRC_FILES)
LOCAL_LDLIBS := -llog -lstdc++ $(MY_OPENSSL_PATH)/libssl.so $(MY_OPENSSL_PATH)/libcrypto.so
LOCAL_C_INCLUDES := $(MY_HEADER_PATHS)
LOCAL_EXPORT_C_INCLUDES := $(MY_HEADER_PATHS)
LOCAL_CFLAGS    := -DZDK_ANDROID_PLATFORM -DHAVE_CONFIG_H
LOCAL_CPPFLAGS := -fexceptions -frtti
LOCAL_EXPORT_LDLIBS := -llog $(MY_OPENSSL_PATH)/libssl.so $(MY_OPENSSL_PATH)/libcrypto.so
include $(BUILD_SHARED_LIBRARY)