/**
 * Autogenerated by Thrift Compiler (1.0.0-dev)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#include "auth_types.h"

#include <algorithm>



const char* AuthRegisterDeviceArgs::ascii_fingerprint = "EFB929595D312AC8F305D5A794CFEDA1";
const uint8_t AuthRegisterDeviceArgs::binary_fingerprint[16] = {0xEF,0xB9,0x29,0x59,0x5D,0x31,0x2A,0xC8,0xF3,0x05,0xD5,0xA7,0x94,0xCF,0xED,0xA1};

uint32_t AuthRegisterDeviceArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->password);
          this->__isset.password = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t AuthRegisterDeviceArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("AuthRegisterDeviceArgs");

  xfer += oprot->writeFieldBegin("password", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->password);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(AuthRegisterDeviceArgs &a, AuthRegisterDeviceArgs &b) {
  using ::std::swap;
  swap(a.password, b.password);
  swap(a.__isset, b.__isset);
}

const char* AuthIssueTokenArgs::ascii_fingerprint = "E86E2BAE7070F7DF8856742F6E44A960";
const uint8_t AuthIssueTokenArgs::binary_fingerprint[16] = {0xE8,0x6E,0x2B,0xAE,0x70,0x70,0xF7,0xDF,0x88,0x56,0x74,0x2F,0x6E,0x44,0xA9,0x60};

uint32_t AuthIssueTokenArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->password);
          this->__isset.password = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->appId);
          this->__isset.appId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->email);
          this->__isset.email = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 4:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->username);
          this->__isset.username = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 5:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->userId);
          this->__isset.userId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 6:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->zid);
          this->__isset.zid = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t AuthIssueTokenArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("AuthIssueTokenArgs");

  xfer += oprot->writeFieldBegin("password", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->password);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("appId", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->appId);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.email) {
    xfer += oprot->writeFieldBegin("email", ::apache::thrift::protocol::T_STRING, 3);
    xfer += oprot->writeString(this->email);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.username) {
    xfer += oprot->writeFieldBegin("username", ::apache::thrift::protocol::T_STRING, 4);
    xfer += oprot->writeString(this->username);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.userId) {
    xfer += oprot->writeFieldBegin("userId", ::apache::thrift::protocol::T_STRING, 5);
    xfer += oprot->writeString(this->userId);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.zid) {
    xfer += oprot->writeFieldBegin("zid", ::apache::thrift::protocol::T_STRING, 6);
    xfer += oprot->writeString(this->zid);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(AuthIssueTokenArgs &a, AuthIssueTokenArgs &b) {
  using ::std::swap;
  swap(a.password, b.password);
  swap(a.appId, b.appId);
  swap(a.email, b.email);
  swap(a.username, b.username);
  swap(a.userId, b.userId);
  swap(a.zid, b.zid);
  swap(a.__isset, b.__isset);
}


