/**
 * Autogenerated by Thrift Compiler (1.0.0-dev)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#include "experiment_types.h"

#include <algorithm>



const char* ExperimentGetArgs::ascii_fingerprint = "B9B01DEDBBFE8A06CA291180221E421B";
const uint8_t ExperimentGetArgs::binary_fingerprint[16] = {0xB9,0xB0,0x1D,0xED,0xBB,0xFE,0x8A,0x06,0xCA,0x29,0x11,0x80,0x22,0x1E,0x42,0x1B};

uint32_t ExperimentGetArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->experimentName);
          this->__isset.experimentName = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->experimentVer);
          this->__isset.experimentVer = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->overrideAppId);
          this->__isset.overrideAppId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 4:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->overrideSnid);
          this->__isset.overrideSnid = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 5:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->overrideLookupAppId);
          this->__isset.overrideLookupAppId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 6:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->partitionId);
          this->__isset.partitionId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t ExperimentGetArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("ExperimentGetArgs");

  xfer += oprot->writeFieldBegin("experimentName", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->experimentName);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.experimentVer) {
    xfer += oprot->writeFieldBegin("experimentVer", ::apache::thrift::protocol::T_STRING, 2);
    xfer += oprot->writeString(this->experimentVer);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideAppId) {
    xfer += oprot->writeFieldBegin("overrideAppId", ::apache::thrift::protocol::T_STRING, 3);
    xfer += oprot->writeString(this->overrideAppId);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideSnid) {
    xfer += oprot->writeFieldBegin("overrideSnid", ::apache::thrift::protocol::T_STRING, 4);
    xfer += oprot->writeString(this->overrideSnid);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideLookupAppId) {
    xfer += oprot->writeFieldBegin("overrideLookupAppId", ::apache::thrift::protocol::T_I32, 5);
    xfer += oprot->writeI32(this->overrideLookupAppId);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.partitionId) {
    xfer += oprot->writeFieldBegin("partitionId", ::apache::thrift::protocol::T_I32, 6);
    xfer += oprot->writeI32(this->partitionId);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(ExperimentGetArgs &a, ExperimentGetArgs &b) {
  using ::std::swap;
  swap(a.experimentName, b.experimentName);
  swap(a.experimentVer, b.experimentVer);
  swap(a.overrideAppId, b.overrideAppId);
  swap(a.overrideSnid, b.overrideSnid);
  swap(a.overrideLookupAppId, b.overrideLookupAppId);
  swap(a.partitionId, b.partitionId);
  swap(a.__isset, b.__isset);
}

const char* ExperimentGetMultiArgs::ascii_fingerprint = "DE1F9CC3E3E729A3C402BDCA54047BDB";
const uint8_t ExperimentGetMultiArgs::binary_fingerprint[16] = {0xDE,0x1F,0x9C,0xC3,0xE3,0xE7,0x29,0xA3,0xC4,0x02,0xBD,0xCA,0x54,0x04,0x7B,0xDB};

uint32_t ExperimentGetMultiArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_LIST) {
          {
            this->experiments.clear();
            uint32_t _size0;
            ::apache::thrift::protocol::TType _etype3;
            xfer += iprot->readListBegin(_etype3, _size0);
            this->experiments.resize(_size0);
            uint32_t _i4;
            for (_i4 = 0; _i4 < _size0; ++_i4)
            {
              xfer += iprot->readString(this->experiments[_i4]);
            }
            xfer += iprot->readListEnd();
          }
          this->__isset.experiments = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->overrideAppId);
          this->__isset.overrideAppId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->overrideSnid);
          this->__isset.overrideSnid = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 4:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->overrideLookupAppId);
          this->__isset.overrideLookupAppId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 5:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->partitionId);
          this->__isset.partitionId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t ExperimentGetMultiArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("ExperimentGetMultiArgs");

  xfer += oprot->writeFieldBegin("experiments", ::apache::thrift::protocol::T_LIST, 1);
  {
    xfer += oprot->writeListBegin(::apache::thrift::protocol::T_STRING, static_cast<uint32_t>(this->experiments.size()));
    std::vector<std::string> ::const_iterator _iter5;
    for (_iter5 = this->experiments.begin(); _iter5 != this->experiments.end(); ++_iter5)
    {
      xfer += oprot->writeString((*_iter5));
    }
    xfer += oprot->writeListEnd();
  }
  xfer += oprot->writeFieldEnd();

  if (this->__isset.overrideAppId) {
    xfer += oprot->writeFieldBegin("overrideAppId", ::apache::thrift::protocol::T_STRING, 2);
    xfer += oprot->writeString(this->overrideAppId);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideSnid) {
    xfer += oprot->writeFieldBegin("overrideSnid", ::apache::thrift::protocol::T_STRING, 3);
    xfer += oprot->writeString(this->overrideSnid);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideLookupAppId) {
    xfer += oprot->writeFieldBegin("overrideLookupAppId", ::apache::thrift::protocol::T_I32, 4);
    xfer += oprot->writeI32(this->overrideLookupAppId);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.partitionId) {
    xfer += oprot->writeFieldBegin("partitionId", ::apache::thrift::protocol::T_I32, 5);
    xfer += oprot->writeI32(this->partitionId);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(ExperimentGetMultiArgs &a, ExperimentGetMultiArgs &b) {
  using ::std::swap;
  swap(a.experiments, b.experiments);
  swap(a.overrideAppId, b.overrideAppId);
  swap(a.overrideSnid, b.overrideSnid);
  swap(a.overrideLookupAppId, b.overrideLookupAppId);
  swap(a.partitionId, b.partitionId);
  swap(a.__isset, b.__isset);
}

const char* ExperimentLogAndGetArgs::ascii_fingerprint = "B9B01DEDBBFE8A06CA291180221E421B";
const uint8_t ExperimentLogAndGetArgs::binary_fingerprint[16] = {0xB9,0xB0,0x1D,0xED,0xBB,0xFE,0x8A,0x06,0xCA,0x29,0x11,0x80,0x22,0x1E,0x42,0x1B};

uint32_t ExperimentLogAndGetArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->experimentName);
          this->__isset.experimentName = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->experimentVer);
          this->__isset.experimentVer = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->overrideAppId);
          this->__isset.overrideAppId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 4:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->overrideSnid);
          this->__isset.overrideSnid = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 5:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->overrideLookupAppId);
          this->__isset.overrideLookupAppId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 6:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->partitionId);
          this->__isset.partitionId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t ExperimentLogAndGetArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("ExperimentLogAndGetArgs");

  xfer += oprot->writeFieldBegin("experimentName", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->experimentName);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.experimentVer) {
    xfer += oprot->writeFieldBegin("experimentVer", ::apache::thrift::protocol::T_STRING, 2);
    xfer += oprot->writeString(this->experimentVer);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideAppId) {
    xfer += oprot->writeFieldBegin("overrideAppId", ::apache::thrift::protocol::T_STRING, 3);
    xfer += oprot->writeString(this->overrideAppId);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideSnid) {
    xfer += oprot->writeFieldBegin("overrideSnid", ::apache::thrift::protocol::T_STRING, 4);
    xfer += oprot->writeString(this->overrideSnid);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideLookupAppId) {
    xfer += oprot->writeFieldBegin("overrideLookupAppId", ::apache::thrift::protocol::T_I32, 5);
    xfer += oprot->writeI32(this->overrideLookupAppId);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.partitionId) {
    xfer += oprot->writeFieldBegin("partitionId", ::apache::thrift::protocol::T_I32, 6);
    xfer += oprot->writeI32(this->partitionId);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(ExperimentLogAndGetArgs &a, ExperimentLogAndGetArgs &b) {
  using ::std::swap;
  swap(a.experimentName, b.experimentName);
  swap(a.experimentVer, b.experimentVer);
  swap(a.overrideAppId, b.overrideAppId);
  swap(a.overrideSnid, b.overrideSnid);
  swap(a.overrideLookupAppId, b.overrideLookupAppId);
  swap(a.partitionId, b.partitionId);
  swap(a.__isset, b.__isset);
}

const char* ExperimentExistsArgs::ascii_fingerprint = "05E28D76AE756AD25FE99639AC7145D5";
const uint8_t ExperimentExistsArgs::binary_fingerprint[16] = {0x05,0xE2,0x8D,0x76,0xAE,0x75,0x6A,0xD2,0x5F,0xE9,0x96,0x39,0xAC,0x71,0x45,0xD5};

uint32_t ExperimentExistsArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->experimentName);
          this->__isset.experimentName = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->overrideAppId);
          this->__isset.overrideAppId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->overrideSnid);
          this->__isset.overrideSnid = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 4:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->overrideLookupAppId);
          this->__isset.overrideLookupAppId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t ExperimentExistsArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("ExperimentExistsArgs");

  xfer += oprot->writeFieldBegin("experimentName", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->experimentName);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.overrideAppId) {
    xfer += oprot->writeFieldBegin("overrideAppId", ::apache::thrift::protocol::T_STRING, 2);
    xfer += oprot->writeString(this->overrideAppId);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideSnid) {
    xfer += oprot->writeFieldBegin("overrideSnid", ::apache::thrift::protocol::T_STRING, 3);
    xfer += oprot->writeString(this->overrideSnid);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideLookupAppId) {
    xfer += oprot->writeFieldBegin("overrideLookupAppId", ::apache::thrift::protocol::T_I32, 4);
    xfer += oprot->writeI32(this->overrideLookupAppId);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(ExperimentExistsArgs &a, ExperimentExistsArgs &b) {
  using ::std::swap;
  swap(a.experimentName, b.experimentName);
  swap(a.overrideAppId, b.overrideAppId);
  swap(a.overrideSnid, b.overrideSnid);
  swap(a.overrideLookupAppId, b.overrideLookupAppId);
  swap(a.__isset, b.__isset);
}

const char* ExperimentLogGoalArgs::ascii_fingerprint = "BDF9EFB1392F885301C32D89A9D4411D";
const uint8_t ExperimentLogGoalArgs::binary_fingerprint[16] = {0xBD,0xF9,0xEF,0xB1,0x39,0x2F,0x88,0x53,0x01,0xC3,0x2D,0x89,0xA9,0xD4,0x41,0x1D};

uint32_t ExperimentLogGoalArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->experimentName);
          this->__isset.experimentName = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->goal);
          this->__isset.goal = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->The);
          this->__isset.The = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 4:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->overrideAppId);
          this->__isset.overrideAppId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 5:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->overrideSnid);
          this->__isset.overrideSnid = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t ExperimentLogGoalArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("ExperimentLogGoalArgs");

  xfer += oprot->writeFieldBegin("experimentName", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->experimentName);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("goal", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->goal);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.The) {
    xfer += oprot->writeFieldBegin("The", ::apache::thrift::protocol::T_STRING, 3);
    xfer += oprot->writeString(this->The);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideAppId) {
    xfer += oprot->writeFieldBegin("overrideAppId", ::apache::thrift::protocol::T_STRING, 4);
    xfer += oprot->writeString(this->overrideAppId);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideSnid) {
    xfer += oprot->writeFieldBegin("overrideSnid", ::apache::thrift::protocol::T_STRING, 5);
    xfer += oprot->writeString(this->overrideSnid);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(ExperimentLogGoalArgs &a, ExperimentLogGoalArgs &b) {
  using ::std::swap;
  swap(a.experimentName, b.experimentName);
  swap(a.goal, b.goal);
  swap(a.The, b.The);
  swap(a.overrideAppId, b.overrideAppId);
  swap(a.overrideSnid, b.overrideSnid);
  swap(a.__isset, b.__isset);
}

const char* ExperimentGetMetadataArgs::ascii_fingerprint = "05E28D76AE756AD25FE99639AC7145D5";
const uint8_t ExperimentGetMetadataArgs::binary_fingerprint[16] = {0x05,0xE2,0x8D,0x76,0xAE,0x75,0x6A,0xD2,0x5F,0xE9,0x96,0x39,0xAC,0x71,0x45,0xD5};

uint32_t ExperimentGetMetadataArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->experimentName);
          this->__isset.experimentName = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->overrideAppId);
          this->__isset.overrideAppId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->overrideSnid);
          this->__isset.overrideSnid = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 4:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->overrideLookupAppId);
          this->__isset.overrideLookupAppId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t ExperimentGetMetadataArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("ExperimentGetMetadataArgs");

  xfer += oprot->writeFieldBegin("experimentName", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->experimentName);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.overrideAppId) {
    xfer += oprot->writeFieldBegin("overrideAppId", ::apache::thrift::protocol::T_STRING, 2);
    xfer += oprot->writeString(this->overrideAppId);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideSnid) {
    xfer += oprot->writeFieldBegin("overrideSnid", ::apache::thrift::protocol::T_STRING, 3);
    xfer += oprot->writeString(this->overrideSnid);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideLookupAppId) {
    xfer += oprot->writeFieldBegin("overrideLookupAppId", ::apache::thrift::protocol::T_I32, 4);
    xfer += oprot->writeI32(this->overrideLookupAppId);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(ExperimentGetMetadataArgs &a, ExperimentGetMetadataArgs &b) {
  using ::std::swap;
  swap(a.experimentName, b.experimentName);
  swap(a.overrideAppId, b.overrideAppId);
  swap(a.overrideSnid, b.overrideSnid);
  swap(a.overrideLookupAppId, b.overrideLookupAppId);
  swap(a.__isset, b.__isset);
}

const char* ExperimentGetDateArgs::ascii_fingerprint = "D43E8348958FD3E17C3B10D8EC589847";
const uint8_t ExperimentGetDateArgs::binary_fingerprint[16] = {0xD4,0x3E,0x83,0x48,0x95,0x8F,0xD3,0xE1,0x7C,0x3B,0x10,0xD8,0xEC,0x58,0x98,0x47};

uint32_t ExperimentGetDateArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->experimentName);
          this->__isset.experimentName = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->type);
          this->__isset.type = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->overrideAppId);
          this->__isset.overrideAppId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 4:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->overrideSnid);
          this->__isset.overrideSnid = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 5:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->overrideLookupAppId);
          this->__isset.overrideLookupAppId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t ExperimentGetDateArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("ExperimentGetDateArgs");

  xfer += oprot->writeFieldBegin("experimentName", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->experimentName);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("type", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->type);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.overrideAppId) {
    xfer += oprot->writeFieldBegin("overrideAppId", ::apache::thrift::protocol::T_STRING, 3);
    xfer += oprot->writeString(this->overrideAppId);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideSnid) {
    xfer += oprot->writeFieldBegin("overrideSnid", ::apache::thrift::protocol::T_STRING, 4);
    xfer += oprot->writeString(this->overrideSnid);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.overrideLookupAppId) {
    xfer += oprot->writeFieldBegin("overrideLookupAppId", ::apache::thrift::protocol::T_I32, 5);
    xfer += oprot->writeI32(this->overrideLookupAppId);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(ExperimentGetDateArgs &a, ExperimentGetDateArgs &b) {
  using ::std::swap;
  swap(a.experimentName, b.experimentName);
  swap(a.type, b.type);
  swap(a.overrideAppId, b.overrideAppId);
  swap(a.overrideSnid, b.overrideSnid);
  swap(a.overrideLookupAppId, b.overrideLookupAppId);
  swap(a.__isset, b.__isset);
}


