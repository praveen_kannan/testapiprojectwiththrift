# Workaround for http://gcc.gnu.org/bugzilla/show_bug.cgi?id=42748
TARGET_CFLAGS += -Wno-psabi

LOCAL_PATH := $(call my-dir)/../../ZyngaAPI-Thrift
MY_ZYNGA_API_PATH := $(LOCAL_PATH)
MY_JSON_PATH := $(LOCAL_PATH)/../../../lib/cpp/json
MY_THRIFT_PATH := $(LOCAL_PATH)/../../../thrift/cpp/thrift
MY_BOOST_LIB_PATH := $(call my-dir)/../../../../boost/Android
MY_BOOST_PATH := $(call my-dir)/../../../../boost/Android/include/boost
MY_OPENSSL_PATH := $(call my-dir)/../../../../openssl/Android
MY_EXPORT_PATH := $(LOCAL_PATH)/../../../build/Android/exported

#$(warning LOCAL_PATH is: $(LOCAL_PATH))
#$(warning MY_ZYNGA_API_PATH is: $(MY_ZYNGA_API_PATH))
#$(warning MY_JSON_PATH is: $(MY_JSON_PATH))

# Convenience function to copy files, while creating missing directories if needed
# 1: Source file to be copied to export
# 2: Name of include file group (ZyngaAPI-Thrift/thrift/etc)
# 3: Base path for source file to begin copy from
# TODO: RM the includes folder before calling
define COPY_FILE
$(1): ;
	$(eval destination = $(patsubst $(3)/%, $(MY_EXPORT_PATH)/ZyngaAPI-Thrift/includes/$(2)/%, $(1)))
	$(eval destinationDir = $(dir ${destination}))
	$(shell test -d $(destinationDir) || mkdir -p $(destinationDir))
	$(shell rsync -drHav $(1) $(destinationDir))
endef

define COPY_BOOST_HEADERS_TO_EXPORT
: ;
	$(shell rsync -drHav $(MY_BOOST_PATH) $(MY_EXPORT_PATH)/ZyngaAPI-Thrift/includes/)
endef

getSourceFiles = \
	$(patsubst $(LOCAL_PATH)/%, %, \
	$(wildcard $(MY_ZYNGA_API_PATH)/gen-cpp/src/*.cpp) \
	$(wildcard $(MY_ZYNGA_API_PATH)/*/*.cpp) \
	$(wildcard $(MY_ZYNGA_API_PATH)/*.cpp) \
	$(wildcard $(MY_JSON_PATH)/src/*.cpp) \
	$(wildcard $(MY_THRIFT_PATH)/*.cpp) \
	$(wildcard $(MY_THRIFT_PATH)/**/*.cpp))

MY_SRC_FILES = $(call getSourceFiles)

# Remove problematic source files -- these are also disabled in the xcode builds
MY_SRC_FILES := $(filter-out gen-cpp/src/Auth_server.skeleton.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out gen-cpp/src/Conversation_server.skeleton.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out gen-cpp/src/DAPI_server.skeleton.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out gen-cpp/src/Experiment_server.skeleton.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out gen-cpp/src/Track_server.skeleton.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out gen-cpp/src/PlayerId_server.skeleton.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out gen-cpp/src/UserData_server.skeleton.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out gen-cpp/src/Neighbors_server.skeleton.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out gen-cpp/src/Leaderboard_server.skeleton.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out gen-cpp/src/friends_server.skeleton.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out gen-cpp/src/identities_server.skeleton.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out gen-cpp/src/account_server.skeleton.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out gen-cpp/src/ExternalSn_server.skeleton.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out gen-cpp/src/Zids_server.skeleton.cpp,$(MY_SRC_FILES))

# Remove problematic source files -- these are also disabled in the xcode builds
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/async/TEvhttpClientChannel.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/async/TEvhttpServer.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/concurrency/BoostMutex.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/concurrency/BoostMonitor.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/concurrency/BoostThreadFactory.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/protocol/TDAPIProtocol.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/qt/TQIODeviceTransport.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/qt/TQTcpServer.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/server/TNonblockingServer.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/transport/TZlibTransport.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/windows/GetTimeOfDay.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/windows/SocketPair.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/windows/StdAfx.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/windows/TWinsockSingleton.cpp,$(MY_SRC_FILES))
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/windows/WinFcntl.cpp,$(MY_SRC_FILES))
# Remove problematic source files -- these are only disabled for android (for convenience)
MY_SRC_FILES := $(filter-out ../../../thrift/cpp/thrift/transport/TFileTransport.cpp,$(MY_SRC_FILES))

$(warning MY_SRC_FILES are: $(MY_SRC_FILES))

# Careful, if you change these, the change needs to be propagated to the separate
# Android.mk for the pre-built at /<REPO ROOT>/Android/exported/ZyngaAPI-Thrift/Android.mk
# 1) Be very careful how you include the thrift headers!! There is a bogus imitation tr1/functional that
# will break compilation if it is chosen over the standard boost header
# 2) Also thrift references a <config.h> which can be confused with the json one if the order is changed here
MY_HEADER_PATHS := $(MY_ZYNGA_API_PATH)/../ \
				$(MY_ZYNGA_API_PATH)/gen-cpp/include \
				$(MY_OPENSSL_PATH)/include \
				$(MY_THRIFT_PATH) \
				$(MY_THRIFT_PATH)/.. \
				$(MY_BOOST_PATH)/.. \
				$(MY_BOOST_PATH) \
				$(MY_BOOST_PATH)/tr1 \
				$(MY_JSON_PATH)/../ \
				$(MY_JSON_PATH)/ \
				$(MY_JSON_PATH)/src/

$(warning MY_HEADER_PATHS are: $(MY_HEADER_PATHS))

# COPY THE DAPI HEADERS TO THE EXPORT FOLDER
MY_EXPORT_HEADER_PATHS = $(sort $(dir $(wildcard $(MY_ZYNGA_API_PATH)/*/ $(MY_ZYNGA_API_PATH)/*/*/)))
MY_EXPORT_HEADER_FILES = $(foreach path, $(MY_EXPORT_HEADER_PATHS)/, $(wildcard $(path)*.h))
$(foreach srcFilePath,$(MY_EXPORT_HEADER_FILES),$(call COPY_FILE,$(srcFilePath),ZyngaAPI-Thrift,$(MY_ZYNGA_API_PATH)))

# COPY THE THRIFT HEADERS TO THE EXPORT FOLDER
MY_EXPORT_HEADER_PATHS = $(sort $(dir $(wildcard $(MY_THRIFT_PATH)/*/ $(MY_THRIFT_PATH)/*/*/)))
MY_EXPORT_HEADER_FILES = $(foreach path, $(MY_EXPORT_HEADER_PATHS)/, $(wildcard $(path)*.h))
$(foreach srcFilePath,$(MY_EXPORT_HEADER_FILES),$(call COPY_FILE,$(srcFilePath),thrift,$(MY_THRIFT_PATH)))

#COPY THE BOOST HEADERS TO THE EXPORT FOLDER
$(call COPY_BOOST_HEADERS_TO_EXPORT)

include $(CLEAR_VARS)
LOCAL_MODULE := zyngaapi-thrift_static
LOCAL_SRC_FILES := $(MY_SRC_FILES)
LOCAL_LDLIBS := -llog \
				-lstdc++ \
				$(MY_OPENSSL_PATH)/$(TARGET_ARCH_ABI)/libssl.so \
				$(MY_OPENSSL_PATH)/$(TARGET_ARCH_ABI)/libcrypto.so \
				$(MY_BOOST_LIB_PATH)/libboost_atomic-gcc-mt-1_53.a \
				$(MY_BOOST_LIB_PATH)/libboost_system-gcc-mt-1_53.a \
				$(MY_BOOST_LIB_PATH)/libboost_thread-gcc-mt-1_53.a 

LOCAL_C_INCLUDES := $(MY_HEADER_PATHS)
LOCAL_EXPORT_C_INCLUDES := $(MY_HEADER_PATHS)
LOCAL_CFLAGS    := -DZDK_ANDROID_PLATFORM -DHAVE_CONFIG_H
LOCAL_CPPFLAGS := -fexceptions -frtti
LOCAL_EXPORT_LDLIBS := -llog \
				$(MY_OPENSSL_PATH)/$(TARGET_ARCH_ABI)/libssl.so \
				$(MY_OPENSSL_PATH)/$(TARGET_ARCH_ABI)/libcrypto.so \
				$(MY_BOOST_LIB_PATH)/libboost_atomic-gcc-mt-1_53.a \
				$(MY_BOOST_LIB_PATH)/libboost_system-gcc-mt-1_53.a \
				$(MY_BOOST_LIB_PATH)/libboost_thread-gcc-mt-1_53.a 

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := zyngaapi-thrift_shared
LOCAL_SRC_FILES := $(MY_SRC_FILES)
LOCAL_LDLIBS := -llog \
				-lstdc++ \
				$(MY_OPENSSL_PATH)/$(TARGET_ARCH_ABI)/libssl.so \
				$(MY_OPENSSL_PATH)/$(TARGET_ARCH_ABI)/libcrypto.so \
				$(MY_BOOST_LIB_PATH)/libboost_atomic-gcc-mt-1_53.a \
				$(MY_BOOST_LIB_PATH)/libboost_system-gcc-mt-1_53.a \
				$(MY_BOOST_LIB_PATH)/libboost_thread-gcc-mt-1_53.a 

LOCAL_C_INCLUDES := $(MY_HEADER_PATHS)
LOCAL_EXPORT_C_INCLUDES := $(MY_HEADER_PATHS)
LOCAL_CFLAGS    := -DZDK_ANDROID_PLATFORM -DHAVE_CONFIG_H
LOCAL_CPPFLAGS := -fexceptions -frtti
LOCAL_EXPORT_LDLIBS := -llog \
				$(MY_OPENSSL_PATH)/$(TARGET_ARCH_ABI)/libssl.so \
				$(MY_OPENSSL_PATH)/$(TARGET_ARCH_ABI)/libcrypto.so \
				$(MY_BOOST_LIB_PATH)/libboost_atomic-gcc-mt-1_53.a \
				$(MY_BOOST_LIB_PATH)/libboost_system-gcc-mt-1_53.a \
				$(MY_BOOST_LIB_PATH)/libboost_thread-gcc-mt-1_53.a 

include $(BUILD_SHARED_LIBRARY)
