/**
 * Autogenerated by Thrift Compiler (1.0.0-dev)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#include "neighbors_types.h"

#include <algorithm>



const char* NeighborsAddArgs::ascii_fingerprint = "34DD99BECD0CD3CFD33583C30C2D0F05";
const uint8_t NeighborsAddArgs::binary_fingerprint[16] = {0x34,0xDD,0x99,0xBE,0xCD,0x0C,0xD3,0xCF,0xD3,0x35,0x83,0xC3,0x0C,0x2D,0x0F,0x05};

uint32_t NeighborsAddArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_LIST) {
          {
            this->neighborZids.clear();
            uint32_t _size0;
            ::apache::thrift::protocol::TType _etype3;
            xfer += iprot->readListBegin(_etype3, _size0);
            this->neighborZids.resize(_size0);
            uint32_t _i4;
            for (_i4 = 0; _i4 < _size0; ++_i4)
            {
              xfer += iprot->readString(this->neighborZids[_i4]);
            }
            xfer += iprot->readListEnd();
          }
          this->__isset.neighborZids = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->appId);
          this->__isset.appId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->state);
          this->__isset.state = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t NeighborsAddArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("NeighborsAddArgs");

  xfer += oprot->writeFieldBegin("neighborZids", ::apache::thrift::protocol::T_LIST, 1);
  {
    xfer += oprot->writeListBegin(::apache::thrift::protocol::T_STRING, static_cast<uint32_t>(this->neighborZids.size()));
    std::vector<std::string> ::const_iterator _iter5;
    for (_iter5 = this->neighborZids.begin(); _iter5 != this->neighborZids.end(); ++_iter5)
    {
      xfer += oprot->writeString((*_iter5));
    }
    xfer += oprot->writeListEnd();
  }
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("appId", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->appId);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.state) {
    xfer += oprot->writeFieldBegin("state", ::apache::thrift::protocol::T_STRING, 3);
    xfer += oprot->writeString(this->state);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(NeighborsAddArgs &a, NeighborsAddArgs &b) {
  using ::std::swap;
  swap(a.neighborZids, b.neighborZids);
  swap(a.appId, b.appId);
  swap(a.state, b.state);
  swap(a.__isset, b.__isset);
}

const char* NeighborsAreNeighborsArgs::ascii_fingerprint = "07A9615F837F7D0A952B595DD3020972";
const uint8_t NeighborsAreNeighborsArgs::binary_fingerprint[16] = {0x07,0xA9,0x61,0x5F,0x83,0x7F,0x7D,0x0A,0x95,0x2B,0x59,0x5D,0xD3,0x02,0x09,0x72};

uint32_t NeighborsAreNeighborsArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->neighborZid);
          this->__isset.neighborZid = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->appId);
          this->__isset.appId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t NeighborsAreNeighborsArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("NeighborsAreNeighborsArgs");

  xfer += oprot->writeFieldBegin("neighborZid", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->neighborZid);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("appId", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->appId);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(NeighborsAreNeighborsArgs &a, NeighborsAreNeighborsArgs &b) {
  using ::std::swap;
  swap(a.neighborZid, b.neighborZid);
  swap(a.appId, b.appId);
  swap(a.__isset, b.__isset);
}

const char* NeighborsConfirmArgs::ascii_fingerprint = "BE556BF7091B2DABBA1863D5E458B15F";
const uint8_t NeighborsConfirmArgs::binary_fingerprint[16] = {0xBE,0x55,0x6B,0xF7,0x09,0x1B,0x2D,0xAB,0xBA,0x18,0x63,0xD5,0xE4,0x58,0xB1,0x5F};

uint32_t NeighborsConfirmArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_LIST) {
          {
            this->neighborZids.clear();
            uint32_t _size6;
            ::apache::thrift::protocol::TType _etype9;
            xfer += iprot->readListBegin(_etype9, _size6);
            this->neighborZids.resize(_size6);
            uint32_t _i10;
            for (_i10 = 0; _i10 < _size6; ++_i10)
            {
              xfer += iprot->readString(this->neighborZids[_i10]);
            }
            xfer += iprot->readListEnd();
          }
          this->__isset.neighborZids = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->appId);
          this->__isset.appId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t NeighborsConfirmArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("NeighborsConfirmArgs");

  xfer += oprot->writeFieldBegin("neighborZids", ::apache::thrift::protocol::T_LIST, 1);
  {
    xfer += oprot->writeListBegin(::apache::thrift::protocol::T_STRING, static_cast<uint32_t>(this->neighborZids.size()));
    std::vector<std::string> ::const_iterator _iter11;
    for (_iter11 = this->neighborZids.begin(); _iter11 != this->neighborZids.end(); ++_iter11)
    {
      xfer += oprot->writeString((*_iter11));
    }
    xfer += oprot->writeListEnd();
  }
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("appId", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->appId);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(NeighborsConfirmArgs &a, NeighborsConfirmArgs &b) {
  using ::std::swap;
  swap(a.neighborZids, b.neighborZids);
  swap(a.appId, b.appId);
  swap(a.__isset, b.__isset);
}

const char* NeighborsDenyArgs::ascii_fingerprint = "BE556BF7091B2DABBA1863D5E458B15F";
const uint8_t NeighborsDenyArgs::binary_fingerprint[16] = {0xBE,0x55,0x6B,0xF7,0x09,0x1B,0x2D,0xAB,0xBA,0x18,0x63,0xD5,0xE4,0x58,0xB1,0x5F};

uint32_t NeighborsDenyArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_LIST) {
          {
            this->neighborZids.clear();
            uint32_t _size12;
            ::apache::thrift::protocol::TType _etype15;
            xfer += iprot->readListBegin(_etype15, _size12);
            this->neighborZids.resize(_size12);
            uint32_t _i16;
            for (_i16 = 0; _i16 < _size12; ++_i16)
            {
              xfer += iprot->readString(this->neighborZids[_i16]);
            }
            xfer += iprot->readListEnd();
          }
          this->__isset.neighborZids = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->appId);
          this->__isset.appId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t NeighborsDenyArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("NeighborsDenyArgs");

  xfer += oprot->writeFieldBegin("neighborZids", ::apache::thrift::protocol::T_LIST, 1);
  {
    xfer += oprot->writeListBegin(::apache::thrift::protocol::T_STRING, static_cast<uint32_t>(this->neighborZids.size()));
    std::vector<std::string> ::const_iterator _iter17;
    for (_iter17 = this->neighborZids.begin(); _iter17 != this->neighborZids.end(); ++_iter17)
    {
      xfer += oprot->writeString((*_iter17));
    }
    xfer += oprot->writeListEnd();
  }
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("appId", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->appId);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(NeighborsDenyArgs &a, NeighborsDenyArgs &b) {
  using ::std::swap;
  swap(a.neighborZids, b.neighborZids);
  swap(a.appId, b.appId);
  swap(a.__isset, b.__isset);
}

const char* NeighborsGetArgs::ascii_fingerprint = "14699E0CEF958F615AB5B72E673ABF6E";
const uint8_t NeighborsGetArgs::binary_fingerprint[16] = {0x14,0x69,0x9E,0x0C,0xEF,0x95,0x8F,0x61,0x5A,0xB5,0xB7,0x2E,0x67,0x3A,0xBF,0x6E};

uint32_t NeighborsGetArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->appId);
          this->__isset.appId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->state);
          this->__isset.state = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_BOOL) {
          xfer += iprot->readBool(this->withTimestamp);
          this->__isset.withTimestamp = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t NeighborsGetArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("NeighborsGetArgs");

  xfer += oprot->writeFieldBegin("appId", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->appId);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.state) {
    xfer += oprot->writeFieldBegin("state", ::apache::thrift::protocol::T_STRING, 2);
    xfer += oprot->writeString(this->state);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.withTimestamp) {
    xfer += oprot->writeFieldBegin("withTimestamp", ::apache::thrift::protocol::T_BOOL, 3);
    xfer += oprot->writeBool(this->withTimestamp);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(NeighborsGetArgs &a, NeighborsGetArgs &b) {
  using ::std::swap;
  swap(a.appId, b.appId);
  swap(a.state, b.state);
  swap(a.withTimestamp, b.withTimestamp);
  swap(a.__isset, b.__isset);
}

const char* NeighborsGetAllArgs::ascii_fingerprint = "99914B932BD37A50B983C5E7C90AE93B";
const uint8_t NeighborsGetAllArgs::binary_fingerprint[16] = {0x99,0x91,0x4B,0x93,0x2B,0xD3,0x7A,0x50,0xB9,0x83,0xC5,0xE7,0xC9,0x0A,0xE9,0x3B};

uint32_t NeighborsGetAllArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    xfer += iprot->skip(ftype);
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t NeighborsGetAllArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("NeighborsGetAllArgs");

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(NeighborsGetAllArgs &a, NeighborsGetAllArgs &b) {
  using ::std::swap;
  (void) a;
  (void) b;
}

const char* NeighborsGetMutualNeighborsArgs::ascii_fingerprint = "07A9615F837F7D0A952B595DD3020972";
const uint8_t NeighborsGetMutualNeighborsArgs::binary_fingerprint[16] = {0x07,0xA9,0x61,0x5F,0x83,0x7F,0x7D,0x0A,0x95,0x2B,0x59,0x5D,0xD3,0x02,0x09,0x72};

uint32_t NeighborsGetMutualNeighborsArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->neighborZid);
          this->__isset.neighborZid = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->appId);
          this->__isset.appId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t NeighborsGetMutualNeighborsArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("NeighborsGetMutualNeighborsArgs");

  xfer += oprot->writeFieldBegin("neighborZid", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->neighborZid);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("appId", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->appId);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(NeighborsGetMutualNeighborsArgs &a, NeighborsGetMutualNeighborsArgs &b) {
  using ::std::swap;
  swap(a.neighborZid, b.neighborZid);
  swap(a.appId, b.appId);
  swap(a.__isset, b.__isset);
}

const char* NeighborsRemoveArgs::ascii_fingerprint = "34DD99BECD0CD3CFD33583C30C2D0F05";
const uint8_t NeighborsRemoveArgs::binary_fingerprint[16] = {0x34,0xDD,0x99,0xBE,0xCD,0x0C,0xD3,0xCF,0xD3,0x35,0x83,0xC3,0x0C,0x2D,0x0F,0x05};

uint32_t NeighborsRemoveArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_LIST) {
          {
            this->neighborZids.clear();
            uint32_t _size18;
            ::apache::thrift::protocol::TType _etype21;
            xfer += iprot->readListBegin(_etype21, _size18);
            this->neighborZids.resize(_size18);
            uint32_t _i22;
            for (_i22 = 0; _i22 < _size18; ++_i22)
            {
              xfer += iprot->readString(this->neighborZids[_i22]);
            }
            xfer += iprot->readListEnd();
          }
          this->__isset.neighborZids = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->appId);
          this->__isset.appId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->state);
          this->__isset.state = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t NeighborsRemoveArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("NeighborsRemoveArgs");

  xfer += oprot->writeFieldBegin("neighborZids", ::apache::thrift::protocol::T_LIST, 1);
  {
    xfer += oprot->writeListBegin(::apache::thrift::protocol::T_STRING, static_cast<uint32_t>(this->neighborZids.size()));
    std::vector<std::string> ::const_iterator _iter23;
    for (_iter23 = this->neighborZids.begin(); _iter23 != this->neighborZids.end(); ++_iter23)
    {
      xfer += oprot->writeString((*_iter23));
    }
    xfer += oprot->writeListEnd();
  }
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("appId", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->appId);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.state) {
    xfer += oprot->writeFieldBegin("state", ::apache::thrift::protocol::T_STRING, 3);
    xfer += oprot->writeString(this->state);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(NeighborsRemoveArgs &a, NeighborsRemoveArgs &b) {
  using ::std::swap;
  swap(a.neighborZids, b.neighborZids);
  swap(a.appId, b.appId);
  swap(a.state, b.state);
  swap(a.__isset, b.__isset);
}

const char* NeighborsSetArgs::ascii_fingerprint = "34DD99BECD0CD3CFD33583C30C2D0F05";
const uint8_t NeighborsSetArgs::binary_fingerprint[16] = {0x34,0xDD,0x99,0xBE,0xCD,0x0C,0xD3,0xCF,0xD3,0x35,0x83,0xC3,0x0C,0x2D,0x0F,0x05};

uint32_t NeighborsSetArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_LIST) {
          {
            this->neighborZids.clear();
            uint32_t _size24;
            ::apache::thrift::protocol::TType _etype27;
            xfer += iprot->readListBegin(_etype27, _size24);
            this->neighborZids.resize(_size24);
            uint32_t _i28;
            for (_i28 = 0; _i28 < _size24; ++_i28)
            {
              xfer += iprot->readString(this->neighborZids[_i28]);
            }
            xfer += iprot->readListEnd();
          }
          this->__isset.neighborZids = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->appId);
          this->__isset.appId = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->state);
          this->__isset.state = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t NeighborsSetArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("NeighborsSetArgs");

  xfer += oprot->writeFieldBegin("neighborZids", ::apache::thrift::protocol::T_LIST, 1);
  {
    xfer += oprot->writeListBegin(::apache::thrift::protocol::T_STRING, static_cast<uint32_t>(this->neighborZids.size()));
    std::vector<std::string> ::const_iterator _iter29;
    for (_iter29 = this->neighborZids.begin(); _iter29 != this->neighborZids.end(); ++_iter29)
    {
      xfer += oprot->writeString((*_iter29));
    }
    xfer += oprot->writeListEnd();
  }
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("appId", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->appId);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.state) {
    xfer += oprot->writeFieldBegin("state", ::apache::thrift::protocol::T_STRING, 3);
    xfer += oprot->writeString(this->state);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(NeighborsSetArgs &a, NeighborsSetArgs &b) {
  using ::std::swap;
  swap(a.neighborZids, b.neighborZids);
  swap(a.appId, b.appId);
  swap(a.state, b.state);
  swap(a.__isset, b.__isset);
}

const char* NeighborsAdminSetArgs::ascii_fingerprint = "BE556BF7091B2DABBA1863D5E458B15F";
const uint8_t NeighborsAdminSetArgs::binary_fingerprint[16] = {0xBE,0x55,0x6B,0xF7,0x09,0x1B,0x2D,0xAB,0xBA,0x18,0x63,0xD5,0xE4,0x58,0xB1,0x5F};

uint32_t NeighborsAdminSetArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_LIST) {
          {
            this->neighborZids.clear();
            uint32_t _size30;
            ::apache::thrift::protocol::TType _etype33;
            xfer += iprot->readListBegin(_etype33, _size30);
            this->neighborZids.resize(_size30);
            uint32_t _i34;
            for (_i34 = 0; _i34 < _size30; ++_i34)
            {
              xfer += iprot->readString(this->neighborZids[_i34]);
            }
            xfer += iprot->readListEnd();
          }
          this->__isset.neighborZids = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->state);
          this->__isset.state = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t NeighborsAdminSetArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("NeighborsAdminSetArgs");

  xfer += oprot->writeFieldBegin("neighborZids", ::apache::thrift::protocol::T_LIST, 1);
  {
    xfer += oprot->writeListBegin(::apache::thrift::protocol::T_STRING, static_cast<uint32_t>(this->neighborZids.size()));
    std::vector<std::string> ::const_iterator _iter35;
    for (_iter35 = this->neighborZids.begin(); _iter35 != this->neighborZids.end(); ++_iter35)
    {
      xfer += oprot->writeString((*_iter35));
    }
    xfer += oprot->writeListEnd();
  }
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("state", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->state);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(NeighborsAdminSetArgs &a, NeighborsAdminSetArgs &b) {
  using ::std::swap;
  swap(a.neighborZids, b.neighborZids);
  swap(a.state, b.state);
  swap(a.__isset, b.__isset);
}

const char* NeighborsAdminDeleteAllArgs::ascii_fingerprint = "1959DF646639D95C0F1375CF60F71F5B";
const uint8_t NeighborsAdminDeleteAllArgs::binary_fingerprint[16] = {0x19,0x59,0xDF,0x64,0x66,0x39,0xD9,0x5C,0x0F,0x13,0x75,0xCF,0x60,0xF7,0x1F,0x5B};

uint32_t NeighborsAdminDeleteAllArgs::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_BOOL) {
          xfer += iprot->readBool(this->force);
          this->__isset.force = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_BOOL) {
          xfer += iprot->readBool(this->allData);
          this->__isset.allData = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t NeighborsAdminDeleteAllArgs::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("NeighborsAdminDeleteAllArgs");

  if (this->__isset.force) {
    xfer += oprot->writeFieldBegin("force", ::apache::thrift::protocol::T_BOOL, 1);
    xfer += oprot->writeBool(this->force);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.allData) {
    xfer += oprot->writeFieldBegin("allData", ::apache::thrift::protocol::T_BOOL, 2);
    xfer += oprot->writeBool(this->allData);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(NeighborsAdminDeleteAllArgs &a, NeighborsAdminDeleteAllArgs &b) {
  using ::std::swap;
  swap(a.force, b.force);
  swap(a.allData, b.allData);
  swap(a.__isset, b.__isset);
}


