/**
 * Autogenerated by Thrift Compiler (1.0.0-dev)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#ifndef zids_private_inl_TYPES_H
#define zids_private_inl_TYPES_H




  inline void ZidsMapArgs::__set_uids(const std::vector<std::string> & val) {
    uids = val;
  }

  inline void ZidsMapArgs::__set_fromNetwork(const std::string& val) {
    fromNetwork = val;
  }

  inline bool ZidsMapArgs::operator == (const ZidsMapArgs & rhs) const {
    if (!(uids == rhs.uids))
      return false;
    if (!(fromNetwork == rhs.fromNetwork))
      return false;
    return true;
  }

  inline bool ZidsMapArgs::operator != (const ZidsMapArgs &rhs) const {
      return !(*this == rhs);
  }


#endif
