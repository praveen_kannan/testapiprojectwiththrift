/**
 * Autogenerated by Thrift Compiler (1.0.0-dev)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#ifndef ExternalSn_H
#define ExternalSn_H

#include <thrift/TDispatchProcessor.h>
#include "externalsn_types.h"
#include "ExternalSn_private.h"
#include <ZyngaAPI-Thrift/DapiMgr.h>



class ExternalSnClient : virtual public ExternalSnIf {
 public:
  ExternalSnClient(boost::shared_ptr< ::apache::thrift::protocol::TProtocol> prot) :
    piprot_(prot),
    poprot_(prot) {
    iprot_ = prot.get();
    oprot_ = prot.get();
  }
  ExternalSnClient(boost::shared_ptr< ::apache::thrift::protocol::TProtocol> iprot, boost::shared_ptr< ::apache::thrift::protocol::TProtocol> oprot) :
    piprot_(iprot),
    poprot_(oprot) {
    iprot_ = iprot.get();
    oprot_ = oprot.get();
  }
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> getInputProtocol() {
    return piprot_;
  }
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> getOutputProtocol() {
    return poprot_;
  }
  void GetFriendList( ::DAPIResponse& _return, const ExternalSnGetFriendListArgs& arguments);
  void GetAppUsingFriendList( ::DAPIResponse& _return, const ExternalSnGetAppUsingFriendListArgs& arguments);
  void GetNonAppUsingFriendList( ::DAPIResponse& _return, const ExternalSnGetNonAppUsingFriendListArgs& arguments);
  void GetUserInfoList( ::DAPIResponse& _return, const ExternalSnGetUserInfoListArgs& arguments);

  static void AsyncGetFriendList(ZyngaAPI::DapiMgr::ZyngaAPICallback cb, const ExternalSnGetFriendListArgs& arguments, void* pUserData);
  static void AsyncGetAppUsingFriendList(ZyngaAPI::DapiMgr::ZyngaAPICallback cb, const ExternalSnGetAppUsingFriendListArgs& arguments, void* pUserData);
  static void AsyncGetNonAppUsingFriendList(ZyngaAPI::DapiMgr::ZyngaAPICallback cb, const ExternalSnGetNonAppUsingFriendListArgs& arguments, void* pUserData);
  static void AsyncGetUserInfoList(ZyngaAPI::DapiMgr::ZyngaAPICallback cb, const ExternalSnGetUserInfoListArgs& arguments, void* pUserData);

  static bool HandleAsyncExternalSnCall(ZyngaAPI::DapiMgr::CallbackContext& context);

  void send_GetFriendList(const ExternalSnGetFriendListArgs& arguments);
  void recv_GetFriendList( ::DAPIResponse& _return);
  void send_GetAppUsingFriendList(const ExternalSnGetAppUsingFriendListArgs& arguments);
  void recv_GetAppUsingFriendList( ::DAPIResponse& _return);
  void send_GetNonAppUsingFriendList(const ExternalSnGetNonAppUsingFriendListArgs& arguments);
  void recv_GetNonAppUsingFriendList( ::DAPIResponse& _return);
  void send_GetUserInfoList(const ExternalSnGetUserInfoListArgs& arguments);
  void recv_GetUserInfoList( ::DAPIResponse& _return);
 protected:
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> piprot_;
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> poprot_;
  ::apache::thrift::protocol::TProtocol* iprot_;
  ::apache::thrift::protocol::TProtocol* oprot_;
};



#endif
