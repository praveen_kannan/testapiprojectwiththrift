/**
 * Autogenerated by Thrift Compiler (1.0.0-dev)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#ifndef zids_TYPES_H
#define zids_TYPES_H

#include <thrift/Thrift.h>
#include <thrift/TApplicationException.h>
#include <thrift/protocol/TProtocol.h>
#include <thrift/transport/TTransport.h>

#include "dapi_types.h"

#include "zids_types_private.h"




class ZidsMapArgs {
 public:

  static const char* ascii_fingerprint; // = "BE556BF7091B2DABBA1863D5E458B15F";
  static const uint8_t binary_fingerprint[16]; // = {0xBE,0x55,0x6B,0xF7,0x09,0x1B,0x2D,0xAB,0xBA,0x18,0x63,0xD5,0xE4,0x58,0xB1,0x5F};

  ZidsMapArgs() : fromNetwork() {
  }

  virtual ~ZidsMapArgs() throw() {}

  std::vector<std::string>  uids;
  std::string fromNetwork;

  _ZidsMapArgs__isset __isset;

  void __set_uids(const std::vector<std::string> & val);
  void __set_fromNetwork(const std::string& val);

  bool operator == (const ZidsMapArgs & rhs) const;
  bool operator != (const ZidsMapArgs &rhs) const;
  bool operator < (const ZidsMapArgs & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;
};

void swap(ZidsMapArgs &a, ZidsMapArgs &b);


#include "zids_types_private_inl.h"



#endif
