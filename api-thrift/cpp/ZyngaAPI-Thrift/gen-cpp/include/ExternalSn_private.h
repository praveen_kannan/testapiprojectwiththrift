/**
 * Autogenerated by Thrift Compiler (1.0.0-dev)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#ifndef ExternalSn_private_H
#define ExternalSn_private_H

class ExternalSnIf {
 public:
  virtual ~ExternalSnIf() {}
  virtual void GetFriendList( ::DAPIResponse& _return, const ExternalSnGetFriendListArgs& arguments) = 0;
  virtual void GetAppUsingFriendList( ::DAPIResponse& _return, const ExternalSnGetAppUsingFriendListArgs& arguments) = 0;
  virtual void GetNonAppUsingFriendList( ::DAPIResponse& _return, const ExternalSnGetNonAppUsingFriendListArgs& arguments) = 0;
  virtual void GetUserInfoList( ::DAPIResponse& _return, const ExternalSnGetUserInfoListArgs& arguments) = 0;
};

class ExternalSnIfFactory {
 public:
  typedef ExternalSnIf Handler;

  virtual ~ExternalSnIfFactory() {}

  virtual ExternalSnIf* getHandler(const ::apache::thrift::TConnectionInfo& connInfo) = 0;
  virtual void releaseHandler(ExternalSnIf* /* handler */) = 0;
};

class ExternalSnIfSingletonFactory : virtual public ExternalSnIfFactory {
 public:
  ExternalSnIfSingletonFactory(const boost::shared_ptr<ExternalSnIf>& iface) : iface_(iface) {}
  virtual ~ExternalSnIfSingletonFactory() {}

  virtual ExternalSnIf* getHandler(const ::apache::thrift::TConnectionInfo&) {
    return iface_.get();
  }
  virtual void releaseHandler(ExternalSnIf* /* handler */) {}

 protected:
  boost::shared_ptr<ExternalSnIf> iface_;
};

class ExternalSnNull : virtual public ExternalSnIf {
 public:
  virtual ~ExternalSnNull() {}
  void GetFriendList( ::DAPIResponse& /* _return */, const ExternalSnGetFriendListArgs& /* arguments */) {
    return;
  }
  void GetAppUsingFriendList( ::DAPIResponse& /* _return */, const ExternalSnGetAppUsingFriendListArgs& /* arguments */) {
    return;
  }
  void GetNonAppUsingFriendList( ::DAPIResponse& /* _return */, const ExternalSnGetNonAppUsingFriendListArgs& /* arguments */) {
    return;
  }
  void GetUserInfoList( ::DAPIResponse& /* _return */, const ExternalSnGetUserInfoListArgs& /* arguments */) {
    return;
  }
};

typedef struct _ExternalSn_GetFriendList_args__isset {
  _ExternalSn_GetFriendList_args__isset() : arguments(false) {}
  bool arguments;
} _ExternalSn_GetFriendList_args__isset;

class ExternalSn_GetFriendList_args {
 public:

  ExternalSn_GetFriendList_args() {
  }

  virtual ~ExternalSn_GetFriendList_args() throw() {}

  ExternalSnGetFriendListArgs arguments;

  _ExternalSn_GetFriendList_args__isset __isset;

  void __set_arguments(const ExternalSnGetFriendListArgs& val) {
    arguments = val;
  }


  bool operator == (const ExternalSn_GetFriendList_args & rhs) const {
    if (!(arguments == rhs.arguments))
      return false;
    return true;
  }

  bool operator != (const ExternalSn_GetFriendList_args &rhs) const {
      return !(*this == rhs);
  }
  bool operator < (const ExternalSn_GetFriendList_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;
};


class ExternalSn_GetFriendList_pargs {
 public:


  virtual ~ExternalSn_GetFriendList_pargs() throw() {}

  const ExternalSnGetFriendListArgs* arguments;


  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;
};

typedef struct _ExternalSn_GetFriendList_result__isset {
  _ExternalSn_GetFriendList_result__isset() : success(false) {}
  bool success;
} _ExternalSn_GetFriendList_result__isset;

class ExternalSn_GetFriendList_result {
 public:

  ExternalSn_GetFriendList_result() {
  }

  virtual ~ExternalSn_GetFriendList_result() throw() {}

   ::DAPIResponse success;

  _ExternalSn_GetFriendList_result__isset __isset;

  void __set_success(const  ::DAPIResponse& val) {
    success = val;
  }


  bool operator == (const ExternalSn_GetFriendList_result & rhs) const {
    if (!(success == rhs.success))
      return false;
    return true;
  }

  bool operator != (const ExternalSn_GetFriendList_result &rhs) const {
      return !(*this == rhs);
  }
  bool operator < (const ExternalSn_GetFriendList_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;
};

typedef struct _ExternalSn_GetFriendList_presult__isset {
  _ExternalSn_GetFriendList_presult__isset() : success(false) {}
  bool success;
} _ExternalSn_GetFriendList_presult__isset;

class ExternalSn_GetFriendList_presult {
 public:


  virtual ~ExternalSn_GetFriendList_presult() throw() {}

   ::DAPIResponse* success;

  _ExternalSn_GetFriendList_presult__isset __isset;



  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
};

typedef struct _ExternalSn_GetAppUsingFriendList_args__isset {
  _ExternalSn_GetAppUsingFriendList_args__isset() : arguments(false) {}
  bool arguments;
} _ExternalSn_GetAppUsingFriendList_args__isset;

class ExternalSn_GetAppUsingFriendList_args {
 public:

  ExternalSn_GetAppUsingFriendList_args() {
  }

  virtual ~ExternalSn_GetAppUsingFriendList_args() throw() {}

  ExternalSnGetAppUsingFriendListArgs arguments;

  _ExternalSn_GetAppUsingFriendList_args__isset __isset;

  void __set_arguments(const ExternalSnGetAppUsingFriendListArgs& val) {
    arguments = val;
  }


  bool operator == (const ExternalSn_GetAppUsingFriendList_args & rhs) const {
    if (!(arguments == rhs.arguments))
      return false;
    return true;
  }

  bool operator != (const ExternalSn_GetAppUsingFriendList_args &rhs) const {
      return !(*this == rhs);
  }
  bool operator < (const ExternalSn_GetAppUsingFriendList_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;
};


class ExternalSn_GetAppUsingFriendList_pargs {
 public:


  virtual ~ExternalSn_GetAppUsingFriendList_pargs() throw() {}

  const ExternalSnGetAppUsingFriendListArgs* arguments;


  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;
};

typedef struct _ExternalSn_GetAppUsingFriendList_result__isset {
  _ExternalSn_GetAppUsingFriendList_result__isset() : success(false) {}
  bool success;
} _ExternalSn_GetAppUsingFriendList_result__isset;

class ExternalSn_GetAppUsingFriendList_result {
 public:

  ExternalSn_GetAppUsingFriendList_result() {
  }

  virtual ~ExternalSn_GetAppUsingFriendList_result() throw() {}

   ::DAPIResponse success;

  _ExternalSn_GetAppUsingFriendList_result__isset __isset;

  void __set_success(const  ::DAPIResponse& val) {
    success = val;
  }


  bool operator == (const ExternalSn_GetAppUsingFriendList_result & rhs) const {
    if (!(success == rhs.success))
      return false;
    return true;
  }

  bool operator != (const ExternalSn_GetAppUsingFriendList_result &rhs) const {
      return !(*this == rhs);
  }
  bool operator < (const ExternalSn_GetAppUsingFriendList_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;
};

typedef struct _ExternalSn_GetAppUsingFriendList_presult__isset {
  _ExternalSn_GetAppUsingFriendList_presult__isset() : success(false) {}
  bool success;
} _ExternalSn_GetAppUsingFriendList_presult__isset;

class ExternalSn_GetAppUsingFriendList_presult {
 public:


  virtual ~ExternalSn_GetAppUsingFriendList_presult() throw() {}

   ::DAPIResponse* success;

  _ExternalSn_GetAppUsingFriendList_presult__isset __isset;



  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
};

typedef struct _ExternalSn_GetNonAppUsingFriendList_args__isset {
  _ExternalSn_GetNonAppUsingFriendList_args__isset() : arguments(false) {}
  bool arguments;
} _ExternalSn_GetNonAppUsingFriendList_args__isset;

class ExternalSn_GetNonAppUsingFriendList_args {
 public:

  ExternalSn_GetNonAppUsingFriendList_args() {
  }

  virtual ~ExternalSn_GetNonAppUsingFriendList_args() throw() {}

  ExternalSnGetNonAppUsingFriendListArgs arguments;

  _ExternalSn_GetNonAppUsingFriendList_args__isset __isset;

  void __set_arguments(const ExternalSnGetNonAppUsingFriendListArgs& val) {
    arguments = val;
  }


  bool operator == (const ExternalSn_GetNonAppUsingFriendList_args & rhs) const {
    if (!(arguments == rhs.arguments))
      return false;
    return true;
  }

  bool operator != (const ExternalSn_GetNonAppUsingFriendList_args &rhs) const {
      return !(*this == rhs);
  }
  bool operator < (const ExternalSn_GetNonAppUsingFriendList_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;
};


class ExternalSn_GetNonAppUsingFriendList_pargs {
 public:


  virtual ~ExternalSn_GetNonAppUsingFriendList_pargs() throw() {}

  const ExternalSnGetNonAppUsingFriendListArgs* arguments;


  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;
};

typedef struct _ExternalSn_GetNonAppUsingFriendList_result__isset {
  _ExternalSn_GetNonAppUsingFriendList_result__isset() : success(false) {}
  bool success;
} _ExternalSn_GetNonAppUsingFriendList_result__isset;

class ExternalSn_GetNonAppUsingFriendList_result {
 public:

  ExternalSn_GetNonAppUsingFriendList_result() {
  }

  virtual ~ExternalSn_GetNonAppUsingFriendList_result() throw() {}

   ::DAPIResponse success;

  _ExternalSn_GetNonAppUsingFriendList_result__isset __isset;

  void __set_success(const  ::DAPIResponse& val) {
    success = val;
  }


  bool operator == (const ExternalSn_GetNonAppUsingFriendList_result & rhs) const {
    if (!(success == rhs.success))
      return false;
    return true;
  }

  bool operator != (const ExternalSn_GetNonAppUsingFriendList_result &rhs) const {
      return !(*this == rhs);
  }
  bool operator < (const ExternalSn_GetNonAppUsingFriendList_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;
};

typedef struct _ExternalSn_GetNonAppUsingFriendList_presult__isset {
  _ExternalSn_GetNonAppUsingFriendList_presult__isset() : success(false) {}
  bool success;
} _ExternalSn_GetNonAppUsingFriendList_presult__isset;

class ExternalSn_GetNonAppUsingFriendList_presult {
 public:


  virtual ~ExternalSn_GetNonAppUsingFriendList_presult() throw() {}

   ::DAPIResponse* success;

  _ExternalSn_GetNonAppUsingFriendList_presult__isset __isset;



  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
};

typedef struct _ExternalSn_GetUserInfoList_args__isset {
  _ExternalSn_GetUserInfoList_args__isset() : arguments(false) {}
  bool arguments;
} _ExternalSn_GetUserInfoList_args__isset;

class ExternalSn_GetUserInfoList_args {
 public:

  ExternalSn_GetUserInfoList_args() {
  }

  virtual ~ExternalSn_GetUserInfoList_args() throw() {}

  ExternalSnGetUserInfoListArgs arguments;

  _ExternalSn_GetUserInfoList_args__isset __isset;

  void __set_arguments(const ExternalSnGetUserInfoListArgs& val) {
    arguments = val;
  }


  bool operator == (const ExternalSn_GetUserInfoList_args & rhs) const {
    if (!(arguments == rhs.arguments))
      return false;
    return true;
  }

  bool operator != (const ExternalSn_GetUserInfoList_args &rhs) const {
      return !(*this == rhs);
  }
  bool operator < (const ExternalSn_GetUserInfoList_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;
};


class ExternalSn_GetUserInfoList_pargs {
 public:


  virtual ~ExternalSn_GetUserInfoList_pargs() throw() {}

  const ExternalSnGetUserInfoListArgs* arguments;


  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;
};

typedef struct _ExternalSn_GetUserInfoList_result__isset {
  _ExternalSn_GetUserInfoList_result__isset() : success(false) {}
  bool success;
} _ExternalSn_GetUserInfoList_result__isset;

class ExternalSn_GetUserInfoList_result {
 public:

  ExternalSn_GetUserInfoList_result() {
  }

  virtual ~ExternalSn_GetUserInfoList_result() throw() {}

   ::DAPIResponse success;

  _ExternalSn_GetUserInfoList_result__isset __isset;

  void __set_success(const  ::DAPIResponse& val) {
    success = val;
  }


  bool operator == (const ExternalSn_GetUserInfoList_result & rhs) const {
    if (!(success == rhs.success))
      return false;
    return true;
  }

  bool operator != (const ExternalSn_GetUserInfoList_result &rhs) const {
      return !(*this == rhs);
  }
  bool operator < (const ExternalSn_GetUserInfoList_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;
};

typedef struct _ExternalSn_GetUserInfoList_presult__isset {
  _ExternalSn_GetUserInfoList_presult__isset() : success(false) {}
  bool success;
} _ExternalSn_GetUserInfoList_presult__isset;

class ExternalSn_GetUserInfoList_presult {
 public:


  virtual ~ExternalSn_GetUserInfoList_presult() throw() {}

   ::DAPIResponse* success;

  _ExternalSn_GetUserInfoList_presult__isset __isset;



  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
};

class ExternalSnProcessor : public ::apache::thrift::TDispatchProcessor {
 protected:
  boost::shared_ptr<ExternalSnIf> iface_;
  virtual bool dispatchCall(::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, const std::string& fname, int32_t seqid, void* callContext);
 private:
  typedef  void (ExternalSnProcessor::*ProcessFunction)(int32_t, ::apache::thrift::protocol::TProtocol*, ::apache::thrift::protocol::TProtocol*, void*);
  typedef std::map<std::string, ProcessFunction> ProcessMap;
  ProcessMap processMap_;
  void process_GetFriendList(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_GetAppUsingFriendList(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_GetNonAppUsingFriendList(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_GetUserInfoList(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
 public:
  ExternalSnProcessor(boost::shared_ptr<ExternalSnIf> iface) :
    iface_(iface) {
    processMap_["GetFriendList"] = &ExternalSnProcessor::process_GetFriendList;
    processMap_["GetAppUsingFriendList"] = &ExternalSnProcessor::process_GetAppUsingFriendList;
    processMap_["GetNonAppUsingFriendList"] = &ExternalSnProcessor::process_GetNonAppUsingFriendList;
    processMap_["GetUserInfoList"] = &ExternalSnProcessor::process_GetUserInfoList;
  }

  virtual ~ExternalSnProcessor() {}
};

class ExternalSnProcessorFactory : public ::apache::thrift::TProcessorFactory {
 public:
  ExternalSnProcessorFactory(const ::boost::shared_ptr< ExternalSnIfFactory >& handlerFactory) :
      handlerFactory_(handlerFactory) {}

  ::boost::shared_ptr< ::apache::thrift::TProcessor > getProcessor(const ::apache::thrift::TConnectionInfo& connInfo);

 protected:
  ::boost::shared_ptr< ExternalSnIfFactory > handlerFactory_;
};

class ExternalSnMultiface : virtual public ExternalSnIf {
 public:
  ExternalSnMultiface(std::vector<boost::shared_ptr<ExternalSnIf> >& ifaces) : ifaces_(ifaces) {
  }
  virtual ~ExternalSnMultiface() {}
 protected:
  std::vector<boost::shared_ptr<ExternalSnIf> > ifaces_;
  ExternalSnMultiface() {}
  void add(boost::shared_ptr<ExternalSnIf> iface) {
    ifaces_.push_back(iface);
  }
 public:
  void GetFriendList( ::DAPIResponse& _return, const ExternalSnGetFriendListArgs& arguments) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->GetFriendList(_return, arguments);
    }
    ifaces_[i]->GetFriendList(_return, arguments);
    return;
  }

  void GetAppUsingFriendList( ::DAPIResponse& _return, const ExternalSnGetAppUsingFriendListArgs& arguments) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->GetAppUsingFriendList(_return, arguments);
    }
    ifaces_[i]->GetAppUsingFriendList(_return, arguments);
    return;
  }

  void GetNonAppUsingFriendList( ::DAPIResponse& _return, const ExternalSnGetNonAppUsingFriendListArgs& arguments) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->GetNonAppUsingFriendList(_return, arguments);
    }
    ifaces_[i]->GetNonAppUsingFriendList(_return, arguments);
    return;
  }

  void GetUserInfoList( ::DAPIResponse& _return, const ExternalSnGetUserInfoListArgs& arguments) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->GetUserInfoList(_return, arguments);
    }
    ifaces_[i]->GetUserInfoList(_return, arguments);
    return;
  }

};

#endif
