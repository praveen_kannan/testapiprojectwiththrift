//
//  AuthMgr.h
//  DAPI
//
//  Created by bmorishita on 4/11/13.
//  Copyright (c) 2013 jdao. All rights reserved.
//

#ifndef __DAPI__AuthMgr__
#define __DAPI__AuthMgr__

#include <ZyngaAPI-Thrift/gen-cpp/include/dapi_types.h>

namespace ZyngaAPI
{
	//! AuthMgr is used to simplify storage and management of auth information.
	//! It is used internally to ZyngaAPI to provide auth information to all dapi service clients.
	//! Auth needs to be set up by calling Create and then initializing the auth context via:
	//!		1) UseUserSessionToken
	//!		2) UseApplicationToken
	//!		3) UseStringToken
	//!		4) PublicToken
	//! Now you can make calls and the last auth information you provided will be used.
	//! Nothing else needs to be done here unless you need to change the AuthContext at a later time.
	//!
	//! An overview of these authentication schemes is available at:
	//! https://zyntranet.apps.corp.zynga.com/display/DAPI/Protocol+v1.2
	class AuthMgr
	{
	public:
		struct UserSessionToken
		{
			std::string mZid;
			std::string mAppId;		// Social network app id
			int			mnSnid;		// Social network id
			
			// Any key value pairs associated with the session (ex, call MiSocial::GetSessionMap)
			// Hint: For example, Facebook session validation requires
			//	1) user_id
			//	2) access_token
			std::map<std::string, std::string> mSession;
		};
		
		struct ApplicationToken
		{
			std::string mAppId;		// Zynga app Id
			std::string mSnid;		// Social network id
			std::string mSecret;	// Zynga app secret
		};
		
		enum AuthContextType
		{
			eUserSessionToken,		// SN Session auth
			eApplicationToken,
			eStringToken,			// Anonymous auth
			ePublic					// No auth
		};
		
		static AuthMgr& Create(const std::string& appId, const std::string& clientId);
		static void Destroy();
		
		static AuthMgr& Get();
		static AuthMgr* GetPtr();
		
		void UseUserSessionToken(const UserSessionToken& token);
		void UseApplicationToken(const ApplicationToken& token);
		void UseStringToken(const std::string& token);
		void UsePublicToken();
		
		AuthContextType GetAuthContextType();
		void GetUserSessionToken(UserSessionToken& token) const;
		void GetApplicationToken(ApplicationToken& token) const;
		void GetStringToken(std::string& token) const;
		
		void GetClientId(std::string& clientId) const;
		
	protected:
		void SetZid(const std::string& zid);
		
	private:
		// These are platform specific implementations (ios/android)
		bool RegisterDevice();
		bool IssueToken(const std::string& userId, const std::string& zid);
		
		AuthMgr();
		AuthMgr(const std::string& appId, const std::string& clientId);
		
		AuthMgr(AuthMgr const&);
		AuthMgr& operator=(AuthMgr const&);
		
		static AuthMgr* spAuthSingleton;
		
		AuthContextType meAuthContextType;
		UserSessionToken mUserSessionToken;
		ApplicationToken mApplicationToken;
		std::string mStringToken;
		
		std::string mAppId;
		std::string mClientId;
	};
}

#endif /* defined(__DAPI__AuthMgr__) */
