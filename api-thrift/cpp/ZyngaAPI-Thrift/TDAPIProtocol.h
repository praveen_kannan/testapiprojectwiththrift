#ifndef _THRIFT_PROTOCOL_TDAPIPROTOCOL_H_
#define _THRIFT_PROTOCOL_TDAPIPROTOCOL_H_ 1

#include <thrift/protocol/TVirtualProtocol.h>
#include <thrift/protocol/TJSONProtocol.h>

#include <ZyngaAPI-Thrift/gen-cpp/include/dapi_types.h>
using namespace std;

namespace apache { namespace thrift { namespace protocol {

class TDAPIProtocol : public TVirtualProtocol<TDAPIProtocol, TJSONProtocol> {
    public:

        TDAPIProtocol(boost::shared_ptr<TTransport> ptrans);
        ~TDAPIProtocol();

        uint32_t readMessageBegin(std::string& name, TMessageType& messageType, int32_t& seqid);
        uint32_t readMessageEnd();
        uint32_t readStructBegin(std::string& name);
        uint32_t readStructEnd();
        uint32_t readFieldBegin(std::string& name, TType& fieldType, int16_t& fieldId);
        uint32_t readFieldEnd();
        uint32_t readMapBegin(TType& keyType, TType& valType, uint32_t& size);
        uint32_t readMapEnd();
        uint32_t readListBegin(TType& elemType, uint32_t& size);
        uint32_t readListEnd();
        uint32_t readSetBegin(TType& elemType, uint32_t& size);
        uint32_t readSetEnd();
        uint32_t readBool(bool& value);
        // Provide the default readBool() implementation for std::vector<bool>
        using TVirtualProtocol<TDAPIProtocol, TJSONProtocol>::readBool;
        uint32_t readByte(int8_t& byte);
        uint32_t readI16(int16_t& i16);
        uint32_t readI32(int32_t& i32);
        uint32_t readI64(int64_t& i64);
        uint32_t readDouble(double& dub);
        uint32_t readString(std::string& str);
        uint32_t readBinary(std::string& str);
	
		using TVirtualProtocol::writeMessageBegin;
		uint32_t writeMessageBegin(const std::string& name, const TMessageType messageType, const int32_t seqid, const std::string& serviceName);
        uint32_t writeMessageEnd();
        uint32_t writeStructBegin(const char* name);
        uint32_t writeStructEnd();
        uint32_t writeFieldBegin(const char* name, const TType fieldType, const int16_t fieldId);
        uint32_t writeFieldEnd();
        uint32_t writeFieldStop();
        uint32_t writeMapBegin(const TType keyType, const TType valType, const uint32_t size);
        uint32_t writeMapEnd();
        uint32_t writeListBegin(const TType elemType, const uint32_t size);
        uint32_t writeListEnd();
        uint32_t writeSetBegin(const TType elemType, const uint32_t size);
        uint32_t writeSetEnd();
        uint32_t writeBool(const bool value);
        uint32_t writeByte(const int8_t byte);
        uint32_t writeI16(const int16_t i16);
        uint32_t writeI32(const int32_t i32);
        uint32_t writeI64(const int64_t i64);
        uint32_t writeDouble(const double dub);
        uint32_t writeString(const std::string& str);
        uint32_t writeBinary(const std::string& str);
        uint32_t skip(TType type);
	
        uint32_t consumeObject(std::string &str);
        uint32_t consumeArray(std::string &str);
        uint32_t consumeBool(std::string &str);
		uint32_t consumeNumber(std::string &str);
		uint32_t consumeNull(std::string &str);
	
        uint32_t writeBasicString(const std::string &str);
        uint32_t writeBasicDouble(const double num);

        bool isAtEndOfStream(TJSONProtocol::LookaheadReader &reader);

		string stringToken;
	
		uint32_t writeMethodArgumentPrefixData();
	
    private:
		bool IsNumberCharacter(char c, bool bIsFirstChar);
	
		bool WriteKeyValueFromMap(std::string key, std::map<std::string, std::string> map, bool bWriteKey = true);
		void WriteUserToken();
		void WriteAppToken();
		void WriteStringToken();
		
		// Parsing state
        bool startedParse;
        bool startedWrite;
		bool startedDAPIRequest;
		bool startedArguments;
		bool wroteMethodArgumentPrefixData;

        string callName;
		string serviceName;

        // Stack of field names to keep track of when parsing
        std::stack<std::string> currField_;
};

class TDAPIProtocolFactory : public TProtocolFactory {
 public:
    TDAPIProtocolFactory() {}

    virtual ~TDAPIProtocolFactory() {}

    boost::shared_ptr<TProtocol> getProtocol(boost::shared_ptr<TTransport> trans) {
        return boost::shared_ptr<TProtocol>(new TDAPIProtocol(trans));
    }
};


class TSimpleProtocol : public TVirtualProtocol<TSimpleProtocol> {
    public:

    TSimpleProtocol(boost::shared_ptr<TTransport> ptrans);
    ~TSimpleProtocol();

    uint32_t readMessageBegin(std::string& name, TMessageType& messageType, int32_t& seqid);
    uint32_t readString(std::string &str);
    uint32_t readFieldBegin(std::string& name, TType& fieldType, int16_t& fieldId);
	
	using TVirtualProtocol::writeMessageBegin;
    uint32_t writeMessageBegin(const std::string& name, const TMessageType messageType, const int32_t seqid);
	uint32_t writeMessageBegin(const std::string& name, const TMessageType messageType, const int32_t seqid, const std::string& serviceName);
    uint32_t writeDouble(const double dub);
    uint32_t writeString(const std::string &str);
    uint32_t writeChar(uint8_t ch);

    // uint32_t readMessageBegin(std::string& name, TMessageType& messageType, int32_t& seqid);
    uint32_t readMessageEnd();
    uint32_t readStructBegin(std::string& name);
    uint32_t readStructEnd();
    // uint32_t readFieldBegin(std::string& name, TType& fieldType, int16_t& fieldId);
    uint32_t readFieldEnd();
    uint32_t readMapBegin(TType& keyType, TType& valType, uint32_t& size);
    uint32_t readMapEnd();
    uint32_t readListBegin(TType& elemType, uint32_t& size);
    uint32_t readListEnd();
    uint32_t readSetBegin(TType& elemType, uint32_t& size);
    uint32_t readSetEnd();
    uint32_t readBool(bool& value);
    uint32_t readBool(std::vector<bool>::reference value);
    uint32_t readByte(int8_t& byte);
    uint32_t readI16(int16_t& i16);
    uint32_t readI32(int32_t& i32);
    uint32_t readI64(int64_t& i64);
    uint32_t readDouble(double& dub);
    // uint32_t readString(std::string& str);
    uint32_t readBinary(std::string& str);
    // uint32_t writeMessageBegin(const std::string& name, const TMessageType messageType, const int32_t seqid);
    uint32_t writeMessageEnd();
    uint32_t writeStructBegin(const char* name);
    uint32_t writeStructEnd();
    uint32_t writeFieldBegin(const char* name, const TType fieldType, const int16_t fieldId);
    uint32_t writeFieldEnd();
    uint32_t writeFieldStop();
    uint32_t writeMapBegin(const TType keyType, const TType valType, const uint32_t size);
    uint32_t writeMapEnd();
    uint32_t writeListBegin(const TType elemType, const uint32_t size);
    uint32_t writeListEnd();
    uint32_t writeSetBegin(const TType elemType, const uint32_t size);
    uint32_t writeSetEnd();
    uint32_t writeBool(const bool value);
    uint32_t writeByte(const int8_t byte);
    uint32_t writeI16(const int16_t i16);
    uint32_t writeI32(const int32_t i32);
    uint32_t writeI64(const int64_t i64);
    // uint32_t writeDouble(const double dub);
    // uint32_t writeString(const std::string& str);
    uint32_t writeBinary(const std::string& str);
    uint32_t skip(TType type);
 private:
    TTransport* trans_;
    bool finishedRead;

};

/**
 * Constructs input and output protocol objects given transports.
 */
class TSimpleProtocolFactory : public TProtocolFactory {
 public:
    TSimpleProtocolFactory() {}

    virtual ~TSimpleProtocolFactory() {}

    boost::shared_ptr<TProtocol> getProtocol(boost::shared_ptr<TTransport> trans) {
        return boost::shared_ptr<TProtocol>(new TSimpleProtocol(trans));
    }
};

}}} // apache::thrift::protocol
#endif // #define _THRIFT_PROTOCOL_TJSONPROTOCOL_H_ 1
