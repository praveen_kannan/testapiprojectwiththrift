//
//  DapiMgr.cpp
//  DAPI
//
//  Created by bmorishita on 4/11/13.
//  Copyright (c) 2013 jdao. All rights reserved.
//

#include "DapiMgr.h"

#include <ZyngaAPI-Thrift/TDAPIProtocol.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/auth.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/Conversation.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/Experiment.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/Track.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/UserData.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/PlayerId.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/Neighbors.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/Leaderboard.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/ExternalSn.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/Zids.h>

#include <thrift/transport/THttpModClient.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TSSLSocket.h>

#include <boost/lexical_cast.hpp>
#include <iostream>
#include <boost/atomic.hpp>
#include <boost/functional/hash.hpp>

#include <fcntl.h>

using namespace std;
using namespace apache::thrift::transport;
using namespace apache::thrift::protocol;

namespace ZyngaAPI
{
	boost::shared_ptr<TSSLSocketFactory> factory;
	boost::shared_ptr<TSocket> socket;
	boost::shared_ptr<TTransport> httpTransport;
	boost::shared_ptr<TTransport> transport;
	boost::shared_ptr<TProtocol> protocol;
	
	boost::lockfree::queue<DapiMgr::CallbackContext>* DapiMgr::spCallQueue = NULL;
	DapiMgr*		DapiMgr::spSingleton = NULL;
	sem_t*			DapiMgr::spSignalQueue = NULL;
	pthread_t		DapiMgr::sDrainer;
	pthread_mutex_t	DapiMgr::sQuitMutex;
	
	
	DapiMgr& DapiMgr::Create()
	{
		if(!spSingleton)
		{
			spSingleton = new DapiMgr();
			spCallQueue = new boost::lockfree::queue<DapiMgr::CallbackContext>(128);
			
			// Threading
			cout << "boost::lockfree::queue is ";
			if (!spCallQueue->is_lock_free())
				cout << "not ";
			cout << "lockfree" << endl;
			
			if ((spSignalQueue = sem_open("signalQueue", O_CREAT, 0644, 1)) == SEM_FAILED) {
				cout << "failed to create semaphore";
			}
			
			pthread_mutex_init(&sQuitMutex, NULL);
			pthread_mutex_lock(&sQuitMutex);
			pthread_create(&sDrainer, NULL, DrainAsyncCallQueue, NULL);
		}
		return *spSingleton;
	}

	void DapiMgr::Destroy()
	{
		pthread_mutex_unlock(&sQuitMutex);
		sem_post(spSignalQueue);
		pthread_join(sDrainer, NULL);
		sem_close(spSignalQueue);
		
		delete spCallQueue;
		spCallQueue = NULL;
		delete spSingleton;
		spSingleton = NULL;
	}

	DapiMgr& DapiMgr::Get()
	{
		Create();
		return *spSingleton;
	}
	
	DapiMgr* DapiMgr::GetPtr()
	{
		Create();
		return spSingleton;
	}

	TrackClient* DapiMgr::GetTrackClient() const
	{
		return mpTrackClient;
	}
	
	AuthClient* DapiMgr::GetAuthClient() const
	{
		return mpAuthClient;
	}
    
    ConversationClient* DapiMgr::GetConversationClient() const
    {
        return mpConversationClient;
    }

	UserDataClient* DapiMgr::GetUserDataClient() const
	{
		return mpUserDataClient;
	}
    
    ExperimentClient* DapiMgr::GetExperimentClient() const
    {
        return mpExperimentClient;
    }
	
    PlayerIdClient* DapiMgr::GetPlayerIdClient() const
    {
        return mpPlayerIdClient;
    }
    
    NeighborsClient* DapiMgr::GetNeighborsClient() const
    {
        return mpNeighborsClient;
    }

    LeaderboardClient* DapiMgr::GetLeaderboardClient() const
    {
        return mpLeaderboardClient;
    }
    
    ExternalSnClient* DapiMgr::GetExternalSnClient() const
    {
        return mpExternalSnClient;
    }
    
    ZidsClient* DapiMgr::GetZidsClient() const
    {
        return mpZidsClient;
    }
    
	bool DapiMgr::GetStringFromDapiResponse(string& string, const DAPIResponse& response)
	{
		bool bSuccess = true;
		string.clear();
		
		if (response.__isset.error)
		{
			cout << "ERROR in DAPI request, dapiResponse.error: " + response.error.message + "\n" << endl;
			bSuccess = false;
		}
		else
		{
            if(response.calls.empty())
            {
                cout << "ERROR in DAPI request, DAPIResponse was empty. \n" << endl;
				bSuccess = false;
            }
            /**
             Check values set for name and data before calling the copy constructor"
             */
            else if(response.calls[0].__isset.data && !response.calls[0].data.empty())
            {
                try {
                    CallResult c = response.calls[0];
                    if (c.__isset.error)
                    {
                        cout << "ERROR in DAPI request, dapiResponse.calls.error: " + c.error.message + "\n" << endl;
                        bSuccess = false;
                    }
                    else {
                        string = c.data;
                        cout << "  " + string + "\n" << endl;
                    }
                }
                catch (std::exception& e) {
                    cout << "Exception in GetStringFromDapiResponse: " << e.what() << endl;
                    bSuccess = false;
                }
            }
            else
            {
                cout << "ERROR in DAPI request, DAPIResponse data was not set. \n" << endl;
				bSuccess = false;
            }
        }
		return bSuccess;
	}
	
	DapiMgr::DapiMgr():
		mpTrackClient(NULL),
		mpAuthClient(NULL),
        mpConversationClient(NULL),
		mpExperimentClient(NULL),
		mpUserDataClient(NULL),
        mpPlayerIdClient(NULL),
        mpNeighborsClient(NULL),
        mpLeaderboardClient(NULL),
        mpExternalSnClient(NULL),
        mpZidsClient(NULL)
	{
		factory = boost::shared_ptr<TSSLSocketFactory>(new TSSLSocketFactory());
		socket = factory->createSocket("api.zynga.com", 443);
		httpTransport = boost::shared_ptr<TTransport>(new THttpModClient(socket, "api.zynga.com", "/"));
		transport = boost::shared_ptr<TTransport>(new TBufferedTransport(httpTransport));
		protocol = boost::shared_ptr<TProtocol>(new TDAPIProtocol(transport));
		
		mpTrackClient = new TrackClient(protocol);
		mpAuthClient = new AuthClient(protocol);
        mpConversationClient = new ConversationClient(protocol);
		mpUserDataClient = new UserDataClient(protocol);
        mpExperimentClient = new ExperimentClient(protocol);
        mpPlayerIdClient = new PlayerIdClient(protocol);
        mpNeighborsClient = new NeighborsClient(protocol);
        mpLeaderboardClient = new LeaderboardClient(protocol);
        mpExternalSnClient = new ExternalSnClient(protocol);
        mpZidsClient = new ZidsClient(protocol);
    }
    
	DapiMgr::~DapiMgr()
	{
		delete mpTrackClient;
		mpTrackClient = NULL;
		delete mpAuthClient;
		mpAuthClient = NULL;
		delete mpConversationClient;
		mpConversationClient = NULL;
		delete mpUserDataClient;
		mpUserDataClient = NULL;
		delete mpExperimentClient;
		mpExperimentClient = NULL;
		delete mpPlayerIdClient;
		mpPlayerIdClient = NULL;
		delete mpNeighborsClient;
		mpNeighborsClient = NULL;
		delete mpLeaderboardClient;
		mpLeaderboardClient = NULL;
        delete mpExternalSnClient;
        mpExternalSnClient = NULL;
        delete mpZidsClient;
        mpZidsClient = NULL;
	}
	
	// A separate drain thread uses this function to constantly pop pending calls
	void* DapiMgr::DrainAsyncCallQueue(void*)
	{
		while(true)
		{
			// OK to use no-thread safe empty flag, b/c of eventual
			// signalling
			while (!spCallQueue->empty())
			{
				CallbackContext context;
				while(!spCallQueue->pop(context))
				{
				}
				
				// Create the service and call the requested function				
				// Make the blocking call and then process the callback
				if(TrackClient::HandleAsyncTrackCall(context))
				{
				}
				else if(AuthClient::HandleAsyncAuthCall(context))
				{
				}
				else if(ConversationClient::HandleAsyncConversationCall(context))
				{
				}
				else if(ExperimentClient::HandleAsyncExperimentCall(context))
				{
				}
				else if(UserDataClient::HandleAsyncUserDataCall(context))
				{
				}
				else if(PlayerIdClient::HandleAsyncPlayerIdCall(context))
				{
				}
				else if(NeighborsClient::HandleAsyncNeighborsCall(context))
				{
				}
				else if(LeaderboardClient::HandleAsyncLeaderboardCall(context))
				{
				}
                else if(ExternalSnClient::HandleAsyncExternalSnCall(context))
                {
                }
                else if(ZidsClient::HandleAsyncZidsCall(context))
                {
                }
			}
			
			// If empty, wait for signal. Note it might be possible as to
			// when the drain thread has finished checking the queue, it
			// would
			sem_wait(spSignalQueue);
			
			// Check if we can quit
			switch(pthread_mutex_trylock(&sQuitMutex))
			{
				case 0: // Got lock
					pthread_mutex_unlock(&sQuitMutex);
					return NULL;
				case 1: // The mutex was locked
					// Do nothing
					break;
			}
		}
	}
	
	void DapiMgr::AddAsyncCallContext(CallbackContext& context)
	{
		// B/c it uses a fixed size (128) queue, its possible
		// to have a busy loop if there's a sudden burst of api calls
		// and we aren't consuming them fast enough, but highly unlikely
		while (!spCallQueue->push(context))
			;
		
		// If we need to be absolutely sure to drain the queue we always have
		// to post the semaphore b/c otherwise there would always be a scenario
		// where the consumer thread would return to waiting after having checked
		// last entry, while a producer has pushed on to the queue
		sem_post(spSignalQueue);
	}
	
	void DapiMgr::CreateDapiProtocol(boost::shared_ptr<TProtocol>& protocol)
	{
		boost::shared_ptr<TSSLSocketFactory> factory = boost::shared_ptr<TSSLSocketFactory>(new TSSLSocketFactory());
		boost::shared_ptr<TSocket> socket = factory->createSocket("api.zynga.com", 443);
		boost::shared_ptr<TTransport> httpTransport = boost::shared_ptr<TTransport>(new THttpModClient(socket, "api.zynga.com", "/"));
		boost::shared_ptr<TTransport>transport = boost::shared_ptr<TTransport>(new TBufferedTransport(httpTransport));
		protocol = boost::shared_ptr<TProtocol>(new TDAPIProtocol(transport));
	}
}