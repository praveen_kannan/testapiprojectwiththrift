//
//  DapiMgr.h
//  DAPI
//
//  Created by bmorishita on 4/11/13.
//  Copyright (c) 2013 jdao. All rights reserved.
//

#ifndef __DAPI__DapiMgr__
#define __DAPI__DapiMgr__

#include <ZyngaAPI-Thrift/gen-cpp/include/dapi_types.h>
#include <thrift/protocol/TProtocol.h>

#include <semaphore.h>
#include <boost/lockfree/queue.hpp>

// Forward Declarations
class TrackClient;
class DAPIClient;
class ConversationClient;
class UserDataClient;
class AuthClient;
class ExperimentClient;
class PlayerIdClient;
class NeighborsClient;
class LeaderboardClient;
class ExternalSnClient;
class ZidsClient;


namespace ZyngaAPI
{
	//! Main management class that must be created before using any ZyngaAPI-Thrift functionality.
	class DapiMgr
	{
	public:

		typedef void (*ZyngaAPICall)(DAPIResponse response, void* pArgs);
		typedef void (*ZyngaAPICallback)(const DAPIResponse& response, void* pUserData);
		typedef void* ZyngaAPICallArgs;
		
		static DapiMgr& Create();
		static void Destroy();
		~DapiMgr();
		
		static DapiMgr& Get();
		static DapiMgr* GetPtr();

		//! Convenience functions to get module clients, currently the following are supported for end-users:
		TrackClient* GetTrackClient() const;
        ConversationClient* GetConversationClient() const;
		UserDataClient* GetUserDataClient() const;
		ExperimentClient* GetExperimentClient() const;
		PlayerIdClient* GetPlayerIdClient() const;
		NeighborsClient* GetNeighborsClient() const;
   		LeaderboardClient* GetLeaderboardClient() const;
		AuthClient* GetAuthClient() const;
        ExternalSnClient *GetExternalSnClient() const;
        ZidsClient *GetZidsClient() const;
		
		//! Helper functions to get at the string data returned from in a response object
		static bool GetStringFromDapiResponse(std::string& string, const DAPIResponse& response);
		
		
		// For internal use only:		
		//! Must have a trivial destructor for use with boost::lockfree::queue
		struct CallbackContext
		{
			size_t mModuleName;
			size_t mModuleFunctionName;
			
			void* mpArgs;
			void* mpUserData;
			ZyngaAPICallback mpCallback;
			
			CallbackContext()
			:	mModuleName(0),
			mModuleFunctionName(0),
			mpArgs(NULL),
			mpUserData(NULL),
			mpCallback(NULL)
			{
			}
		};
		static void AddAsyncCallContext(CallbackContext& context);
		static void CreateDapiProtocol(boost::shared_ptr<apache::thrift::protocol::TProtocol>& protocol);
		
	private:
		// Private and unimplemented to prevent direct creation, assignment, and copying
		DapiMgr();
		DapiMgr(DapiMgr const&);
		DapiMgr& operator=(DapiMgr const&);
		
		static void* DrainAsyncCallQueue(void*);
				
		static DapiMgr*		spSingleton;
		static boost::lockfree::queue<CallbackContext>* spCallQueue;
		static sem_t*		spSignalQueue;
		static pthread_t	sDrainer;
		static pthread_mutex_t sQuitMutex;
		
		TrackClient*		mpTrackClient;
		AuthClient*			mpAuthClient;
        ConversationClient* mpConversationClient;
		UserDataClient*		mpUserDataClient;
        ExperimentClient*	mpExperimentClient;
        PlayerIdClient*     mpPlayerIdClient;
        NeighborsClient*    mpNeighborsClient;
        LeaderboardClient*  mpLeaderboardClient;
        ExternalSnClient*   mpExternalSnClient;
        ZidsClient*         mpZidsClient;
	};
}
#endif /* defined(__DAPI__DapiMgr__) */
