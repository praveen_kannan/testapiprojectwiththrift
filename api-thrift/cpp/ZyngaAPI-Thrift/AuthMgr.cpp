//
//  AuthMgr.cpp
//  DAPI
//
//  Created by bmorishita on 4/11/13.
//  Copyright (c) 2013 jdao. All rights reserved.
//

#include "AuthMgr.h"

#include <iostream>
#include <stdexcept>

using namespace std;

namespace ZyngaAPI
{
	AuthMgr* AuthMgr::spAuthSingleton = NULL;
	
	AuthMgr& AuthMgr::Create(const string& appId, const string& clientId)
	{
		if(!spAuthSingleton)
		{
			spAuthSingleton = new AuthMgr(appId, clientId);
		}
		return *spAuthSingleton;
	}
	
	void AuthMgr::Destroy()
	{
		delete spAuthSingleton;
		spAuthSingleton = NULL;
	}
	
	AuthMgr& AuthMgr::Get()
	{
		if(!spAuthSingleton)
		{
			throw runtime_error("AuthMgr is uninitialized, call Create before trying to access");
		}
		return *spAuthSingleton;
	}
	
	AuthMgr* AuthMgr::GetPtr()
	{
		return spAuthSingleton;
	}
	
	void AuthMgr::UseUserSessionToken(const UserSessionToken& token)
	{
		mUserSessionToken = token;
		meAuthContextType = eUserSessionToken;
	}
	
	void AuthMgr::UseApplicationToken(const ApplicationToken& token)
	{
		mApplicationToken = token;
		meAuthContextType = eApplicationToken;
	}
	
	void AuthMgr::UseStringToken(const std::string& token)
	{
		mStringToken = token;
		meAuthContextType = eStringToken;
	}
	
	void AuthMgr::UsePublicToken()
	{
		meAuthContextType = ePublic;
	}
	
	AuthMgr::AuthContextType AuthMgr::GetAuthContextType()
	{
		return meAuthContextType;
	}
	
	void AuthMgr::GetUserSessionToken(UserSessionToken& token) const
	{
		token = mUserSessionToken;
	}
	
	void AuthMgr::GetApplicationToken(ApplicationToken& token) const
	{
		token = mApplicationToken;
	}
	
	void AuthMgr::GetStringToken(std::string& token) const
	{
		token = mStringToken;
	}
		
	void AuthMgr::GetClientId(std::string& clientId) const
	{
		clientId = mClientId;
	}
	
	AuthMgr::AuthMgr(const string& appId, const string& clientId):
		mAppId(appId),
		mClientId(clientId),
		meAuthContextType(ePublic)
	{
	}
}
