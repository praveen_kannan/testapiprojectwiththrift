#include "CppClient.h"
#include "DapiMgr.h"
#include "AuthMgr.h"

//#include <ZyngaAPI-Thrift/gen-cpp/include/account.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/Auth.h>
//#include <ZyngaAPI-Thrift/gen-cpp/include/friends.h>
//#include <ZyngaAPI-Thrift/gen-cpp/include/identities.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/Track.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/Conversation.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/UserData.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/Experiment.h>

#include <boost/lexical_cast.hpp>
#include <ZyngaAPI-Thrift/TDAPIProtocol.h>
#include <thrift/transport/THttpModClient.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TSSLSocket.h>

#include <iostream>

using namespace ZyngaAPI;
using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;

using boost::lexical_cast;

static int snRunningAsyncTests = 0;

const static string TEST_EMAIL = "jdao@zynga.com";

// For track verification purposes, appId "5001263" is associated with the dev vertica.
// Use the real time tracker to verify calls: https://stats.zynga.com/dev/tail/tail.html
//
// To test prod, the zynga testbed app uses the appId "80000002".
const static string TEST_APPID = "5001263";

const static string TEST_APPSECRET = "6ea1577c3e6b8678c10af38d26fa09fd";
const static string TEST_ZID = "34852696123";
const static string TEST_FRIEND_ZID = "31111262053";

static void GetRandomHexString(string& output, int length)
{
	output.clear();
	
	std::stringstream sstream;
	for (int i = 0; i < length; i++)
	{
		int nRandomNum = rand() % 16;
		sstream << std::hex << nRandomNum;
	}
	output = sstream.str();
}

//void CppClient::testPlainDAPIInterface() {
//    runDAPITestSimple();
//
//    string method;
//    map<string, string> arguments;
//    string data;
//    Json::Value dict;
//    string snid = "24";
//    string zid;
//    string userId;
//    string generatedPassword;
//    getRandomHexString(generatedPassword, 32);
//    AppToken appToken;
//    appToken.__set_appId(TEST_APPID);
//    appToken.__set_snId(snid);
//    appToken.__set_secret(TEST_APPSECRET);
//    DAPIResponse dapiResponse;
//
//    method = "account.exists";
//    cout << "--" + method + "()" << endl;
//    arguments.clear();
//    arguments["email"] = TEST_EMAIL;
//    runDAPICall(dapiResponse, method, arguments);
//    getString(data, dapiResponse);
//
//    method = "auth.registerDevice";
//    cout << "--" + method + "()" << endl;
//    arguments.clear();
//    arguments["snid"] = snid;
//    arguments["password"] = generatedPassword;
//    runDAPICall(dapiResponse, method, arguments);
//    getDict(dict, dapiResponse);
//    userId = dict.get("userId", "").asString();
//    zid = lexical_cast<string>(dict.get("zid", 0).asLargestUInt());
//
//    method = "auth.issueToken";
//    cout << "--" + method + "()" << endl;
//    arguments.clear();
//    arguments["appId"] = TEST_APPID;
//    arguments["password"] = generatedPassword;
//    arguments["userId"] = userId;
//    arguments["zid"] = zid;
//    runDAPICall(dapiResponse, method, arguments);
//    getDict(dict, dapiResponse);
//    string token = dict.get("token", "").asString();
//
//    method = "friends.areFriends";
//    cout << "--" + method + "()" << endl;
//    arguments.clear();
//    arguments["snid"] = snid;
//    arguments["zid"] = zid;
//    arguments["friendZid"] = TEST_FRIEND_ZID;
//    runDAPICall(dapiResponse, method, arguments, appToken);
//    getString(data, dapiResponse);
//
//    method = "identities.get";
//    cout << "--" + method + "()" << endl;
//    arguments.clear();
//    arguments["zids"] = TEST_ZID;
//    runDAPICall(dapiResponse, method, arguments, token);
//    getString(data, dapiResponse);
//	
//	method = "track.logCount";
//	cout << "--" + method + "()" << endl;
//	arguments.clear();
//	arguments["zid"] = TEST_ZID;
//	arguments["counter"] = "testCounter1";
//	arguments["clientId"] = "testClientId";
//	runDAPICall(dapiResponse, method, arguments, token);
//	getString(data, dapiResponse);
//}

//void CppClient::runDAPITestSimple() {
//    try {
//        cout << "Testing DAPI endpoints using raw JSON string" << endl;
//        boost::shared_ptr<TSSLSocketFactory> factory = boost::shared_ptr<TSSLSocketFactory>(new TSSLSocketFactory());
//        boost::shared_ptr<TSocket> socket = factory->createSocket("api.zynga.com", 443);
//        boost::shared_ptr<TTransport> httpTransport(new THttpModClient(socket, "api.zynga.com", "/"));
//        boost::shared_ptr<TTransport> transport(new TBufferedTransport(httpTransport));
//        boost::shared_ptr<TProtocol> protocol(new TSimpleProtocol(transport));
//
//        DAPIClient client(protocol);
//        
//        transport->open();
//        string requestBody = "{\"c\": [{\"m\":\"account.exists\", \"al\": {\"email\": \"" + TEST_EMAIL + "\"}}]}";
//        string exists_simple;
//        client.call_simple(exists_simple, requestBody);
//        cout << "  " + exists_simple + "\n" << endl;
//        transport->close();
//    } catch (TProtocolException exn) {
//        cout << "Protocol Exception: " << exn.what() << endl;
//    }
//}
//
//void CppClient::runDAPICall(DAPIResponse& dapiResponse, const string& method, map<string, string> args) {
//    boost::shared_ptr<DAPIClient> client;
//    boost::shared_ptr<TTransport> transport;
//    client = getClient();
//    transport = client->getInputProtocol()->getTransport();
//
//    transport->open();
//
//    vector<CallRequest> dapiCalls;
//    CallRequest callRequest;
//    callRequest.__set_method(method);
//    callRequest.__set_argz(args);
//    dapiCalls.push_back(callRequest);
//
//    DAPIRequest dapiRequest;
//    dapiRequest.__set_calls(dapiCalls);
//
//    client->call(dapiResponse, dapiRequest);
//
//    transport->close();
//}
//
//void CppClient::runDAPICall(DAPIResponse& dapiResponse, const string& method, map<string, string> args, const AppToken& appToken) {
//    boost::shared_ptr<DAPIClient> client;
//    boost::shared_ptr<TTransport> transport;
//    client = getClient();
//    transport = client->getInputProtocol()->getTransport();
//
//    transport->open();
//
//    vector<CallRequest> dapiCalls;
//    CallRequest callRequest;
//    callRequest.__set_method(method);
//    callRequest.__set_argz(args);
//    dapiCalls.push_back(callRequest);
//
//    DAPIRequest dapiRequest;
//    dapiRequest.__set_calls(dapiCalls);
//    dapiRequest.__set_appToken(appToken);
//
//    client->call(dapiResponse, dapiRequest);
//
//    transport->close();
//}
//
//void CppClient::runDAPICall(DAPIResponse& dapiResponse, const string& method, map<string, string> args, const UserToken& userToken) {
//    boost::shared_ptr<DAPIClient> client;
//    boost::shared_ptr<TTransport> transport;
//    client = getClient();
//    transport = client->getInputProtocol()->getTransport();
//
//    transport->open();
//
//    vector<CallRequest> dapiCalls;
//    CallRequest callRequest;
//    callRequest.__set_method(method);
//    callRequest.__set_argz(args);
//    dapiCalls.push_back(callRequest);
//
//    DAPIRequest dapiRequest;
//    dapiRequest.__set_calls(dapiCalls);
//    dapiRequest.__set_userToken(userToken);
//
//    client->call(dapiResponse, dapiRequest);
//
//    transport->close();
//}
//
//void CppClient::runDAPICall(DAPIResponse& dapiResponse, const string& method, map<string, string> args, const string& stringToken) {
//    boost::shared_ptr<DAPIClient> client;
//    boost::shared_ptr<TTransport> transport;
//    client = getClient();
//    transport = client->getInputProtocol()->getTransport();
//
//    transport->open();
//
//    vector<CallRequest> dapiCalls;
//    CallRequest callRequest;
//    callRequest.__set_method(method);
//    callRequest.__set_argz(args);
//    dapiCalls.push_back(callRequest);
//
//    DAPIRequest dapiRequest;
//    dapiRequest.__set_calls(dapiCalls);
//    dapiRequest.__set_stringToken(stringToken);
//
//    client->call(dapiResponse, dapiRequest);
//
//    transport->close();
//}

// Call down to DAPI from higher level generated services
void CppClient::testWebServices()
{
	DapiMgr::Create();
	AuthMgr::Create(TEST_APPID, "2");
	FakeAuthInitialization();
	
	string data;
	string snid = "24";
	string zid;

	DAPIResponse dapiResponse;
	
	boost::shared_ptr<TSSLSocketFactory> factory = boost::shared_ptr<TSSLSocketFactory>(new TSSLSocketFactory());
	boost::shared_ptr<TSocket> socket = factory->createSocket("api.zynga.com", 443);
	boost::shared_ptr<TTransport> httpTransport(new THttpModClient(socket, "api.zynga.com", "/"));
	boost::shared_ptr<TTransport> transport(new TBufferedTransport(httpTransport));
	boost::shared_ptr<TProtocol> protocol(new TDAPIProtocol(transport));
	
	transport->open();
//	accountClient account(protocol);
//	friendsClient friends(protocol);
//	identitiesClient identities(protocol);
//	
//	cout << "--account.exists()" << endl;
//	transport->open();
//	account.exists(dapiResponse, TEST_EMAIL);
//	getString(data, dapiResponse);
//	
//	cout << "--friends.areFriends()" << endl;
//	friends.areFriends(dapiResponse, snid, TEST_ZID, TEST_FRIEND_ZID);
//	getString(data, dapiResponse);
//	
//	cout << "--identities.get()" << endl;
//	std::vector<std::string> zids;
//	
//	AuthMgr::UserSessionToken token;
//	AuthMgr::Get().GetUserSessionToken(token);
//	zids.push_back(token.mZid);
//	identities.get(dapiResponse, zids);
//	getString(data, dapiResponse);
	
	transport->close();
	
	AuthMgr::Destroy();
	DapiMgr::Destroy();
}

//! Test track
void CppClient::testTrack()
{
	DAPIResponse dapiResponse;
	Json::Value dict;
	
	DapiMgr::Create();
	AuthMgr::Create(TEST_APPID, "2");
	FakeAuthInitialization();
	
	cout << "--Track.logCount()" << endl;
	map<string, string> optionalArgs;
	optionalArgs.insert(std::pair<string,string>("kingdom","testKingdom"));
	optionalArgs.insert(std::pair<string,string>("phylum","testPhylum"));
	
	TrackLogCountArgs args;
	args.counter = "test_counter";
	args.__set_phylum("testPhylum");
	args.__set_kingdom("testKingdom");
	args.__set_class_("testClass");
	
	DapiMgr::Get().GetTrackClient()->LogCount(dapiResponse, args);
	getDict(dict, dapiResponse);

	DapiMgr::Get().GetTrackClient()->LogCount(dapiResponse, args);
	getDict(dict, dapiResponse);
	
	TrackLogCountArgs args2;
	args2.counter = "test_counter";
	args2.phylum = "badTestPhylum";		// Will not be written (optional arg is not using setter)
	args2.kingdom = "badTestKingdom";	// Will not be written (optional arg is not using setter)
	args2.class_ = "badTestClass";		// Will not be written (optional arg is not using setter)
	args2.__set_family("testFamily");
	DapiMgr::Get().GetTrackClient()->LogCount(dapiResponse, args2);
	getDict(dict, dapiResponse);
	
	
	// Test logCountDetail (verified in vertica - s_zt_zasp)
	cout << "Test logCountDetail" << endl;
	TrackLogCountDetailArgs logCountDetailArgs;
	logCountDetailArgs.__set_string1("testString1");
	logCountDetailArgs.__set_number1(3);
	DapiMgr::Get().GetTrackClient()->LogCountDetail(dapiResponse, logCountDetailArgs);
	getDict(dict, dapiResponse);
	
	// Test logVisit (verified in vertica - s_zt_dau)
	cout << "Test logVisit" << endl;
	TrackLogVisitArgs logVisitArgs;
	logVisitArgs.__set_isActive(true);
	time_t timer;
	time(&timer);
	logVisitArgs.__set_clientDeviceTs(timer);
	DapiMgr::Get().GetTrackClient()->LogVisit(dapiResponse, logVisitArgs);
	getDict(dict, dapiResponse);

	// Test logAssociate (verified in vertica s_zt_associate)
	cout << "Test logAssociate" << endl;
	TrackLogAssociateArgs logAssociateArgs;
	logAssociateArgs.attribute = "testAttribute";
	logAssociateArgs.value1 = "testValue1";
	logAssociateArgs.__set_kingdom("testKingdom");
	DapiMgr::Get().GetTrackClient()->LogAssociate(dapiResponse, logAssociateArgs);
	getDict(dict, dapiResponse);

	// Test buildSendKey (not logged in vertica by design)
	cout << "Test buildSendKey" << endl;
	TrackBuildSendKeyArgs buildSendKeyArgs;
	buildSendKeyArgs.channel = "testChannel";
	DapiMgr::Get().GetTrackClient()->BuildSendKey(dapiResponse, buildSendKeyArgs);
	getDict(dict, dapiResponse);
	

	// Test logMessage (verified in vertica)
	cout << "Test logMessage" << endl;
	TrackLogMessageArgs logMessageArgs;
	//vector<string> members = dict.getMemberNames();
	logMessageArgs.sendKey = dapiResponse.calls[0].data;
	vector<string> zidList;
	zidList.push_back("1234");
	zidList.push_back("4321");
	logMessageArgs.toZidList = zidList;
	logMessageArgs.channel = "testChannel";
	DapiMgr::Get().GetTrackClient()->LogMessage(dapiResponse, logMessageArgs);
	getDict(dict, dapiResponse);
	
	AuthMgr::Destroy();
	DapiMgr::Destroy();
}

static const int kNumAsyncCalls = 4;
void CppClient::testAsync()
{
	DAPIResponse dapiResponse;
	Json::Value dict;
	
	DapiMgr::Create();
	AuthMgr::Create(TEST_APPID, "2");
	FakeAuthInitialization();
	
	pthread_t aLogCountThreadIds[kNumAsyncCalls];
	pthread_t aSetLevelThreadIds[kNumAsyncCalls];
	for(int i = 0; i < kNumAsyncCalls; ++i)
	{
		pthread_create(&aLogCountThreadIds[i], NULL, AsyncLogCountTester, (void*)i);
		pthread_create(&aSetLevelThreadIds[i], NULL, AsyncSetLevelTester, (void*)i);
	}
	
	for(int i = 0; i < kNumAsyncCalls; ++i)
	{
		pthread_join(aLogCountThreadIds[i], NULL);
		pthread_join(aSetLevelThreadIds[i], NULL);
	}

	snRunningAsyncTests = 2;
	while(snRunningAsyncTests)
	{
	}
	
	AuthMgr::Destroy();
	DapiMgr::Destroy();
}

void* CppClient::AsyncLogCountTester(void* val)
{
	int nVal = (int)val;
	TrackLogCountArgs args;
	args.counter = "test_counter";
	args.__set_phylum("testPhylum");
	args.__set_kingdom("testKingdom");
	args.__set_class_("testClass");
	args.__set_value(nVal);
	TrackClient::AsyncLogCount(&ZyngaAPILogCountCallback, args, val);
	return NULL;
}

void* CppClient::AsyncSetLevelTester(void* val)
{
	int nVal = (int)val;
	UserDataSetLevelArgs setLevelArgs;
	setLevelArgs.snid = "24";
	setLevelArgs.appId = TEST_APPID;
	
	ostringstream ss;
	ss << nVal;
	setLevelArgs.level = ss.str();
	UserDataClient::AsyncSetLevel(&ZyngaAPISetLevelCallback, setLevelArgs, val);
	return NULL;
}

void CppClient::testConversation()
{
	DAPIResponse dapiResponse;
	Json::Value dict;
	
	DapiMgr::Create();
	AuthMgr::Create(TEST_APPID, "2");
	FakeAuthInitialization();
	
	cout << "Test conversation create" << endl;
	ConversationCreateArgs args;
	args.__set_id("testConversation");
	time_t timer;
	time(&timer);
	std::stringstream ss;
	ss << timer;
	std::string ts = ss.str();
	args.__set_debug_dot_networkCallTimeMS(ts);
	DapiMgr::Get().GetConversationClient()->Create(dapiResponse, args);
	getDict(dict, dapiResponse);
	
	cout << "Test conversation get" << endl;
	ConversationGetArgs getArgs;
	getArgs.ids.push_back("testConversation");
	getArgs.__set_filters_dot_range_dot_begin(ts);
	DapiMgr::Get().GetConversationClient()->Get(dapiResponse, getArgs);
	getDict(dict, dapiResponse);
	
	cout << "Test conversation remove" << endl;
	ConversationRemoveArgs removeArgs;
	removeArgs.id = "testConversation";
	DapiMgr::Get().GetConversationClient()->Remove(dapiResponse, removeArgs);
	getDict(dict, dapiResponse);
	
	AuthMgr::Destroy();
	DapiMgr::Destroy();
}

void CppClient::testUserData()
{
	DAPIResponse dapiResponse;
	Json::Value dict;
	
	DapiMgr::Create();
	AuthMgr::Create(TEST_APPID, "2");
	FakeAuthInitialization();
	
	// Begin test
	cout << "Test setLevelArgs" << endl;
	UserDataSetLevelArgs setLevelArgs;
	setLevelArgs.snid = "24";
	setLevelArgs.appId = TEST_APPID;
	setLevelArgs.level = "3";
	DapiMgr::Get().GetUserDataClient()->SetLevel(dapiResponse, setLevelArgs);
	getDict(dict, dapiResponse);
	
	cout << "Test getLevelArgs";
	UserDataGetLevelArgs getLevelArgs;
	getLevelArgs.snid = "24";
	getLevelArgs.appId = TEST_APPID;
	DapiMgr::Get().GetUserDataClient()->GetLevel(dapiResponse, getLevelArgs);
	getDict(dict, dapiResponse);
	// End test
	
	AuthMgr::Destroy();
	DapiMgr::Destroy();
}

void CppClient::testExperiment()
{
    DAPIResponse dapiResponse;
    Json::Value dict;
    
    DapiMgr::Create();
    AuthMgr::Create(TEST_APPID, "2");
	FakeAuthInitialization();
    
    cout << "Test Experiment Get"<< endl;
    ExperimentGetArgs expArgs;
    expArgs.__set_experimentVer("1");
    expArgs.__set_experimentName("ExperimentUnitTest3");
    expArgs.__set_overrideSnid("1");
    
    DapiMgr::Get().GetExperimentClient()->Get(dapiResponse, expArgs);
    getDict(dict, dapiResponse);
    
    AuthMgr::Destroy();
	DapiMgr::Destroy();
}

//boost::shared_ptr<DAPIClient> CppClient::getClient() {
//    boost::shared_ptr<TSSLSocketFactory> factory = boost::shared_ptr<TSSLSocketFactory>(new TSSLSocketFactory());
//    boost::shared_ptr<TSocket> socket = factory->createSocket("api.zynga.com", 443);
//    boost::shared_ptr<TTransport> httpTransport(new THttpModClient(socket, "api.zynga.com", "/"));
//    boost::shared_ptr<TTransport> transport(new TBufferedTransport(httpTransport));
//    boost::shared_ptr<TProtocol> protocol(new TDAPIProtocol(transport));
//    boost::shared_ptr<DAPIClient> client(new DAPIClient(protocol));
//    return client;
//}

bool GetValueFromDapiResponse(Json::Value& value, const DAPIResponse& response)
{
	bool bSuccess = true;
	
	// Conditionals to avoid unecessary assert
	if(value.type() == Json::nullValue ||
	   value.type() == Json::arrayValue ||
	   value.type() == Json::objectValue) {
		value.clear();
	}
	
	string valueString;
	DapiMgr::GetStringFromDapiResponse(valueString, response);
	uint64_t length = valueString.length();
	const char* c = valueString.c_str();
	
	if (length != 0)
	{
		Json::Reader reader;
		bool parsingSuccessful = reader.parse(c, c + length, value);
		if (!parsingSuccessful)
		{
			std::cout << "Failed to parse configuration\n" << reader.getFormattedErrorMessages();
			bSuccess = false;
		}
	}
	return bSuccess;
}

bool CppClient::FakeAuthInitialization()
{
	const int kMobileAnonymousSnid = 24;
	
	// TODO: We should not be generating a new password every time...
	string generatedPassword;
	GetRandomHexString(generatedPassword, 32);
	
	DAPIResponse dapiResponse;
	AuthMgr::Get().UsePublicToken();
	AuthClient* pAuth = DapiMgr::Get().GetAuthClient();
	
	cout << "--Auth.registerDevice()" << endl;
	AuthRegisterDeviceArgs registerDeviceArgs;
	registerDeviceArgs.password = generatedPassword;
	pAuth->RegisterDevice(dapiResponse, registerDeviceArgs);
	Json::Value value;
	GetValueFromDapiResponse(value, dapiResponse);
	string userId = value.get("userId", "").asString();
	string zid = value.get("zid","").asString();
	
	cout << "--Auth.issueToken()" << endl;
	AuthIssueTokenArgs args;
	args.password = generatedPassword;
	args.appId = TEST_APPID;
	args.__set_userId(userId);
	args.__set_zid(zid);
	
	pAuth->IssueToken(dapiResponse, args);
	GetValueFromDapiResponse(value, dapiResponse);
	
	AuthMgr::UserSessionToken token;
	token.mZid = zid;
	token.mAppId = TEST_APPID;
	token.mnSnid = kMobileAnonymousSnid;
	token.mSession["access_token"] = value.get("token","").asString();
	AuthMgr::Get().UseUserSessionToken(token);
	return true;
}

void CppClient::getString(string& str, DAPIResponse& d) {
    str.clear();
    if (d.__isset.error) {
        cout << "ERROR in DAPI request, dapiResponse.error: " + d.error.message + "\n" << endl;
    }
    else {
        CallResult c = d.calls[0];
        if (c.__isset.error) {
            cout << "ERROR in DAPI request, dapiResponse.calls.error: " + c.error.message + "\n" << endl;
        }
        else {
            str = c.data;
            cout << "  " + str + "\n" << endl;
        }
    }
}

void CppClient::getDict(Json::Value& dict, DAPIResponse& d) {
	// Conditionals to avoid unecessary assert
	if(dict.type() == Json::nullValue ||
	   dict.type() == Json::arrayValue ||
	   dict.type() == Json::objectValue) {
		dict.clear();
	}
	
    string val;
    getString(val, d);
    uint64_t length = val.length();
    const char* c = val.c_str();

    if (length != 0) {
        Json::Reader reader;
        bool parsingSuccessful = reader.parse(c, c + length, dict);
        if (!parsingSuccessful) {
            std::cout  << "Failed to parse configuration\n" << reader.getFormattedErrorMessages();
            return;
        }
    }
}

void CppClient::getRandomHexString(string& str, const int len) {
    static const char alphanum[] = "0123456789abcdef";
    str.clear();
    for (int i = 0; i < len; ++i) {
        str += alphanum[rand() % (sizeof(alphanum) - 1)];
    }
}

void CppClient::ZyngaAPILogCountCallback(const DAPIResponse& response, void* pUserData)
{
	Json::Value value;
	GetValueFromDapiResponse(value, response);
	if((int)pUserData == kNumAsyncCalls - 1)
	{
		snRunningAsyncTests--;
	}
}

void CppClient::ZyngaAPISetLevelCallback(const DAPIResponse& response, void* pUserData)
{
	Json::Value value;
	GetValueFromDapiResponse(value, response);
	if((int)pUserData == kNumAsyncCalls - 1)
	{
		snRunningAsyncTests--;
	}
}