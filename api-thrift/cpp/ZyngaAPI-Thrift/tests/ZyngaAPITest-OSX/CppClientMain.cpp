//
//  CppClientMain.cpp
//  DAPI
//
//  Created by Praveen Kannan on 4/3/13.
//  Copyright (c) 2013 jdao. All rights reserved.
//

#include "CppClientMain.h"
#include "CppClient.h"
#include <boost/lexical_cast.hpp>
#include <thrift/protocol/TDAPIProtocol.h>
#include <thrift/transport/THttpModClient.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TSSLSocket.h>

#include <iostream>

using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;

using boost::lexical_cast;

int main(int argc, char** argv) {
    try {
        CppClient cppClient;
        // manual invocation of DAPI
		
//        cout << "Testing Plain DAPI Interface" << endl;
//        cppClient.testPlainDAPIInterface();
		
		cout << "Testing Web Service-based DAPI Interface\n" << endl;
		cppClient.testWebServices();
		
		cout << "Testing Track\n" << endl;
		cppClient.testTrack();
		
		cout << "Testing Conversation\n" << endl;
		cppClient.testConversation();
		
		cout << "Testing UserData\n" << endl;
		cppClient.testUserData();
		
		cout << "Testing Async\n" << endl;
		cppClient.testAsync();
		
        cout << "Testing Experiment\n" << endl;
		cppClient.testExperiment();
        
    } catch (TException &tx) {
        printf("ERROR: %s\n", tx.what());
    }
}