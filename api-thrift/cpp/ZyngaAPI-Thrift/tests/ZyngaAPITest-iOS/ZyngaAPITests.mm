//
//  ZyngaAPITests.m
//  DAPI
//
//  Created by Praveen Kannan on 4/3/13.
//  Copyright (c) 2013 jdao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GHUnitIOS/GHUnit.h>
#import "CppClient.h"

@interface ZyngaAPITests : GHAsyncTestCase {
}
@end

@implementation ZyngaAPITests

//-(void)testSampleCalls
//{
//    [self prepare];
//    CppClient cppClient;
//    // manual invocation of DAPI
//    NSLog(@"Testing Plain DAPI Interface");
//    cppClient.testPlainDAPIInterface();
//}

-(void)testWebServices
{
	CppClient cppClient;
	cout << "Testing Web Service-based DAPI Interface\n" << endl;
	cppClient.testWebServices();
}

-(void)testTrack
{
	CppClient cppClient;
	cout << "Testing Track\n" << endl;
	cppClient.testTrack();
}

-(void)testConversation
{
	CppClient cppClient;
	cout << "Testing Conversation\n" << endl;
	cppClient.testConversation();
}

-(void)testExperiment
{
	CppClient cppClient;
	cout << "Testing Experiment\n" << endl;
	cppClient.testExperiment();
}

-(void)testUserData
{
	CppClient cppClient;
	cout << "Testing UserData\n" << endl;
	cppClient.testUserData();
}

-(void)testAsync
{
	CppClient cppClient;
	cout << "Testing Async\n" << endl;
	cppClient.testAsync();
}

#pragma mark - Setup

- (BOOL)shouldRunOnMainThread {
    // By default NO, but if you have a UI test or test dependent on running on the main thread return YES.
    // Also an async test that calls back on the main thread, you'll probably want to return YES.
    return NO;
}

- (void)setUpClass {
    // Run at start of all tests in the class
    
    // set the connection manager to burst every 3 seconds
}

- (void)tearDownClass {
    // Run at end of all tests in the class
}

- (void)setUp {
    // Run before each test method
}

- (void)tearDown {
    // Run after each test method
}

@end
