//
//  main.m
//  ZyngaAPITest-iOS
//
//  Created by Praveen Kannan on 4/3/13.
//  Copyright (c) 2013 jdao. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, @"GHUnitIOSAppDelegate");
    }
}
