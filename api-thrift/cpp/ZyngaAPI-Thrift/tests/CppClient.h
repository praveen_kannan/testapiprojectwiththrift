#ifndef _CPPCLIENT_H_
#define _CPPCLIENT_H_ 1

//#include <ZyngaAPI-Thrift/gen-cpp/include/DAPI.h>
#include <ZyngaAPI-Thrift/gen-cpp/include/dapi_types.h>
#include "json.h"
       

using namespace std;
using boost::shared_ptr;

class CppClient {
public:	
//    void testPlainDAPIInterface();
    void runDAPITestSimple();
	void testWebServices();
	void testTrack();
	void testConversation();
	void testUserData();
    void testExperiment();

	void testAsync();

	bool FakeAuthInitialization();
	
//    void runDAPICall(DAPIResponse& dapiResponse, const string& method, map<string, string> args);
//    void runDAPICall(DAPIResponse& dapiResponse, const string& method, map<string, string> args, const AppToken& appToken);
//    void runDAPICall(DAPIResponse& dapiResponse, const string& method, map<string, string> args, const UserToken& userToken);
//    void runDAPICall(DAPIResponse& dapiResponse, const string& method, map<string, string> args, const string& stringToken);

//    boost::shared_ptr<DAPIClient> getClient();
    void getString(string& str, DAPIResponse& d);
    void getDict(Json::Value& dict, DAPIResponse& d);
    void getRandomHexString(string& str, const int len);
	
	static void* AsyncLogCountTester(void*);
	static void* AsyncSetLevelTester(void*);
	static void ZyngaAPILogCountCallback(const DAPIResponse& response, void* pUserData);
	static void ZyngaAPISetLevelCallback(const DAPIResponse& response, void* pUserData);
};
#endif
