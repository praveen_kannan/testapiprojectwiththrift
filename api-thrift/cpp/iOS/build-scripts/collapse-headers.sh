#!/bin/bash

set -e

set +u
if [ "$ULIB_MASTER_SCRIPT_RUNNING" == "$PROJECT_NAME" ]
then
    # Nothing for the slave script to do
    exit 0
fi
set -u


mkdir -p "${ZDK_BUILD_TMP_DIR}/includes/${PROJECT_NAME}"
mkdir -p "${ZDK_BUILD_TMP_DIR}/private_includes/${PROJECT_NAME}"

# resources aren't cleaned for libZyngaSDK.a + ZyngaSDK.framework
rm -rf "${ZDK_BUILD_TMP_DIR}/resources/${PROJECT_NAME}"
mkdir -p "${ZDK_BUILD_TMP_DIR}/resources/${PROJECT_NAME}"

echo "Collect public headers + update imports"

for HEADER_DIR in `ls -l "${ZDK_BUILD_TMP_DIR}/includes" | grep '^d' | awk '{print $NF}' | grep -v "^${PROJECT_NAME}$"`
do
	for HEADER_FILE in `find "${ZDK_BUILD_TMP_DIR}/includes/$HEADER_DIR" -name "*.h"`
	do
		HEADER_FILE_BASENAME=`basename "$HEADER_FILE"`
		
		if [[ "$ZDK_TARGET" == "3p" ]]
		then
			sed -e "s/^\(#i[mn][pc][ol][ru][td]e*\) <Zynga.*\/\(.*\.h\)>/\1 \"\2\"/" "$HEADER_FILE" > "${ZDK_BUILD_TMP_DIR}/includes/${PROJECT_NAME}/${HEADER_FILE_BASENAME}"
		else
			sed -e "s/^\(#i[mn][pc][ol][ru][td]e*\) <Zynga.*\/\(.*\.h\)>/\1 <${PROJECT_NAME}\/\2>/" "$HEADER_FILE" > "${ZDK_BUILD_TMP_DIR}/includes/${PROJECT_NAME}/${HEADER_FILE_BASENAME}"
		fi
	done
done

echo "Collect private headers + update imports"

for HEADER_DIR in `ls -l "${ZDK_BUILD_TMP_DIR}/private_includes" | grep '^d' | awk '{print $NF}' | grep -v "^${PROJECT_NAME}$"`
do
	for HEADER_FILE in `find "${ZDK_BUILD_TMP_DIR}/private_includes/$HEADER_DIR" -name "*.h"`
	do
		HEADER_FILE_BASENAME=`basename "$HEADER_FILE"`
		
		if [[ "$ZDK_TARGET" == "3p" ]]
		then
			sed -e "s/^\(#i[mn][pc][ol][ru][td]e*\) <Zynga.*\/\(.*\.h\)>/\1 \"\2\"/" "$HEADER_FILE" > "${ZDK_BUILD_TMP_DIR}/private_includes/${PROJECT_NAME}/${HEADER_FILE_BASENAME}"
		else
			sed -e "s/^\(#i[mn][pc][ol][ru][td]e*\) <Zynga.*\/\(.*\.h\)>/\1 <${PROJECT_NAME}\/\2>/" "$HEADER_FILE" > "${ZDK_BUILD_TMP_DIR}/private_includes/${PROJECT_NAME}/${HEADER_FILE_BASENAME}"
		fi
	done
done

echo "Collect resources"

mkdir -p "${ZDK_BUILD_TMP_DIR}/resources/${PROJECT_NAME}"
for RESOURCE_DIR in `ls -l "${ZDK_BUILD_TMP_DIR}/resources" | grep '^d' | awk '{print $NF}' | grep -v "^${PROJECT_NAME}$"`
do
	cp -a "${ZDK_BUILD_TMP_DIR}/resources/${RESOURCE_DIR}/" "${ZDK_BUILD_TMP_DIR}/resources/${PROJECT_NAME}"
done