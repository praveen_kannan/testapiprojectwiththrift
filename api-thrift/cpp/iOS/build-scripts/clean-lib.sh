#!/bin/bash

set -e

set +u
if [ "$ULIB_MASTER_SCRIPT_RUNNING" == "$PROJECT_NAME" ]
then
    # Nothing for the slave script to do
    exit 0
fi
set -u


echo "Clean ${PRODUCT_NAME} from ${ZDK_BUILD_TMP_DIR}"

rm -rf "${ZDK_BUILD_TMP_DIR}/includes/${PROJECT_NAME}"
rm -rf "${ZDK_BUILD_TMP_DIR}/private_includes/${PROJECT_NAME}"


set +u
if [ "${INSTALL_PROJECT}" != "$PROJECT_NAME" ]
then
	# we aren't installing so don't delete files from the install dir
	exit 0
fi
set -u


echo "Clean ${PRODUCT_NAME} from ${ZDK_INSTALL_DIR}"

rm -rf "${ZDK_INSTALL_DIR}/includes/${PROJECT_NAME}"
rm -rf "${ZDK_INSTALL_DIR}/resources/${PROJECT_NAME}"
rm -f "${ZDK_INSTALL_DIR}/lib${PROJECT_NAME}.a"
rm -rf "${ZDK_INSTALL_DIR}-frameworks/${PROJECT_NAME}.framework"