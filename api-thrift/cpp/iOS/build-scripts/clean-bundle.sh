#!/bin/bash

set -e
set -u

echo "Clean ${PRODUCT_NAME} from ${ZDK_BUILD_TMP_DIR}"
rm -rf "${ZDK_BUILD_TMP_DIR}/resources/${PROJECT_NAME}"
