#!/bin/sh

set -e

set +u
if [ "$ULIB_MASTER_SCRIPT_RUNNING" == "$PROJECT_NAME" ]
then
    # Nothing for the slave script to do
    exit 0
fi
export ULIB_MASTER_SCRIPT_RUNNING=${PROJECT_NAME}

if [ ! -x "$XCODEBUILD" ]
then
	XCODEBUILD=xcodebuild
fi
set -u


if [[ "$SDK_NAME" =~ ([A-Za-z]+) ]]
then
    ULIB_SDK_PLATFORM=${BASH_REMATCH[1]}
else
    echo "Could not find platform name from SDK_NAME: $SDK_NAME"
    exit 1
fi

if [[ "$SDK_NAME" =~ ([0-9]+.*$) ]]
then
    ULIB_SDK_VERSION=${BASH_REMATCH[1]}
else
    echo "Could not find sdk version  from SDK_NAME: $SDK_NAME"
    exit 1
fi

if [[ "$ULIB_SDK_PLATFORM" = "iphoneos" ]]
then
    ULIB_OTHER_PLATFORM=iphonesimulator
else
    ULIB_OTHER_PLATFORM=iphoneos
fi

if [[ "$BUILT_PRODUCTS_DIR" =~ (.*)$ULIB_SDK_PLATFORM$ ]]
then
    ULIB_OTHER_BUILT_PRODUCTS_DIR="${BASH_REMATCH[1]}${ULIB_OTHER_PLATFORM}"
else
    echo "Could not find $UFW_SDK_PLATFORM in $BUILT_PRODUCTS_DIR"
    exit 1
fi


set +u
if [ "${INSTALL_PROJECT}" == "$PROJECT_NAME" ]
then
	echo $XCODEBUILD -project "${PROJECT_FILE_PATH}" -target "${TARGET_NAME}" -configuration "${CONFIGURATION}" -sdk ${ULIB_OTHER_PLATFORM}${ULIB_SDK_VERSION} BUILD_DIR="${BUILD_DIR}" CONFIGURATION_TEMP_DIR="${PROJECT_TEMP_DIR}/${CONFIGURATION}-${ULIB_OTHER_PLATFORM}" $ACTION
	$XCODEBUILD -project "${PROJECT_FILE_PATH}" -target "${TARGET_NAME}" -configuration "${CONFIGURATION}" -sdk ${ULIB_OTHER_PLATFORM}${ULIB_SDK_VERSION} BUILD_DIR="${BUILD_DIR}" CONFIGURATION_TEMP_DIR="${PROJECT_TEMP_DIR}/${CONFIGURATION}-${ULIB_OTHER_PLATFORM}" $ACTION
fi
set -u


echo "Copy headers to ${ZDK_BUILD_TMP_DIR}"
SOURCE_DIR=${PROJECT_FILE_PATH}/../../${PROJECT_NAME}

rm -rf "${ZDK_BUILD_TMP_DIR}/includes"
rm -rf "${ZDK_BUILD_TMP_DIR}/private_includes"
mkdir -p "${ZDK_BUILD_TMP_DIR}/includes"
mkdir -p "${ZDK_BUILD_TMP_DIR}/private_includes"

if [[ -d "${TARGET_BUILD_DIR}/includes/${PRODUCT_NAME}" ]]
then

	# Copy the public headers to the tmp dir while preserving any folder hierarchy that exists
	# Note that xCode will flatten the public headers and place them at a top level directory, but
	# the way thrift generated code expects fully qualified header paths is not compatabile with this
	# flattened structure -- so here their folder structure is being reconstituted during the copy
	for f in $TARGET_BUILD_DIR/includes/${PRODUCT_NAME}/*
	do
		pushd ${SOURCE_DIR} > /dev/null 2>&1
		SRC_HEADER=$(find . -name $(basename $f)) > /dev/null 2>&1
		rsync -Raz $SRC_HEADER ${ZDK_BUILD_TMP_DIR}/includes/${PRODUCT_NAME} > /dev/null 2>&1
		popd > /dev/null 2>&1
	done
fi

if [[ -d "${TARGET_BUILD_DIR}/private_includes/${PRODUCT_NAME}" ]]
then
	cp -r "${TARGET_BUILD_DIR}/private_includes/${PRODUCT_NAME}" "${ZDK_BUILD_TMP_DIR}/private_includes"
fi

if [ "$PROJECT_NAME" != "$PRODUCT_NAME" ]
then
	if [[ -d "${ZDK_BUILD_TMP_DIR}/includes/${PRODUCT_NAME}" ]]
	then
		mkdir -p "${ZDK_BUILD_TMP_DIR}/includes/${PROJECT_NAME}"
		mv "${ZDK_BUILD_TMP_DIR}/includes/${PRODUCT_NAME}/"* "${ZDK_BUILD_TMP_DIR}/includes/${PROJECT_NAME}"
		rm -rf "${ZDK_BUILD_TMP_DIR}/includes/${PRODUCT_NAME}"
	fi
	if [[ -d "${ZDK_BUILD_TMP_DIR}/private_includes/${PRODUCT_NAME}" ]]
	then
		mkdir -p "${ZDK_BUILD_TMP_DIR}/private_includes/${PROJECT_NAME}"
		mv "${ZDK_BUILD_TMP_DIR}/private_includes/${PRODUCT_NAME}/"* "${ZDK_BUILD_TMP_DIR}/private_includes/${PROJECT_NAME}"
		rm -rf "${ZDK_BUILD_TMP_DIR}/private_includes/${PRODUCT_NAME}"
	fi
fi

echo "Remove internal content from ${ZDK_TARGET} headers"

if [[ -d "${ZDK_BUILD_TMP_DIR}/includes/${PROJECT_NAME}" ]]
then
	for HEADER_FILE in `find "${ZDK_BUILD_TMP_DIR}/includes/${PROJECT_NAME}" -regex ".*\.hp*"`
	do
		awk -f "${ZDK_BUILD_SCRIPTS_DIR}/clean-${ZDK_TARGET}-headers.awk" "$HEADER_FILE" > "${HEADER_FILE}.tmp"
		mv "$HEADER_FILE.tmp" "$HEADER_FILE"
	done
fi


 set +u
 if [ "${INSTALL_PROJECT}" != "$PROJECT_NAME" ]
 then
 	# we're not installing  so don't create the universal static lib
 	exit 0
 fi
 set -u


echo "Create universal static library"

mkdir -p "${ZDK_INSTALL_DIR}"

echo "$PLATFORM_DEVELOPER_BIN_DIR/libtool" -static "${BUILT_PRODUCTS_DIR}/${EXECUTABLE_PATH}" "${ULIB_OTHER_BUILT_PRODUCTS_DIR}/${EXECUTABLE_PATH}" -o "${ZDK_INSTALL_DIR}/${EXECUTABLE_PATH}"
"$PLATFORM_DEVELOPER_BIN_DIR/libtool" -static "${BUILT_PRODUCTS_DIR}/${EXECUTABLE_PATH}" "${ULIB_OTHER_BUILT_PRODUCTS_DIR}/${EXECUTABLE_PATH}" -o "${ZDK_INSTALL_DIR}/lib${TARGET_NAME}.a"


echo "Copy public headers to install dir for ${PRODUCT_NAME}"

rm -rf "${ZDK_INSTALL_DIR}/includes"
mkdir -p "${ZDK_INSTALL_DIR}/includes"

if [[ -d "${ZDK_BUILD_TMP_DIR}/includes/${PROJECT_NAME}" ]]
then
	cp -r "${ZDK_BUILD_TMP_DIR}/includes/${PROJECT_NAME}" "${ZDK_INSTALL_DIR}/includes"
fi


echo "Copy resources to install dir for ${PRODUCT_NAME}"

mkdir -p "${ZDK_INSTALL_DIR}/resources"

if [[ -d "${ZDK_BUILD_TMP_DIR}/resources/${PROJECT_NAME}" ]]
then
	cp -a "${ZDK_BUILD_TMP_DIR}/resources/${PROJECT_NAME}" "${ZDK_INSTALL_DIR}/resources"
fi

if [[ "$ZDK_TARGET" == "3p" ]]
then
	# do not create frameworks for 3p targets
	exit 0
fi


echo "Create framework for ${PROJECT_NAME}"

# build framework dirs
mkdir -p "${ZDK_INSTALL_DIR}-frameworks/${PROJECT_NAME}.framework"
mkdir -p "${ZDK_INSTALL_DIR}-frameworks/${PROJECT_NAME}.framework/Versions"
mkdir -p "${ZDK_INSTALL_DIR}-frameworks/${PROJECT_NAME}.framework/Versions/A"

# copy framework files
cp "${ZDK_INSTALL_DIR}/lib${TARGET_NAME}.a" "${ZDK_INSTALL_DIR}-frameworks/${PROJECT_NAME}.framework/Versions/A/${PROJECT_NAME}"

if [[ -d "${ZDK_INSTALL_DIR}/includes/${PROJECT_NAME}" ]]
then
	cp -r "${ZDK_INSTALL_DIR}/includes/${PROJECT_NAME}" "${ZDK_INSTALL_DIR}-frameworks/${PROJECT_NAME}.framework/Versions/A/Headers"
else
	mkdir -p "${ZDK_INSTALL_DIR}-frameworks/${PROJECT_NAME}.framework/Versions/A/Headers"
fi

if [[ -d "${ZDK_INSTALL_DIR}/resources/${PROJECT_NAME}" ]]
then
	cp -a "${ZDK_INSTALL_DIR}/resources/${PROJECT_NAME}" "${ZDK_INSTALL_DIR}-frameworks/${PROJECT_NAME}.framework/Versions/A/Resources"
else
	mkdir -p "${ZDK_INSTALL_DIR}-frameworks/${PROJECT_NAME}.framework/Versions/A/Resources"
fi

# create framework symlinks
ln -s A "${ZDK_INSTALL_DIR}-frameworks/${PROJECT_NAME}.framework/Versions/Current"
ln -s Versions/Current/Headers "${ZDK_INSTALL_DIR}-frameworks/${PROJECT_NAME}.framework/Headers"
ln -s Versions/Current/Resources "${ZDK_INSTALL_DIR}-frameworks/${PROJECT_NAME}.framework/Resources"
ln -s "Versions/Current/${PROJECT_NAME}" "${ZDK_INSTALL_DIR}-frameworks/${PROJECT_NAME}.framework/${PROJECT_NAME}"

