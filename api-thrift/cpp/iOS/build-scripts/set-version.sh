#!/bin/bash

#Fail if any commands fail
set -e

#Complain if unbound variables are used
set -u

#Get the version of our ZDK from a shared Android/iOS file
version=`cat ${ZDK_SHARED_VERSION_FILE}`
echo "Setting the ZDK Version to: $version"

#Set the file we'll echo to
ios_version_file="${SRCROOT}/Includes/ZyngaCore/Version.h"

#Define the strings that we'll echo out to the correct file
comment="//Define the current version of our ZDK"
defversion="#define ZDK_VERSION \"$version\""

echo $comment > $ios_version_file
echo $defversion >> $ios_version_file

set +u
