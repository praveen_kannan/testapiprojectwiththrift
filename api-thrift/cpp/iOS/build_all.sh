#! /bin/sh

#######################################################################################
## build_all.sh
## Description: This script is a convenient script to build static library and framework 
## Usage: build_all.sh 
########################################################################################
client=1

check()
{
	res=$1
	logfile=$2
	if [ $res -eq 0 ]
	then
        if true #[ -n "${ZDK_HUDSON_BUILD}" ]
		then
			echo "Status: OK. see $logfile"
		else
	    	echo "Status: OK"
	    	rm -rf $logfile > /dev/null 2>&1
		fi
	else
	    echo "Status: FAILED. see $logfile"
	    exit 1
	fi
}

#Building zdk-core-ios
if [ $client -ne 0 ]
then
	rm -rf build > /dev/null 2>&1

	echo "Building thrift"
	pushd ../../../thrift/cpp/iOS > /dev/null
	./build_all.sh
	popd > /dev/null

	projects=(
		ZyngaAPI-Thrift
	)
	for project in "${projects[@]}"
	do
		logfile=/var/tmp/${project}_build_Debug.txt
		echo "Building $project - Debug"
		xcodebuild -sdk iphoneos -project $project.xcodeproj -configuration Debug clean > /dev/null 2>&1
		xcodebuild -sdk iphoneos -project $project.xcodeproj -target ZyngaAPI-Thrift-iOS -configuration Debug build INSTALL_PROJECT=$project ZDK_BUILD_DIR=`pwd`/build/Debug > /dev/null > $logfile 2>&1
		res=$?
		check $res $logfile

		logfile=/var/tmp/${project}_build_Release.txt
		echo "Building $project - Release"
		xcodebuild -sdk iphoneos -project $project.xcodeproj -configuration Release clean > /dev/null 2>&1
		xcodebuild -sdk iphoneos -project $project.xcodeproj -target ZyngaAPI-Thrift-iOS -configuration Release build INSTALL_PROJECT=$project ZDK_BUILD_DIR=`pwd`/build/Release > /dev/null > $logfile 2>&1
		res=$?
		check $res $logfile
		echo "Done Building $project"
	done
fi