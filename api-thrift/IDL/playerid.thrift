include "dapi.thrift"

struct PlayerIdGetArgs {
	1: optional list<string> networkIds	//list of zids to retrieve playerIds for
	2: optional list<string> appIds	//list of app ids to retrieve playerIds for
	3: optional bool pseudo	//true if use sample data instead
	4: optional list<string> fields	//With default value of empty, otherwise a list of extra fields to return, ex. snUid
}
struct PlayerIdGetNetworkIdsArgs {
	1: optional list<string> playerIds	//[{"appId"=appId1, "playerId"=playerId1}, {"appId"=appId2, "playerId"=playerId2}, ...]
	2: optional bool pseudo	//true if use sample data instead
}
struct PlayerIdAssignArgs {
	1: optional i32 appId	//appId belonging to the game
	2: optional string networkId	//to assign the playerId
	3: optional string playerId	//if none provided, use the preferredSnid to derive a playerId, if none found error out
	4: optional bool pseudo	//true if use sample data instead
}
struct PlayerIdRemoveArgs {
	1: optional i32 appId	//appId belonging to the game
	2: optional string playerId	//that needs to be removed
	3: optional bool pseudo	//true if use sample data instead
}

service PlayerId{
	dapi.DAPIResponse Get(1:PlayerIdGetArgs arguments)
	dapi.DAPIResponse GetNetworkIds(1:PlayerIdGetNetworkIdsArgs arguments)
	dapi.DAPIResponse Assign(1:PlayerIdAssignArgs arguments)
	dapi.DAPIResponse Remove(1:PlayerIdRemoveArgs arguments)

}
