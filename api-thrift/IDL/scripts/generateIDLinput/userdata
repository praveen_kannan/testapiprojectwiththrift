getUserAttributes
Returns the user attributes for the passed id, snid and appid.

DESCRIPTION
This method can return single/multiple or all attributes for one ore more users.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zids	required	array	Array of user ids for which attributes are required.
snid	required	string	The social network id.
appId	required	string	The app id for which neighbors are required.
userAttrs	optional	array	Array of attribute names whose values are required e.g. first_name, score_predicted_payer. if not passed then all attributes are returned.
SAMPLE RESPONSES
       {'20124696461':{'first_name':'Dheeraj','last_name':'Soti'}}
 
getFriendsByApp
Filter the passed user ids based on an inclusive and an exclusive app list.

DESCRIPTION
Given a list of ids, this method will return an array telling which user is playing which app. It allows you to pass list of apps that you want to be included for the search and list that you want to be excluded.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
array	required	zids	Array of user the ids of the users/friends that need to be filtered.
snid	required	string	The social network id.
includedApps	required	string	Comma-separated list of appIds that have to be included, pass -2 if you want to check any app.
excludedApps	optional	string	Comma-separated list of appIds that should be excluded from the check.
SAMPLE RESPONSES
       {'75':['500091571','726328031'],'115':['500091571','726328031']}       
 
getPredictedPayerProperties
Returns percentile ranks for the passed user for all apps based on his predicted payer score.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
snid	required	string	The social network id.
SAMPLE RESPONSES
       [
       {'snid':'1','zid':'20124696461','appId':8,'ip_country':'US','score_predicted_payer':'','score_predicted_payer_rank':0},
       {'snid':'1','zid':'20124696461','appId':43,'ip_country':'US','score_predicted_payer':'','score_predicted_payer_rank':0}
       ]      
 
getASN
Returns an array of ASNs (Active Social Network) for the passed week(s).

DESCRIPTION
A friend is considered to be ASN if there is a complete loop of exchange i.e., A sends something to B, B accepts it and sends something back to A.

This method can return ASNs (Active Social Network) for: -most recent week -one week prior to recent week - last 14 days -two weeks prior to recent week - last 21 days -three weekes prior to recent week - last 28 days

The returned list is filled ordered by their asn-activity descending. An id can only exist in one of the weeks (i.e. if a friend exists in most recent week then he won't exist in prior weeks).

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user if the authentication type is application, otherwise it is derived from request
snid	required	string	The social network id if the authentication type is application, otherwise it is derived from request
appId	required	string	The app id if the authentication type is application, otherwise it is derived from request
SAMPLE RESPONSES
       {
       '1':['121121212', '676276376726'],
       '2':['223232323', '265655656'],
       '3':[12881001],
       '4':['5565653434']
       }
 
getASNInfo
Returns ASN (Active Social Network) info for the passed user (e.g.

DESCRIPTION
activity, size of network for most recent week).

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user if the authentication type is application, otherwise it is derived from request
snid	required	string	The social network id if the authentication type is application, otherwise it is derived from request
appId	required	string	The app id if the authentication type is application, otherwise it is derived from request
SAMPLE RESPONSES
       {'activity':7,'size':20}
 
getLikelyToShareFriends
Returns an array of friends with whom the user is most likely to share with alongwith the likelihood score.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user if the authentication type is application, otherwise it is derived from request
snid	required	string	The social network id if the authentication type is application, otherwise it is derived from request
appId	required	string	The app id if the authentication type is application, otherwise it is derived from request
SAMPLE RESPONSES
       {'201010101':0.444444,'2201233032':0.333333}
 
getTopHelpers
Returns an array of 3 friends who are the top helpers for the passed users based on the message click count.

DESCRIPTION
This data is aggregated across the whole network.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user if the authentication type is application, otherwise it is derived from request
snid	required	string	The social network id if the authentication type is application, otherwise it is derived from request
SAMPLE RESPONSES
       {'201010101':20,'2201233032':15,'2201233033':10}
 
getRecommendedAppsWeb
Returns an array of 10 recommended web apps for this user.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user if the authentication type is application, otherwise it is derived from request
snid	required	string	The social network id if the authentication type is application, otherwise it is derived from request
appId	required	string	The app id if the authentication type is application, otherwise it is derived from request
SAMPLE RESPONSES
       [[118,"0.1944"],[240,"0.1282"],[223,"0.1203"],[63,"0.101"],[114,"0.095"],[115,"0.0715"],[5000044,"0.0687"],[75,"0.0595"],[91,"0.055"],[90,"0.0497"]]
 
getRecommendedAppsMobile
Returns an array of 5 recommended mobile apps for this user.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user if the authentication type is application, otherwise it is derived from request
snid	required	string	The social network id if the authentication type is application, otherwise it is derived from request
appId	required	string	The app id if the authentication type is application, otherwise it is derived from request
SAMPLE RESPONSES
       [[91,"0.3685"],[109,"0.2199"],[116,"0.1119"],[107,"0.1036"],[5000014,"0.0465"]]
 
getOutOfNetworkFriends
Returns an array of friends who are playing either similar genre nonZynga FB games or nonZynga FB games in general.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user if the authentication type is application, otherwise it is derived from request
snid	required	string	The social network id if the authentication type is application, otherwise it is derived from request
appId	required	string	The app id if the authentication type is application, otherwise it is derived from request
SAMPLE RESPONSES
               ["31005215726","31823522192","20090601509","20252867666","31065856197"]
 
getNonASN
Returns an array of top non-ASN (max.

DESCRIPTION
30) for a given user based on non-clickers and non-senders.

A friend is considered to be non-ASN if there is a incomplete loop of exchange (i.e., A sends something to B, B accepts it but does not send something back to A or did not clicked on received item).

This method can return Non-ASNs for: -most recent week - last 7 days -one week prior to recent week - last 14 days -two weeks prior to recent week - last 21 days -three weekes prior to recent week - last 28 days

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user if the authentication type is application, otherwise it is derived from request
snid	required	string	The social network id if the authentication type is application, otherwise it is derived from request
appId	required	string	The app id if the authentication type is application, otherwise it is derived from request
SAMPLE RESPONSES
       {
       '1':{'non_clickers':['13232232'],'non_senders':['23232']},
       '2':{'non_clickers':['20168314972'],'non_senders':[]},
       '3':{'non_clickers':['20038565938','20306191406'],'non_senders':['20061437162']},
       '4':{'non_clickers':[],'non_senders':['20151068943']}
       }
 
getOptimalGiftRecipients
Returns array of user ids which is the optimal list for gift recipients.

DESCRIPTION
The returned list is filled with:
ASN, ranked order
non-ASN Senders, ranked order
non-ASN Clickers, ranked order
Prior ASN, ranked order
Prior Non-ASN Senders, ranked order
Prior Non-ASN Clickers, ranked order
friends ranked by engagement_15days field (weed out engagement = 0 first)
If the list results in 0 people, then fill to Max=10 with random 10 in-app friends
ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
zids	required	array	String array of the ids of the friends from which to find the recipient list.
snid	required	string	The social network id.
appId	required	string	The app id.
experimentName	optional	string	Argument with empty default value, can be used to specify the experiment name, if any.
bucketSize	optional	int	Argument with default value of 40, specifies maximum number of entries in output.
Version	optional	int	Version of the list of gift recipients (default value is 3).
SAMPLE RESPONSES
      ['20061437162','20029669483','20306191406','20237063750','20155502943','20193461955','20095018415','20350253764','31068097265','20108766465','20151068943','20168314972','20070890226','20247849891']   
 
getOptimalInviteRecipients
Returns array of user ids which is the optimal list for invite recipients.

DESCRIPTION
This is same as getOptimalGiftRecipList(), except for the fact that it weed out any users that are already neighbors.

The returned list is filled with:
ASN, ranked order
non-ASN Senders , ranked order
non-ASN Clickers, ranked order
Prior ASN, ranked order
Prior Non-ASN Senders , ranked order
Prior Non-ASN Clickers, ranked order
friends ranked by engagement_15days field (weed out engagement = 0 first). If the list results in 0 people, then fill to Max=10 with
random 10 in-app friends.
ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
friends	required	array	The ids of the friends from which to find the recipient list.
neighbors	required	array	The ids of neighbors.
snid	required	string	The social network id.
appId	required	string	The app id.
experimentName	optional	string	Argument with empty default value.
type	optional	string	Argument for rank type with default value of SGASN_INVITE_RETENTION. SGASN_INVITE_NEW_INSTALLS: neighbor suggestions of ASN,non-ASN senders, non-asn clickers, priors out of app who have installed this app since you last played (or same day) (lastplayed date). For users whose installdate < 2days, suggestion expaned to friends who installed same app < 7days ago. SGASN_INVITE_LAPSED:neighbor suggestions ASN,non-ASN senders, non-asn clickers, priors in app but not neighbors who have not played in the last 15 days. SGASN_INVITE_NEW_TO_NETWORK:invite suggestions fo users NOT playing any zynga app yet SGASN_INVITE_RETENTION:same logic as get_optimal_gift except we weed out neighbors. SGASN_INVITE_OUTOFGAME_ASN: same as SGASN_INVITE_NEW_INSTALLS except limit to friends who haven't installed the app SGASN_INVITE_OUTOFGAME_ASN_LAPSED: same as SGASN_INVITE_OUTOFGAME_ASN, but limit to friends whose last play date of any other apps is over 15 days
otherApps	optional	array	Argument with default value of empty array. This is an array of other app Ids, which is used when type is one of SGASN_INVITE_NEW_INSTALLS, SGASN_INVITE_OUTOFGAME_ASN, or SGASN_INVITE_OUTOFGAME_ASN_LAPSED
bucketSizeAargument	optional	int	Size of the argument (default value is 60).
version	optional	int	Argument with default value of 3.
SAMPLE RESPONSES
      ['20061437162','20029669483','20306191406','20237063750','20155502943','20193461955','20095018415','20350253764','31068097265','20108766465','20151068943','20168314972','20070890226','20247849891']   
 
getASNOfFriends
Returns ASN of your friends who are not in your ASN.

DESCRIPTION
Returns a ranked list.

For Zlive, set allow_non_friend = true which will find ASN of your friends who are not your friends. Returns a ranked list. The ranked list will be filled by 7day asn, followed by 14,21, 28 day asn, controlled by wdDays. The asns of same period are ranked by their asn_value with user's friends, if two friend's asn friends have same asn_value, when !empty(rankByEngagementSize), use their 15 day engagement score to break even, the higher wins.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
snid	required	string	The social network id.
appId	required	string	The app id.
friends	required	array	Array of sn friends zids supplied by app.
allFriends	optional	array	Default value of nmtry array. These are all user's sn friends. If the app doesn't supply this list, we don't perform a check whether your friend's friends are your sn friends.
allowNonFriend	optional	boolean	Argument with default value of false. If set to true, then it'll filter out the friend's friend who are your friends.
wdDays	optional	string	Defaults to 'all'. It controls up to what time period asn data to use, allowed values are 7, 14, 21 and 28.
rankByEngagementTypes	optional	string	Defaults to 'all'. If it is 0, it won't use engagement score to break even asn values. 'all' means always and int just to ensure the top N list are ranked using engagement score to break even asn values.
includeTypes	optional	string	argument with default value array with one element DS_TYPE_ASN. This is type of app friends (DS_TYPE_ASN DS_TYPE_NONASN_CLICKER DS_TYPE_NONASN_SENDER) to include in the return list, the order of type is important, it decides how the final list is ranked
excludeTypes	optional	array	Default value of array with one element DS_TYPE_ASN. Specifies which types don't include friends' friends of these types.
bucketSize	optional	int	Default value of 60.
SAMPLE RESPONSES
       ['31632270544','20014217744','20030234598','20218286993','31658564708',
      '31176516042','20192703622','20156591539','20326272892','20226219763','20193461955',
      '20189581161','20067791892','20247849891','20237063750','20009102396']
 
getLevel
Returns the cross promo level of the user.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
snid	required	string	The social network id.
appId	required	string	The app id.
SAMPLE RESPONSES
       ['20'] 
 
setLevel
Sets the cross promo level of the user.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
snid	required	string	The social network id.
appId	required	string	The app id.
level	required	string	The level to be set.
SAMPLE RESPONSES
       [true]
 
ERROR RESPONSES
TYPE	DESCRIPTION	DATA
DAPIServerPluginException		None.
getVisit
Returns an associative array with client id and version as the keys and visit datestamp and visit bitmap as values.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
snid	required	string	The social network id.
appId	required	string	The app id.
SAMPLE RESPONSES
      {
      '1':{'-1':{'datestamp':'2012-03-23T14:22:19-07:00','bitmap':2014330783},
      '0':{'datestamp':'2012-03-23T14:22:19-07:00','bitmap':2014330783}},
      '-2':{'-1':{'datestamp':'2012-03-23T14:22:19-07:00','bitmap':2014330783}}
      }
 
ERROR RESPONSES
TYPE	DESCRIPTION	DATA
DAPIServerGenericException		None.
getSmartSystemsBucketNumber
Returns a smart system bucket number based on the parameters such as numActiveFriends, numNeighbors.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
snid	required	string	The social network id.
appId	required	string	The app id.
key1	required	string	The threshold type: feed/request
numActiveFriends	required	int	Number of active friends for current (zid,snid,appId)
numNeighbors	required	int	Number of neighbors for current (zid,snid,appId)
key2,	optional	string	key3, key4, key5, featureIdentifier, gameData
SAMPLE RESPONSES
      ['5'] 
 
getPlayCount
Get the number of completed loops between an user and list of opponents Returns an associative array with opponent id as key and number of completed loops as value.

DESCRIPTION
The array is reverse sorted based on the number of completed loops, so opponent with the most number of completed loops will be the first entry in the associative array.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
snid	required	string	the social network id
opponentZids	optional	array	List of zids of opponents, if not passed then it would return count of plays for all the opponents
appIds	optional	string	The application ids.
minLoopSize	optional	integer	minimum number of completed loops between user and opponent.
numDays	optional	integer	Number of days to look back into history of game sessions
SAMPLE RESPONSES
               {
               '100': 7,
               '200': 10
               }
 
getGamePlayHistory
Get the game play history (win/loss/draw) for a user.

DESCRIPTION
Returns an associative array with WINS or LOSSES as key and the number of wins or losses as value

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
snid	required	string	the social network id
opponentZids	optional	array	Array of opponent zids against which the history is required. If not passed history would be aggregated across all opponents
appIds	optional	array	Array of app ids for which history is required, if not passed would return history for every app.
numPlays	optional	integer	Number of play session to look back into history
numDays	optional	integer	Number of days to look back into history of game sessions
SAMPLE RESPONSES
              {
                      "10":{"100":{"NUM_WINS":1,"NUM_LOSSES":0,"NUM_DRAWS":1,"NUM_DAYS":30}},
                      "20":{"100":{"NUM_WINS":1,"NUM_LOSSES":1,"NUM_DRAWS":0,"NUM_DAYS":30},"300":{"NUM_WINS":1,"NUM_LOSSES":0,"NUM_DRAWS":0,"NUM_DAYS":30}},
                      "30":{"100":{"NUM_WINS":1,"NUM_LOSSES":0,"NUM_DRAWS":0,"NUM_DAYS":30}}
              }
 
setGamePlayResult
Set the number of wins and number of losses between user and opponent Returns true or false if set was successful/unsucessful.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The zid of the user.
snid	required	string	The social network id.
appId	required	string	The application id.
opponentZid	required	string	opponent zid.
outcome	required	string	The game outcome won=1, lost=3, draw=3
timeOfCompletion	optional	string	time the game is completed, defaults to current time.
status	optional	string	status of the game, if any, defaults to blank
userScore	optional	integer	score of the user
opponentScore	optional	integer	score of the opponent
SAMPLE RESPONSES
      [True]
 
setVisit
Sets the datestamp and version for one single visit.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
snid	required	string	The social network id.
appId	required	string	The app id.
version	optional	int	Argument with default value of 0.
clientid	optional	int	Argument with default value of 1.
SAMPLE RESPONSES
       [true]
 
ERROR RESPONSES
TYPE	DESCRIPTION	DATA
DAPIServerPluginException		None.
getInstall
Returns an associative array with client id and version as keys and install datestamp as value.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
snid	required	string	The social network id.
appId	required	string	The app id .
SAMPLE RESPONSES
       {
       '-2':{'-1':'2012-03-23T14:22:19-07:00'},
       '1':{'-1':'2012-03-23T14:22:19-07:00','0':'2012-03-23T14:22:19-07:00','20':'2012-03-10T21:13:39-08:00'}
       }
 
ERROR RESPONSES
TYPE	DESCRIPTION	DATA
DAPIServerGenericException		None.
setInstall
Sets datestamp for install.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
snid	required	string	The social network id.
appId	required	string	The app id.
version	optional	int	Argument with default value of 0.
clientid	optional	int	Argument with default value of 1.
SAMPLE RESPONSES
       [true] 
 
ERROR RESPONSES
TYPE	DESCRIPTION	DATA
DAPIServerPluginException		None.
getAchievement
Returns an associative array with achievement id as the key and count as the vlaue.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
snid	required	string	The social network id.
appId	required	string	The app id.
SAMPLE RESPONSES
       {'97':2}
 
setAchievement
Sets the cross promo achievement.

DESCRIPTION
The $achievement name has to be pre-registered with Achievements manager, which is a self-service web tool to manage registered app achievements.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
snid	required	string	The social network id.
appId	required	string	The app id.
achievement	required	string	Pre-registered achievement name with Achievements Manager.
count	required	int	The value to be set for achievement.
SAMPLE RESPONSES
       [true]
 
ERROR RESPONSES
TYPE	DESCRIPTION	DATA
DAPIServerPluginException		None.
updateAchievement
Updates the achievement counter using delta value.

DESCRIPTION
The achievement name has to be pre-registered with Achievements manager, which is a self-service web tool to manage registered app achievements. Please make sure delta is a positive number. This function will return false for non positive $delta value (e.g. $delta equals to 0). You can always use '$add = false' to represent delete operation.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
snid	required	string	The social network id.
appId	required	string	The app id.
achievement	required	string	Pre-registered achievement name with Achievements Manager.
delta	required	int	The delta value for achievement.
add	optional	boolean	Argument with default value true, set it to false if you want to use this as delete.
SAMPLE RESPONSES
       [true]
 
ERROR RESPONSES
TYPE	DESCRIPTION	DATA
DAPIServerPluginException		None.
getPredictedPayerPercentile
Returns the percentile rank for the passed app and predicted payer score.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
appId	required	string	The app id for which predicted payer percentile is required.
predictedPayerScore	required	float	Score which predicts probability of a payer likely to pay. This is available as a user attribute.
SAMPLE RESPONSES
       ['0.9']
 
getAppObject
Returns the app object stored for the user.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
snid	required	string	The social network id.
appId	required	string	The app id for which the object is required.
clientId	optional	string	Argument with default value of 1.
SAMPLE RESPONSES
       ['dheeraj','soti']
 
setAppObject
Stores the app object stored the user.

DESCRIPTION
This method can be used to store any user level information.

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user
snid	required	string	The social network id.
appId	required	string	The app id for which the object has to be stored.
appObjectData	required	array	Array array with app object data that need to be stored.
clientId	optional	string	Argument with default value of 1.
SAMPLE RESPONSES
       [true]
 
getHashForZid
Given a zid and vendor returns encrypted hash of the zid.

DESCRIPTION
This one way hash is the recommended way of sharing the zids with external vendors. To do reverse lookup of zid for a hash use the method "getZidForHash"

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
zid	required	string	The id of the user.
vendor	required	string	The vendor for which the hash has to be created. Only first 45 characters are used.
SAMPLE RESPONSES
       ['dedca6584749b2c1f31f9b9418c5a9415777d57c93e8e81a977120a5c3d97a47'] 
 
getZidForHash
Given a hash returns the zid.

DESCRIPTION
The passed hash should have been created by getHashForZid and this call is a reverse lookup

ARGUMENTS
NAME	OPTIONAL	TYPE	DESCRIPTION
hash	required	string	The hash that needs to be converted to a zid.
SAMPLE RESPONSES
       ['2010299910'] 
 