getList
Retrieves a ranked list of zids and scores from the leaderboard service.

Description
Filter - Param Map:

   +-----------------------+-------------------------+-------------------------------------------+
   |        FILTER         |         REQUIRED        |         OPTIONAL                          |
   +-----------------------+-------------------------+-------------------------------------------+
   | all                   | name                   | appId, count                            |
   |                       |                         |                                           |
   | 'top'                 | --                      | offset, oldestPeriodIndex,              |
             |                       |                         | newestPeriodIndex                        |
   | 'surrounding'         | zid                    | periodIndex                        |
   | 'friends'             | zids                   | oldestPeriodIndex, newestPeriodIndex    |
   +-----------------------+-------------------------+-------------------------------------------+

Arguments
Name 	Optional 	Type 	Description
appId 	optional 	int 	The app id of the application using the leaderboard service; this may or may not be the the canonical game id; games may also use zlive ids.
name 	required 	string 	The name of the leaderboard being accessed. This is an arbitrary string that must only be unique on a per-game basis.
filter 	required 	string 	The filter used to retrieve the list. Default is 'top'; acceptable options are:

    'top' -> topN global users, optionally starting at an offset.
    'surrounding' -> count users above and below the user specified by zid (required).
    'friends' -> provide scores for a list of zids. This is intended to be the social case, and does not return global rank. The common use case is for a user's social graph within a particular game, but the zids parameter (required) may have arbitrary zids. 

zid 	optional 	int 	The zid to center on. Required only for the 'surrounding' filter. Unused otherwise. Optionally, can be assigned a playerId in order to override the request's userId. Mapping for playerId must be done before the call is made
zids 	optional 	array 	List of zids to retrieve. Required only for the 'friends' filter. Unused otherwise. Optionally, can be assigned a list of playerIds if the leaderboard uses playerId as key Mapping for playerIds must be done before the call is made
count 	optional 	int 	The maximum number of results to return. Optional. Default: 20.
periodIndex 	optional 	int 	Represents interval for which the surrounding query is run (one-based index). Optional. Only used for the 'surrounding' filter for periodic leaderboard. Default: periodCount associated with the leaderboard, Min: 1, Max: periodCount.
oldestPeriodIndex 	optional 	int 	value for start of interval range (oldest timestamp) for which to retrieve the values. One-based index. Optional. Default: 1, Min: 1, Max: periodCount associated with the leaderboard.
newestPeriodIndex 	optional 	int 	value for end of interval range (newest timestamp) for which to retrieve the values. One-based index. Optional. Default: periodCount associated with the leaderboard, Min: 1, Max: periodCount.
Sample Responses

 For 'top' or 'surrounding':
 [
   {
     "value": 900100,
     "rank": 1,
     "zid": 31759631436,
   },
   {
     "value": 644088,
     "rank": 2,
     "zid": 20153739756
   },
   {
     "value": 641646,
     "rank": 3,
     "zid": 20056529876
   },
   {
     "value": 640426,
     "rank": 4,
     "zid": 20130052550
   }
 ]

 For 'friends':
 [
   {
     "value": 900100,
     "ts": 1337263488,
     "zid": 31759631436,
     <extra-key1>: <extra-value1>,
     <extra-key2>: <extra-value2>
   },
   {
     "value": 644088,
     "ts": 1337436062,
     "zid": 20153739756
   },
   {
     "value": 635032,
     "ts": 1337490938,
     "zid": 20335619140
   }
 ]
 

setScore
Sets a new score for user and leaderboard.

Arguments
Name 	Optional 	Type 	Description
appId 	optional 	int 	Application Id.
zid 	optional 	int 	User Id. Optionally, can be assigned a playerId if the leaderboard uses playerId as key Mapping for playerId must be done before the call is made
name 	required 	string 	The name of the leaderboard being accessed. This is an arbitrary string that must only be unique on a per-game basis.
value 	required 	int 	The new value to set.
extra 	optional 	array 	Extra metadata to associate with this score.
filter 	optional 	array 	Filters to apply for conditional set. Options: 'ifMax', 'ifMin', and 'none'. Defaults to 'ifMax' if the metric's configured sort is 'descending' which means that if the new value is larger, it will replace the current value. Similarly this defaults to 'ifMin' if the sort is 'ascending'. To ignore the configred sort and always set overwrite the current value with the new one, set 'filter' = 'none'
Sample Responses

 [ true ]
 

create
Creates and configures a leaderboard.

Description
Once created, leaderboard configuration is immutable.

There are two primary types of leaderboard: permenant and periodic. A permenant leaderboard stores an all-time best score for each user. A periodic leaderboard stores scores for a subset of the recent past, such as "this week's top scores". By default, leaderboards are permenant.

Periodic leaderboards can be created by setting the periodUnits parameter to a non-default value. For example, a weekly leaderboard can be created by setting (periodUnits => 'weeks'). With this parameter set,the service divides all time into one week intervals. The beginning of time for this purpose is Sunday, January 1st, 2012 PST. Every Saturday, at 11:59pm PST, the current period is discarded, and a fresh leaderboard is started.

Valid values for periodUnits are 'days', 'weeks' and 'months'. To specify a period longer than one day, week or month, pass in the period parameter. (period => 3, periodUnits => 'months') would create a leaderboard that resets once every three months.

A periodic leaderboard can store more than one period worth of data, when the periodCount parameter is set. Passing (periodUnits => 'days', periodCount => 7) wll create a leaderboard that tracks the best daily score for the last seven days.

Arguments
Name 	Optional 	Type 	Description
name 	required 	string 	The name of the leaderboard being created. This is an arbitrary string that must be unique on a per-game basis. This name is used as a key to identify the leaderboard for all other API calls.
sort 	optional 	string 	Sort order for the leaderboard: 'ascending' or 'descending'. Default: 'descending'. 'Descending' means higher scores are better than lower scores, and 'ascending' means the opposite.
periodUnits 	optional 	string 	Periodic leaderboards may specify 'days', 'weeks' or 'months' to specify the length of their period intervals. The default value ('infinite') creates a permenant leaderboard.
period 	optional 	int 	The length of a period interval is period * periodUnits. Default: 1
periodCount 	optional 	int 	Queries on a periodic leaderboard will pull in periodCount intervals.
periodStart 	optional 	int 	Starting timestamp for the first interval. Default: 1325404800 (Sun, 01 Jan 2012 08:00:00 GMT)
serverOnlyWrites 	optional 	bool 	Configure the leaderboard to allow writes only from servers and disallow user sessions.
Sample Responses

 {
   "sort": "descending",
   "periodUnits": "infinite"
   "period": 1,
   "periodCount": 1,
   "periodStart": 1325404800
 }
 

delete
Deletes the configuration and all scores associated with a leaderboard.

Arguments
Name 	Optional 	Type 	Description
name 	required 	string 	The name of the leaderboard being accessed.
Sample Responses

 [ true ]
 

getConfig
Reads the configuration for a leaderboard.

Arguments
Name 	Optional 	Type 	Description
name 	optional 	string 	The name of the leaderboard being accessed.
Sample Responses

 {
   "<leaderboard-name>": {
     "sort": "descending"
   }
 }
 


