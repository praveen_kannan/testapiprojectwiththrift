#! /bin/sh

#######################################################################################
## build_all.sh
## Description: This script will copy generated source files into the include and src
## folders. Assumes you have the STG version of thrift installed. See:
## https://zyntranet.apps.corp.zynga.com/display/Mobile/Thift+Installation+Guide
########################################################################################
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
IDL_DIR=$SCRIPT_DIR/../
OUTPUT_DIR=$SCRIPT_DIR/../../

. configs/thriftComponents.config
pushd .
cd $SCRIPT_DIR

for component in "${thriftComponents[@]}"
do
    printf " \e[0;32m...Generating source for $component ... \e[0m\n"
    thrift --gen cpp_dapi $IDL_DIR/$component.thrift
    printf " \e[0;32m...Done... \e[0m\n"
done


# Copy generated code to the ZyngaAPI-Thrift destination gen-cpp location


rm -rf $OUTPUT_DIR/cpp/ZyngaAPI-Thrift/gen-cpp
#mkdir -p $OUTPUT_DIR/cpp/ZyngaAPI-Thrift/gen-cpp

pax -wr gen-cpp $OUTPUT_DIR/cpp/ZyngaAPI-Thrift/

rm -rf $SCRIPT_DIR/gen-cpp

popd
