# 1) Copy the api from the developers-internal webpage after the Methods header to a file
# 	 called 'fullinput' that lives next to this script.
#    For example, for https://developers-internal.zynga.com/docs/api/Track 
#	 begin copying from logCount... to the end of the page.
#
# 2) Run: Python testScript.py -m <moduleName> -i <inputfile(path)>  > outputFileName
#
# 3) Check if there were some things that could not be parsed, they will be surrounded by ### ### 
#    tags and you should fix this script or manually fix them
#
# 4) Double check the usage of any list<string> member variables that were created in the argument structs
#    These are not safe because the parsing just blindly assumes arrays are of type string
#	 Cannot easily extract array type from the written APIs

import sys, getopt;
outputWarnings = '';
moduleName = '';
inputFile = '';

# Parse a line to get the structure name
def GetStructureName(line):
	global moduleName;
	baseStr = line.rstrip('\n') + 'Args';
	upperStr = baseStr[0].upper() + baseStr[1:];
	upperStr = moduleName + upperStr;
	return upperStr;

def GetMethodName(line):
	baseStr = line.rstrip('\n');
	baseStr = baseStr[0].upper() + baseStr[1:];
	return baseStr;

# Convert the input var type to a thrift IDL type
def GetVarTypeText(varType, context):
	varTypeText = '';
	if varType == 'string':
		varTypeText = 'string ';
	elif varType == 'int':
		varTypeText = 'i32 ';
	elif varType == 'integer':
		varTypeText = 'i32 ';
	elif varType == 'timestamp':
		varTypeText = 'i64 ';
	elif varType == 'float':
		varTypeText = 'double ';
	elif varType == 'boolean' or varType =='bool':
		varTypeText = 'bool ';
	elif varType == 'array' or varType == 'string|array' or varType == 'string|array(strings)':
		# This is usually correct, but needs better checking for type
		# Handling complex types like structs will probably be better off done by hand
		varTypeText = 'list<string> ';
	elif varType == 'object':
		varTypeText = 'string ';
		global outputWarnings;
		outputWarnings += (context+ 'found varType object -- assuming string\n');
	else:
		varTypeText = '###('+varType+')### ';
	return varTypeText;

#BEGIN
def main(argv):
	global outputWarnings;
	global moduleName;
	global inputFile;

	try:
		opts, args = getopt.getopt(argv,"hm:i:",["moduleName=", "inputFile="]);
	except getopt.GetoptError:
		print 'generateIDL.py -m <module name> -i <input file>';
		sys.exit(2);
	for opt, arg in opts:
		if opt == '-m':
			moduleName = arg.lstrip();
			#print 'Module name is: ', moduleName;
		if opt == '-i':
			inputFile = arg.lstrip();

	outputStructs = '';
	outputMethods = 'service ' + moduleName + '{\n'
	f = open(inputFile)
	lines = f.readlines()

	i = 0;
	nState = 0;
	lineNum = 0;

	structName = '';
	methodName = '';
	returnType = '###(';
	bReturnTypeSet = 0;

	for i, curLine in enumerate(lines):
		# 0: Looking for the next method line
		if nState == 0:
			# Added a condition that there are no spaces in the line to bypass "ERROR MESSAGES" sections
			if curLine.count('\t') == 0 and len(curLine) > 3 and curLine.count(' ') == 0 and curLine.find('None.') and curLine[0] != '#':
				structName = GetStructureName(curLine);
				methodName = GetMethodName(curLine);

				# if started:
				# 	outputStructs += '}\n';
				
				outputStructs += 'struct ' + GetStructureName(curLine) + ' {\n';
				lineNum = 1;
				started = 1;
				
				nState = 1;



		# 1: Looking for SAMPLE RESPONSES to eat and parse for type
		elif nState == 1:
			if curLine.find('SAMPLE RESPONSES') != -1 or curLine.find('Sample Responses') != -1:
				outputStructs += '}\n';
				nState = 2;
			# Looking for fields to parse into IDL struct member variables
			elif curLine.count('\t') >= 3:
				varName, required, varType, comment = curLine.split("\t");
				varName, required, varType, comment = varName.strip(), required.strip(), varType.strip(), comment.strip();

				# Skip the header section that looks like this:
				# NAME	OPTIONAL	TYPE	DESCRIPTION
				if varName != 'NAME' and varName != 'Name':
					# REQUIRED TEXT
					requiredText = '';
					if required == 'required':
						requiredText = '';
					elif required == 'optional':
						requiredText = 'optional ';
					else:
						requiredText = '###('+required+')###';

					# TYPE TEXT
					contextText = 'In method name: '+ methodName+ ', var name: '+ varName;
					varTypeText = GetVarTypeText(varType, contextText);

					# VARIABLE NAME TEXT
					# ANY FIXUP APPLIED HERE NEEDS TO BE UN-FIXED AT RUNTIME IN TDAPIPROTOCOL.CPP

					# Fixup fields named 'class' to 'class_'
					if varName == 'class':
						varName = 'class_'; # sorry class is a reserved keyword in thrift! Will need to fixup at runtime

					# Fixup fields containing '.' characters to '__dot_'
					if varName.count('.') >= 1:
						varName = varName.replace(".", "_dot_");

					# SPECIAL CASES to omit variables:
					# This is to help end users out so they don't need to constantly feed these variables	
					# For track we omit clientId and zid and these should be manually inserted at runtime (tdapiprotocol.cpp)
#					if moduleName == 'Track' and (varName == 'clientId' or varName == 'zid'):
#						pass; #no-op
#					# For UserData omit zid and manually insert at runtime.
#					if moduleName == 'UserData' and varName == 'zid':
#						pass; #no-op
#					if moduleName == 'Conversation' and varName == 'zid':
#						pass; #no-op
#					if moduleName == 'Experiment' and varName == 'clientId':
#						pass; #no-op
					if (varName == 'zid' or varName == 'clientId'):
						pass; #no-op
					else:
						lineText = '\t'+ str(lineNum) +': '+requiredText +varTypeText+varName+'\t//'+comment;
						outputStructs += lineText.rstrip('\n') + '\n';
						lineNum += 1;

		# 2: Try to figure out the return type
		elif nState == 2:
			if bReturnTypeSet == 0:
				returnType = returnType + curLine;

			# TRY TO DETERMINE THE RETURN TYPE
			if curLine.find('true/false') != -1:
				returnType = 'bool';
				bReturnTypeSet = 1;

			elif curLine.find('["') != -1: #could be multiline, so only checking the first char
				returnType = 'string';
				bReturnTypeSet = 1;

			elif curLine.find('[') != -1: #could be multiline, so only checking the first char
				returnType = 'i32'
				bReturnTypeSet = 1;

			nState = 3;
		#3: Find the end of the SAMPLE RESPONSES and add to the methods
		elif nState == 3:
			if curLine.isspace() == 1:
				if bReturnTypeSet == 0:
					returnType = returnType + ')###';
				nState = 0;
	# 			We are assuming always return DapiResponse for now!
				outputMethods += '\t' + 'dapi.DAPIResponse ' + methodName + '(1:' + structName + ' arguments)\n';

	if outputWarnings != '':
		print 'WARNINGS ---'
		print outputWarnings + 'WARNINGS ---\n';
	print 'include \"dapi.thrift\"\n\n'
	print outputStructs;
	print outputMethods;
	print '}';
	f.close();

if __name__ == "__main__":
	main(sys.argv[1:])