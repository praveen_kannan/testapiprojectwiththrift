include "dapi.thrift"

struct ExperimentGetArgs {
	1: string experimentName	//The experiment name.
	2: optional string experimentVer	//The version of the experiment.
	3: optional string overrideAppId	//An appId to override the implicit one.
	4: optional string overrideSnid	//A social network id to override the implicit one.
	5: optional i32 overrideLookupAppId	//An override appId for lookup an experimention definition in MC
	6: optional i32 partitionId	//An user ID or server ID which is used to partition experiment users into different variants. If not provided, then userId in $request object will be used as $partitionId.
}
struct ExperimentGetMultiArgs {
	1: list<map<string, string>> experiments	//The experiment array with experiment names and other experiment attributs. [ ['experimentName'=>"", 'experimentVersion'=>], ['experimentName'=>"", 'experimentVersion'=>]...]
	2: optional string overrideAppId	//An appId to override the implicit one.
	3: optional string overrideSnid	//A social network id to override the implicit one.
	4: optional i32 overrideLookupAppId	//An override appId for lookup an experimention definition in MC
	5: optional i32 partitionId	//An user ID or server ID which is used to partition experiment users into different variants. If not provided, then userId in $request object will be used as $partitionId.
}
struct ExperimentLogAndGetArgs {
	1: string experimentName	//The experiment name.
	2: optional string experimentVer	//The version of the experiment.
	3: optional string overrideAppId	//An appId to override the implicit one.
	4: optional string overrideSnid	//A social network id to override the implicit one.
	5: optional i32 overrideLookupAppId	//An override appId for lookup an experimention definition in MC
	6: optional i32 partitionId	//An user ID or server ID which is used to partition experiment users into different variants. If not provided, then userId in $request object will be used as $partitionId.
}
struct ExperimentExistsArgs {
	1: string experimentName	//The experiment name.
	2: optional string overrideAppId	//An appId to override the implicit one.
	3: optional string overrideSnid	//A social network id to override the implicit one.
	4: optional i32 overrideLookupAppId	//An override appId for lookup an experimention definition in MC
}
struct ExperimentLogGoalArgs {
	1: string experimentName	//The experiment name.
	2: string goal	//The goal of the experiment.
	3: optional string The //The value of the goal.
	4: optional string overrideAppId	//An appId to override the implicit one.
	5: optional string overrideSnid	//A social network id to override the implicit one.
}
struct ExperimentGetMetadataArgs {
	1: string experimentName	//The experiment name.
	2: optional string overrideAppId	//An appId to override the implicit one.
	3: optional string overrideSnid	//A social network id to override the implicit one.
	4: optional i32 overrideLookupAppId	//An override appId for lookup an experimention definition in MC
}
struct ExperimentGetDateArgs {
	1: string experimentName	//The experiment name.
	2: string type	//Optionas are: "TARGET","START","LAST_CHANGE"
	3: optional string overrideAppId	//An appId to override the implicit one.
	4: optional string overrideSnid	//A social network id to override the implicit one.
	5: optional i32 overrideLookupAppId	//An override appId for lookup an experimention definition in MC
}
struct ExperimentLogAndGetMultiWithVariantNameArgs {
	1: list<map<string, string>> experiments	//The experiment array with experiment names 
	2: optional string overrideAppId	//An appId to override the implicit one.
	3: optional string overrideSnid	//A social network id to override the implicit one.
	4: optional i32 overrideLookupAppId	//An override appId for lookup an experimention definition in MC
	5: optional i32 partitionId	//An user ID or server ID which is used to partition experiment users into different variants. If not provided, then userId in $request object will be used as $partitionId.
}
struct ExperimentGetMultiWithVariantNameArgs {
	1: list<map<string, string>> experiments	//The experiment array with experiment names 
	2: optional string overrideAppId	//An appId to override the implicit one.
	3: optional string overrideSnid	//A social network id to override the implicit one.
	4: optional i32 overrideLookupAppId	//An override appId for lookup an experimention definition in MC
	5: optional i32 partitionId	//An user ID or server ID which is used to partition experiment users into different variants. If not provided, then userId in $request object will be used as $partitionId.
}
service Experiment{
	dapi.DAPIResponse Get(1:ExperimentGetArgs arguments)
	dapi.DAPIResponse GetMulti(1:ExperimentGetMultiArgs arguments)
	dapi.DAPIResponse LogAndGet(1:ExperimentLogAndGetArgs arguments)
	dapi.DAPIResponse Exists(1:ExperimentExistsArgs arguments)
	dapi.DAPIResponse LogGoal(1:ExperimentLogGoalArgs arguments)
	dapi.DAPIResponse GetMetadata(1:ExperimentGetMetadataArgs arguments)
    dapi.DAPIResponse GetDate(1:ExperimentGetDateArgs arguments)
    dapi.DAPIResponse LogAndGetMultiWithVariantNameArgs(1:ExperimentLogAndGetMultiWithVariantNameArgs arguments)
    dapi.DAPIResponse GetMultiWithVariantNameArgs(1:ExperimentGetMultiWithVariantNameArgs arguments)
}
