include "dapi.thrift"

struct ZidsMapArgs{
	1: list<string> uids //A list of user ids to get mapping for
	2: string fromNetwork //The social network that the uids belong to
}

service Zids {
	dapi.DAPIResponse Map(1:ZidsMapArgs arguments)
}
