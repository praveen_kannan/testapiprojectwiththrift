include "dapi.thrift"


struct LeaderboardGetListArgs {
	1: optional i32 appId	//The app id of the application using the leaderboard service; this may or may not be the the canonical game id; games may also use zlive ids.
	2: string name	//The name of the leaderboard being accessed. This is an arbitrary string that must only be unique on a per-game basis.
	3: string filter	//The filter used to retrieve the list. Default is 'top'; acceptable options are:
	4: optional string zid	//The zid to center on. Required only for the 'surrounding' filter. Unused otherwise. Optionally, can be assigned a playerId in order to override the request's userId. Mapping for playerId must be done before the call is made
	5: optional list<string> zids	//List of zids to retrieve. Required only for the 'friends' filter. Unused otherwise. Optionally, can be assigned a list of playerIds if the leaderboard uses playerId as key Mapping for playerIds must be done before the call is made
	6: optional i32 count	//The maximum number of results to return. Optional. Default: 20.
	7: optional i32 periodIndex	//Represents interval for which the surrounding query is run (one-based index). Optional. Only used for the 'surrounding' filter for periodic leaderboard. Default: periodCount associated with the leaderboard, Min: 1, Max: periodCount.
	8: optional i32 oldestPeriodIndex	//value for start of interval range (oldest timestamp) for which to retrieve the values. One-based index. Optional. Default: 1, Min: 1, Max: periodCount associated with the leaderboard.
	9: optional i32 newestPeriodIndex	//value for end of interval range (newest timestamp) for which to retrieve the values. One-based index. Optional. Default: periodCount associated with the leaderboard, Min: 1, Max: periodCount.
}
struct LeaderboardSetScoreArgs {
	1: optional i32 appId	//Application Id.
	2: optional string zid	//User Id. Optionally, can be assigned a playerId if the leaderboard uses playerId as key Mapping for playerId must be done before the call is made
	3: string name	//The name of the leaderboard being accessed. This is an arbitrary string that must only be unique on a per-game basis.
	4: string value	//The new value to set.
	5: optional list<string> extra	//Extra metadata to associate with this score.
	6: optional list<string> filter	//Filters to apply for conditional set. Options: 'ifMax', 'ifMin', and 'none'. Defaults to 'ifMax' if the metric's configured sort is 'descending' which means that if the new value is larger, it will replace the current value. Similarly this defaults to 'ifMin' if the sort is 'ascending'. To ignore the configred sort and always set overwrite the current value with the new one, set 'filter' = 'none'
}
struct LeaderboardCreateArgs {
	1: string name	//The name of the leaderboard being created. This is an arbitrary string that must be unique on a per-game basis. This name is used as a key to identify the leaderboard for all other API calls.
	2: optional string sort	//Sort order for the leaderboard: 'ascending' or 'descending'. Default: 'descending'. 'Descending' means higher scores are better than lower scores, and 'ascending' means the opposite.
	3: optional string periodUnits	//Periodic leaderboards may specify 'days', 'weeks' or 'months' to specify the length of their period intervals. The default value ('infinite') creates a permenant leaderboard.
	4: optional i32 period	//The length of a period interval is period * periodUnits. Default: 1
	5: optional i32 periodCount	//Queries on a periodic leaderboard will pull in periodCount intervals.
	6: optional i32 periodStart	//Starting timestamp for the first interval. Default: 1325404800 (Sun, 01 Jan 2012 08:00:00 GMT)
	7: optional bool serverOnlyWrites	//Configure the leaderboard to allow writes only from servers and disallow user sessions.
}
struct LeaderboardDeleteArgs {
	1: string name	//The name of the leaderboard being accessed.
}
struct LeaderboardGetConfigArgs {
	1: optional string name	//The name of the leaderboard being accessed.
}

service Leaderboard{
	dapi.DAPIResponse GetList(1:LeaderboardGetListArgs arguments)
	dapi.DAPIResponse SetScore(1:LeaderboardSetScoreArgs arguments)
	dapi.DAPIResponse Create(1:LeaderboardCreateArgs arguments)
	dapi.DAPIResponse Delete(1:LeaderboardDeleteArgs arguments)
	dapi.DAPIResponse GetConfig(1:LeaderboardGetConfigArgs arguments)

}
