struct CallRequest {
	1: string method
	2: optional map<string,string> argz
}

struct Session {
	1: optional string access_token
	2: optional string user_id
	3: optional string oauth_token
	4: optional string oauth_token_secret
}

struct UserToken {
	1: string appId
	2: string snId
	3: string userId
	4: Session session
}

struct AppToken {
	1: string appId
	2: string snId
	3: string secret
}

struct DAPIRequest {
	1: list<CallRequest> calls
	2: optional UserToken userToken
	3: optional AppToken appToken
	4: optional string stringToken // Translated/Anonymous/WFN
}

struct DAPIError {
	1: string message
	2: string type
	3: optional string data
}

struct CallResult {
	1: string name
    2: string data
	3: optional DAPIError error
}

struct DAPIResponse {
	1: list<CallResult> calls
	2: optional DAPIError error
	3: optional string token
	4: optional string zid
}
