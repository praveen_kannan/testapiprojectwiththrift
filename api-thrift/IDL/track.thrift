include "dapi.thrift"

struct TrackLogCountArgs {
	1: string counter	//The name of the counter.
	2: optional i32 value	//The value of the counter.
	3: optional string attribute	//User defined attribute field.
	4: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	5: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	6: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	7: optional string family	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	8: optional string genus	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and family. Data must be filled up in sequence, starting from kingdom then phylum and so on
	9: optional i64 clientDeviceTs	//Timestamp of the client device.
	10: optional double sampleRate	//Rate to sample. (Range: 0.00 to 1.00) Example: For 10% sampling, specify sampleRate = 0.1. Default is 1.0.
	11: optional bool sampleByUser	//Flag to request sampling based on user.
	12: optional string overrideAppId	//An appId to override the implicit one.
	13: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogCountDetailArgs {
	1: optional string string1	//User-defined string fields.
	2: optional string string2	//User-defined string fields.
	3: optional string string3	//User-defined string fields.
	4: optional string string4	//User-defined string fields.
	5: optional string string5	//User-defined string fields.
	6: optional string string6	//User-defined string fields.
	7: optional string string7	//User-defined string fields.
	8: optional string string8	//User-defined string fields.
	9: optional i32 number1	//User-defined numeric fields.
	10: optional i32 number2	//User-defined numeric fields.
	11: optional i32 number3	//User-defined numeric fields.
	12: optional i32 number4	//User-defined numeric fields.
	13: optional i32 number5	//User-defined numeric fields.
	14: optional i32 number6	//User-defined numeric fields.
	15: optional i32 number7	//User-defined numeric fields.
	16: optional i32 number8	//User-defined numeric fields.
	17: optional i64 clientDeviceTs	//The Timestamp of the client device.
	18: optional double sampleRate	//Rate to sample. (Range: 0.00 to 1.00) Example: For 10% sampling, specify sampleRate = 0.1. Default is 1.0.
	19: optional bool sampleByUser	//Flag to request sampling based on user.
	20: optional string overrideAppId	//An appId to override the implicit one.
	21: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogPerfArgs {
	1: string metric	//The metric name to track. Use only these values: api fps bandwidth mem_usage transition request load error timing
	2: optional i32 value	//The value of the metric.
	3: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	4: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	5: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	6: optional string family	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	7: optional string genus	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and family. Data must be filled up in sequence, starting from kingdom then phylum and so on
	8: optional double sampleRate	//Rate to sample. (Range: 0.00 to 1.00) Example: For 10% sampling, specify sampleRate = 0.1. Default is 1.0.
	9: optional bool sampleByUser	//Flag to request sampling based on user.
	10: optional string overrideAppId	//An appId to override the implicit one.
	11: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogLanguageArgs {
	1: string language	//The language used by the user. One of the following options: ar - Arabic zh - Chineese nl - Dutch en - English fr - French de - German id - Indonesian it - Italian ja - Japanese ko - Korean ms - Malaysian pl - Polish pt - Portuguese ru - Russian zhcn - Simplified Chinese es - Spanish th - Thai tr - Turkish da - Danish sv - Swedish nb - Norwegian xx - Unknown
	2: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	3: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	4: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	5: optional string family	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	6: optional string genus	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and family. Data must be filled up in sequence, starting from kingdom then phylum and so on
	7: optional i32 value1	//User-defined value.
	8: optional i32 value2	//User-defined values.
	9: optional i64 clientDeviceTs	//The timestamp of the client device.
	10: optional bool isChanged	//Flag indicating if the language was changed. Default is false.
	11: optional string overrideAppId	//An appId to override the implicit one.
	12: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogNumFriendsArgs {
	1: string metric	//The metric name to track. One of the following values: num_neighbor num_sn_friends num_friends_with_app num_non_friend_neighbors
	2: i32 numFriends	//The number of friends.
	3: optional i64 clientDeviceTs	//Timestamp of the client device.
	4: optional string overrideAppId	//An appId to override the implicit one.
	5: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogAppFriendArgs {
	1: list<string> friendZidList	//Array of friend zids.
	2: string action	//Either 'add' or 'remove' (required).
	3: optional string friendGameId	//User-defined field. Used to represent the host game id
	4: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	5: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	6: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	7: optional string family	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	8: optional string genus	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and class. Data must be filled up in sequence, starting from kingdom then phylum and so on
	9: optional string overrideAppId	//An appId to override the implicit one.
	10: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogGameFriendArgs {
	1: list<string> friendZidList	//Array of friend zids.
	2: string action	//Either 'add' or 'remove' (required).
	3: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	4: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	5: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	6: optional string overrideAppId	//An appId to override the implicit one.
	7: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogDemographicArgs {
	1: i32 age	//Age of the user.
	2: string firstName	//First name of the user.
	3: string lastName	//Last name of the user.
	4: string userName	//Name of the user.
	5: string gender	//Gender of the user.
	6: string email	//Email of the user.
	7: string locale	//Locale of the user.
	8: string timezone	//Timezone of the user.
	9: optional string pictureUrl	//Picture url of the user.
	10: optional i64 birthdate	//Birthdate of the user.
	11: optional string ipCountry	//IpCountry of the user.
	12: optional i64 clientDeviceTs	//Timestamp of the client device.
	13: optional string overrideAppId	//An appId to override the implicit one.
	14: optional string overrideSnid	//A social network id to override the implicit one.
	15: optional string city	//City of the user.
	16: optional string state	//State of the user.
	17: optional string country	//Country of the user.
	18: optional string zip	//Zip of the user.
	19: optional string hostOrIp	//Host or IP address.
	20: optional string emailStatus	//Email status
	21: optional string ageMax	//Max age of from Facebook
	22: optional string ageMin	//Min age of from Facebook
}
struct TrackSetUserIpArgs {
	1: optional string userIp	//IP address of the user.
}
struct TrackLogVisitArgs {
	1: optional bool isActive	//Should be set to true when the user's client has become active. false means it is still loading. This allows you to track load times and user drop off accurately.
	2: optional string channel	//The source where the view originated (https://zyntranet.corp.zynga.com/display/Stats/ChannelTaxonomy).
	3: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	4: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	5: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	6: optional string family	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	7: optional string genus	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and family. Data must be filled up in sequence, starting from kingdom then phylum and so on
	8: optional string fromZid	//From zid.
	9: optional i32 fromSnid	//From social network id.
	10: optional i64 fromTs	//From timestamp.
	11: optional i64 clientDeviceTs	//The client device timestamp.
	12: optional i32 externalSendId	//Used for storing event ids like the one that FB sends for notifs.
	13: optional string overrideAppId	//An appId to override the implicit one.
	14: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogVisitSendKeyArgs {
	1: string sendKey	//A sendKey built with buildSendKey() or rebuildSendKey().
	2: optional bool isActive	//Should be set to true when the user's client has become active; false means it is still loading. This allows you to track load times and user drop off accurately.
	3: optional i64 clientDeviceTs	//Timestamp of the client device.
	4: optional string overrideAppId	//An appId to override the implicit one.
	5: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogSessionArgs {
	1: i64 startTs	//Session Start Timestamp in UTC.
	2: i64 endTs	//Session End Timestamp in UTC.
	3: optional i64 activeTime	//Indicates when the session became active after loading.
	4: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	5: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	6: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	7: optional string family	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	8: optional string genus	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and family. Data must be filled up in sequence, starting from kingdom then phylum and so on
	9: optional string attribute1	//User-defined attribute. Independent from the keyX arguments.
	10: optional i64 clientDeviceTs	//Timestamp of the client device.
	11: optional string overrideAppId	//An appId to override the implicit one.
	12: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogInstallArgs {
	1: string channel	//Information available in (https://zyntranet.corp.zynga.com/display/Stats/ChannelTaxonomy).
	2: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	3: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	4: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	5: optional string family	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	6: optional string genus	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and family. Data must be filled up in sequence, starting from kingdom then phylum and so on
	7: string sendKey	//A sendKey built with buildSendKey() or rebuildSendKey().
	8: optional i64 clientDeviceTs	//Timestamp of the client device.
	9: optional i32 externalSendId	//Used for storing external ids like the one that FB sends for notifs.
	10: optional string overrideAppId	//An appId to override the implicit one.
	11: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogPreInstallArgs {
	1: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	2: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	3: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	4: optional string family	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	5: optional string genus	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and family. Data must be filled up in sequence, starting from kingdom then phylum and so on
	6: optional i64 clientDeviceTs	//Timestamp of the client device.
	7: optional string overrideAppId	//An appId to override the implicit one.
	8: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogPreinstallSendkeyArgs {
	1: string sendKey	//A sendKey built with buildSendKey() or rebuildSendKey().
	2: optional i64 clientDeviceTs	//Timestamp of the client device.
	3: optional i32 externalSendId	//Used for storing external ids like the one that FB sends for notifs.
	4: optional string overrideAppId	//An appId to override the implicit one.
	5: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogInstallSendKeyArgs {
	1: string sendKey	//A sendKey built with buildSendKey() or rebuildSendKey().
	2: optional i64 clientDeviceTs	//Timestamp of the client device.
	3: optional i32 externalSendId	//Used for storing external ids like the one that FB sends for notifs.
	4: optional string overrideAppId	//An appId to override the implicit one.
	5: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogUninstallArgs {
	1: optional i64 clientDeviceTs	//Timestamp of the client device.
	2: optional string overrideAppId	//An appId to override the implicit one.
	3: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogAssociateArgs {
	1: string attribute	//Name of the key to be associated with the user.
	2: string value1	//Value of the key to be associated with the user.
	3: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	4: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	5: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	6: optional string family	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	7: optional string genus	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and family. Data must be filled up in sequence, starting from kingdom then phylum and so on
	8: optional i32 value2	//User-defined value.
	9: optional i32 value3	//User-defined value.
	10: optional i64 clientDeviceTs	//Timestamp of the client device.
	11: optional string overrideAppId	//An appId to override the implicit one.
	12: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogSocialNetworkTransitionArgs {
	1: i32 value2	//The prior social network id.
	2: string value3	//The prior zid.
}
struct TrackLogDeviceInfoArgs {
	1: string kingdom	//App version
	2: optional string phylum	//Device OS Version
	3: optional string class_	//Device type with generation if available - for iOS, can be retrieved by sysctl call.
}
struct TrackLogMilestoneArgs {
	1: optional string milestone	//A stage within the application, you want to record explicitly. e.g. Level 'X' reached, Users first quest, User makes greater than X invites.
	2: optional string value	//A value you want to associate with the milestone.
	3: optional i64 clientDeviceTs	//Timestamp of the client device.
	4: optional string overrideAppId	//An appId to override the implicit one.
	5: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogLevelArgs {
	1: string level	//The level achieved.
	2: optional i64 clientDeviceTs	//Timestamp of the client device.
	3: optional string overrideAppId	//An appId to override the implicit one.
	4: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogDirectPermissionArgs {
	1: string key	//Key.
	2: string action	//Grant or deny.
	3: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	4: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	5: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	6: optional string overrideAppId	//An appId to override the implicit one.
	7: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackBuildSendKeyArgs {
	1: string channel	//The channel.
	2: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	3: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	4: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	5: optional string family	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	6: optional string genus	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and family. Data must be filled up in sequence, starting from kingdom then phylum and so on
	7: optional i32 fromClientId	//From client id.
	8: optional string toZid	//The target zid.
	9: optional i32 toClientId	//The target client id.
	10: optional i32 toGameId	//The target game id.
	11: optional i32 toSnid	//The target social network id.
	12: optional i32 externalSendId	//Used for storing external ids like the one that FB sends for notifs.
}
struct TrackRebuildSendKeyArgs {
	1: string sendKeyOld	//The old sendKey to be rebuilt.
	2: optional string fromZid	//From zid.
	3: optional string overrideChannel	//Override the channel field in the original sendKey.
	4: optional string overrideKey1	//Override the key1 in the original sendKey.
	5: optional string overrideKey2	//Override the key2 in the original sendKey.
	6: optional string overrideKey3	//Override the key3 in the original sendKey.
	7: optional string overrideKey4	//Override the key4 in the original sendKey.
	8: optional string overrideKey5	//Override the key5 in the original sendKey.
	9: optional bool setNewSendTime	//Flag to indicate if new send time has to be set.
	10: optional i32 overrideFromClientId	//Override the fromClientId in the original sendKey.
	11: optional string overrideToZid	//Override the targetUid in the original sendKey.
	12: optional i32 overrideToClientId	//Override the toClientId in the original sendKey.
	13: optional i32 overrideToGameId	//Override the toGameId in the original sendKey.
	14: optional i32 overrideToSnid	//Override the toSnid in the original sendKey.
	15: optional i32 overrideExternalSendId	//Override the externalSendId in the original sendKey.
	16: optional string overrideKingdom	//Same as overrideKey1.
	17: optional string overridePhylum	//Same as overrideKey2.
	18: optional string overrideClass	//Same as overrideKey3.
	19: optional string overrideFamily	//Same as overrideKey4.
	20: optional string overrideGenus	//Same as overrideKey5.
}
struct TrackLogMessageSendKeyArgs {
	1: string sendKey	//sendKey built with buildSendKey() or rebuildSendKey().
	2: string status	//Status of the message send.
	3: optional list<string> toZidList	//Array of target user zids.
	4: optional string overrideAppId	//An appId to override the implicit one.
	5: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogMessageArgs {
	1: string sendKey	//A sendKey built with buildSendKey() or rebuildSendKey().
	2: list<string> toZidList	//Array of target user zids.
	3: string channel	//The channel. More info at: https://zyntranet.corp.zynga.com/display/Stats/ChannelTaxonomy.
	4: optional string status	//Status of the Message send.
	5: optional string overrideKey1	//Override the key1 in the sendKey.
	6: optional string overrideKey2	//Override the key2 in the sendKey.
	7: optional string overrideKey3	//Override the key3 in the sendKey.
	8: optional string overrideKey4	//Override the key4 in the sendKey.
	9: optional string overrideKey5	//Override the key5 in the sendKey.
	10: optional i64 overrideFromTime	//Override the From time in the sendKey.
	11: optional i32 overrideToClientId	//Override the target client id in the sendKey.
	12: optional i32 overrideToGameId	//Override the target game id in the sendKey.
	13: optional i32 overrideToSnid	//Override the target social network id in the sendKey.
	14: optional i32 overrideExternalSendId	//Override the external send id in the sendKey.
	15: optional string overrideAppId	//An appId to override the implicit one.
	16: optional string overrideSnid	//A social network id to override the implicit one.
	17: optional string overrideKingdom	//Same as overrideKey1.
	18: optional string overridePhylum	//Same as overrideKey2.
	19: optional string overrideClass	//Same as overrideKey3.
	20: optional string overrideFamily	//Same as overrideKey4.
	21: optional string overrideGenus	//Same as overrideKey5.
}
struct TrackLogMessageClickSendKeyArgs {
	1: string sendKey	//sendKey built with buildSendKey() or rebuildSendKey().
	2: optional string clickType1	//If the message has multiple links within it or perhaps multiple calls to action, use the clickType* fields to differentiate between different types of clicks on the same message.
	3: optional string clickType2	//If the message has multiple links within it or perhaps multiple calls to action, use the clickType* fields to differentiate between different types of clicks on the same message.
	4: optional string clickType3	//If the message has multiple links within it or perhaps multiple calls to action, use the clickType* fields to differentiate between different types of clicks on the same message.
	5: optional i64 clientDeviceTs	//The client device timestamp.
	6: optional string overrideAppId	//An appId to override the implicit one.
	7: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogMessageClickArgs {
	1: optional string sendKey	//A sendKey built with buildSendKey() or rebuildSendKey().
	2: optional string overrideFromZid	//Override the fromUserId in the sendKey, when available.
	3: optional i32 overrideFromSnid	//Override the fromSocialNetworkId in the sendKey, when availabl.e
	4: optional string overrideChannel	//Override the channel in the sendKey when available.
	5: optional string overrideKey1	//Override the key1 in the sendKey when available.
	6: optional string overrideKey2	//Override the key2 in the sendKey when available.
	7: optional string overrideKey3	//Override the key3 in the sendKey when available.
	8: optional string overrideKey4	//Override the key4 in the sendKey when available.
	9: optional string overrideKey5	//Override the key5 in the sendKey when available.
	10: optional string clickType1	//If the message has multiple links within it, or perhaps multiple calls to action, use the clickType* fields to differentiate between different types of clicks on the same message.
	11: optional string clickType2	//If the message has multiple links within it, or perhaps multiple calls to action, use the clickType* fields to differentiate between different types of clicks on the same message.
	12: optional string clickType3	//If the message has multiple links within it, or perhaps multiple calls to action, use the clickType* fields to differentiate between different types of clicks on the same message.
	13: optional i64 clientDeviceTs	//Timestamp of the client device.
	14: optional i32 overrideExternalSendId	//Override the external send id in the sendKey when available.
	15: optional string overrideAppId	//An appId to override the implicit one.
	16: optional string overrideSnid	//A social network id to override the implicit one.
	17: optional string overrideKingdom	//Same as overrideKey1.
	18: optional string overridePhylum	//Same as overrideKey2.
	19: optional string overrideClass	//Same as overrideKey3.
	20: optional string overrideFamily	//Same as overrideKey4.
	21: optional string overrideGenus	//Same as overrideKey5.
}
struct TrackLogSocialArgs {
	1: string action	//Some action of the social contact.
	2: list<string> targetZidList	//The other actors in the social interaction. Array of zids.
	3: optional string actionObject	//An optional qualifier.
	4: optional string amount	//A qualifier on the amount of objects, expected is int, decimal values will be dropped.
	5: optional string attribute1	//User-defined attribute.
	6: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	7: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	8: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	9: optional string family	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	10: optional string genus	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and family. Data must be filled up in sequence, starting from kingdom then phylum and so on
	11: optional i32 targetSnid	//The target social network id.
	12: optional i32 targetClientId	//The target client id.
	13: optional i32 targetGameId	//The target game id.
	14: optional double sampleRate	//Rate to sample. (Range: 0.00 to 1.00) Example: For 10% sampling, specify sampleRate = 0.1. Default is 1.0.
	15: optional bool sampleByUser	//Flag to request sampling based on user.
	16: optional string overrideAppId	//An appId to override the implicit one.
	17: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogGoodsPurchaseArgs {
	1: string goodsName	//Game item being purchased.
	2: string goodsType	//Constants: consumable, durable.
	3: string goodSubtype	//Constants: perpetual, null.
	4: optional double sampleRate	//Rate to sample. (Range: 0.00 to 1.00) Example: For 10% sampling, specify sampleRate = 0.1. Default is 1.0.
	5: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	6: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	7: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	8: optional string family	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	9: optional string genus	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and family. Data must be filled up in sequence, starting from kingdom then phylum and so on
	10: optional i32 amount1	//Amount.
	11: optional i32 totalAmount	//totalAmount
	12: optional string currency1	//Currency1.
	13: optional string currency2	//Currency2.
	14: optional string currency3	//Currency3.
	15: optional string currency4	//Currency4.
	16: optional string currencyFlow	//Currency flow.
	17: optional i32 value1	//Value of good.
	18: optional i32 value2	//Value of good in second currency.
	19: optional i32 value3	//Value3.
	20: optional i32 value4	//Value4.
	21: optional i32 paidAmount	//Paid amount.
	22: optional i32 paidBalance	//Paid balance.
	23: optional i32 freeAmount	//Free amount.
	24: optional i32 freeBalance	//Free balance.
	25: optional string itemCode	//Item code.
	26: optional string overrideAppId	//An appId to override the implicit one.
	27: optional string overrideSnid	//A social network id to override the implicit one.
	28: optional string image_url	//A image url
}
struct TrackLogGoodsUseArgs {
	1: string goodsName	//Game item being purchased.
	2: string goodsType	//Constants, use only these: consumable durable
	3: optional string goodSubtype	//Constants: perpetual, null
	4: optional double sampleRate	//Rate to sample. (Range: 0.00 to 1.00) Example: For 10% sampling, specify sampleRate = 0.1. Default is 1.0.
	5: optional string useType	//Use_type.
	6: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	7: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	8: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	9: optional string family	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	10: optional string genus	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and family. Data must be filled up in sequence, starting from kingdom then phylum and so on
	11: optional i32 amount1	//Amount.
	12: optional i32 totalAmount	//totalAmount.
	13: optional string currency1	//currency.
	14: optional string currency2	//currency2.
	15: optional string currency3	//currency3.
	16: optional string currency4	//currency4.
	17: optional string currencyFlow	//Currency flow.
	18: optional i32 value1	//value1.
	19: optional i32 value2	//value2.
	20: optional i32 value3	//value3.
	21: optional i32 value4	//value4.
	22: optional i32 paidAmount	//Paid amount.
	23: optional i32 paidBalance	//Paid balance.
	24: optional i32 freeAmount	//Free amount.
	25: optional i32 freeBalance	//Free balance.
	26: optional string overrideAppId	//An appId to override the implicit one.
	27: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogEconomyArgs {
	1: i32 amount1	//A positive or negative amount, expected type is int, decimal values are dropped.
	2: string currency1	//The game currency being given - most games probably have one currency so this should be consistent.
	3: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	4: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	5: optional string class_	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	6: optional string family	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	7: optional string genus	//User-defined field. Used to represent hierarchical data along with kingdom, phylum, class and family. Data must be filled up in sequence, starting from kingdom then phylum and so on
	8: optional i32 totalAmount	//The amount of currency remaining after the transaction.
	9: optional string currency2	//Second currency.
	10: optional string currency3	//Third currency.
	11: optional string currency4	//Fourth currency.
	12: optional i32 amount2	//Value of good in second currency.
	13: optional i32 amount3	//Value of good in third currency.
	14: optional i32 amount4	//Value of good in fourth currency.
	15: optional string currencyFlow	//Currency flow.
	16: optional i64 clientDeviceTs	//Timestamp of the client device.
	17: optional string overrideAppId	//An appId to override the implicit one.
	18: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogPaymentArgs {
	1: i32 amount1	//The value in the currency (not the dollar value - this would be chips, coins, reward points, etc...), expected type is int, decimal values are dropped.
	2: string currency1	//The game currency (e.g. "chips", "coins", ...).
	3: string provider	//The payment provider - Use constants below only. android att fb_credits game_card global_collect itunes offerpal paymentech paypal_eco sprint tmobile
	4: optional string transId	//The transaction id.
	5: optional string status	//Status.
	6: optional string kingdom	//User-defined field. Used to represent hierarchical data along with phylum, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on.
	7: optional string phylum	//User-defined field. Used to represent hierarchical data along with kingdom, class, family and genus. Data must be filled up in sequence, starting from kingdom then phylum and so on
	8: optional i32 amount2 // Value of good in second currency.
	9: optional string overrideAppId	//An appId to override the implicit one.
	10: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackLogUserAgentArgs {
	1: string userAgent	//The browser user agent.
	2: optional string browser	//The browser name.
	3: optional string version	//The browser version.
	4: optional string os	//The operating system.
	5: optional string locale	//The locale.
	6: optional string overrideAppId	//An appId to override the implicit one.
	7: optional string overrideSnid	//A social network id to override the implicit one.
}
struct TrackInitDeferredTrackArgs {
	1: i32 batchUpdateFrequency	//The batch update frequency.
	2: i32 batchFlushFrequency	//The batch flush frequency.
}
struct TrackSetDeferredTrackArgs {
	1: string batchKey	//The batch key from initDeferredTrack.
	2: map<string,string> batchedTrackCalls	//The batch track calls.
}
service Track {
	dapi.DAPIResponse LogCount(1:TrackLogCountArgs arguments)
	dapi.DAPIResponse LogCountDetail(1:TrackLogCountDetailArgs arguments)
	dapi.DAPIResponse LogPerf(1:TrackLogPerfArgs arguments)
	dapi.DAPIResponse LogLanguage(1:TrackLogLanguageArgs arguments)
	dapi.DAPIResponse LogNumFriends(1:TrackLogNumFriendsArgs arguments)
	dapi.DAPIResponse LogAppFriend(1:TrackLogAppFriendArgs arguments)
	dapi.DAPIResponse LogGameFriend(1:TrackLogGameFriendArgs arguments)
	dapi.DAPIResponse LogDemographic(1:TrackLogDemographicArgs arguments)
	dapi.DAPIResponse SetUserIp(1:TrackSetUserIpArgs arguments)
	dapi.DAPIResponse LogVisit(1:TrackLogVisitArgs arguments)
	dapi.DAPIResponse LogVisitSendKey(1:TrackLogVisitSendKeyArgs arguments)
	dapi.DAPIResponse LogSession(1:TrackLogSessionArgs arguments)
	dapi.DAPIResponse LogInstall(1:TrackLogInstallArgs arguments)
	dapi.DAPIResponse LogPreInstall(1:TrackLogPreInstallArgs arguments)
	dapi.DAPIResponse LogPreinstallSendkey(1:TrackLogPreinstallSendkeyArgs arguments)
	dapi.DAPIResponse LogInstallSendKey(1:TrackLogInstallSendKeyArgs arguments)
	dapi.DAPIResponse LogUninstall(1:TrackLogUninstallArgs arguments)
	dapi.DAPIResponse LogAssociate(1:TrackLogAssociateArgs arguments)
	dapi.DAPIResponse LogSocialNetworkTransition(1:TrackLogSocialNetworkTransitionArgs arguments)
	dapi.DAPIResponse LogDeviceInfo(1:TrackLogDeviceInfoArgs arguments)
	dapi.DAPIResponse LogMilestone(1:TrackLogMilestoneArgs arguments)
	dapi.DAPIResponse LogLevel(1:TrackLogLevelArgs arguments)
	dapi.DAPIResponse LogDirectPermission(1:TrackLogDirectPermissionArgs arguments)
	dapi.DAPIResponse BuildSendKey(1:TrackBuildSendKeyArgs arguments)
	dapi.DAPIResponse RebuildSendKey(1:TrackRebuildSendKeyArgs arguments)
	dapi.DAPIResponse LogMessageSendKey(1:TrackLogMessageSendKeyArgs arguments)
	dapi.DAPIResponse LogMessage(1:TrackLogMessageArgs arguments)
	dapi.DAPIResponse LogMessageClickSendKey(1:TrackLogMessageClickSendKeyArgs arguments)
	dapi.DAPIResponse LogMessageClick(1:TrackLogMessageClickArgs arguments)
	dapi.DAPIResponse LogSocial(1:TrackLogSocialArgs arguments)
	dapi.DAPIResponse LogGoodsPurchase(1:TrackLogGoodsPurchaseArgs arguments)
	dapi.DAPIResponse LogGoodsUse(1:TrackLogGoodsUseArgs arguments)
	dapi.DAPIResponse LogEconomy(1:TrackLogEconomyArgs arguments)
	dapi.DAPIResponse LogPayment(1:TrackLogPaymentArgs arguments)
	dapi.DAPIResponse LogUserAgent(1:TrackLogUserAgentArgs arguments)
	dapi.DAPIResponse InitDeferredTrack(1:TrackInitDeferredTrackArgs arguments)
	dapi.DAPIResponse SetDeferredTrack(1:TrackSetDeferredTrackArgs arguments)
}
