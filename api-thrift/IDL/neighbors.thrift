include "dapi.thrift"


struct NeighborsAddArgs {
	1: list<string> neighborZids	//Array of neighbor ids.
	2: string appId	//The app id.
	3: optional string state	//With default value of 'neighbor', valid values are: 'neighbor', 'recommended'
}
struct NeighborsAreNeighborsArgs {
	1: string neighborZid	//Zid of the other neighbor.
	2: string appId	//The app id.
}
struct NeighborsConfirmArgs {
	1: list<string> neighborZids	//Array of neighbor ids.
	2: string appId	//The app id.
}
struct NeighborsDenyArgs {
	1: list<string> neighborZids	//Array of neighbor ids.
	2: string appId	//The app id.
}
struct NeighborsGetArgs {
	1: string appId	//The app id.
	2: optional string state	//With default value of 'neighbor', valid values are: 'neighbor', 'pendingInvite', 'sentInvite', 'recommended', 'rejected'
	3: optional bool withTimestamp	//With default value of false, if true then an associative array with neighbor id as key and timestamp when it was added as value
}
struct NeighborsGetAllArgs {
}
struct NeighborsGetMutualNeighborsArgs {
	1: string neighborZid	//Zid of second user.
	2: string appId	//The app id.
}
struct NeighborsRemoveArgs {
	1: list<string> neighborZids	//Array of neighbor zids.
	2: string appId	//The app id.
	3: optional string state	//With default value of 'neighbor', valid values are: 'neighbor', 'recommended'
}
struct NeighborsSetArgs {
	1: list<string> neighborZids	//Array of neighbor zids.
	2: string appId	//The app id.
	3: optional string state	//With default value of 'neighbor', valid values are: 'neighbor', 'recommended'
}
struct NeighborsAdminSetArgs {
	1: list<string> neighborZids	//Array of neighbor zids.
	2: string state	//Valid states are 'invited', 'neighbor', 'denied', 'removed', 'deleted'.
}
struct NeighborsAdminDeleteAllArgs {
	1: optional bool force	//With default value of false, remove user from all the other blobs that he has relationships with; otherwise if set to true, don't bother going to all user's relationships and cleaning them up.
	2: optional bool allData	//With default value of false, delete the user's app all neighbors; otherwise if set to true, delete the entire user blob
}

service Neighbors{
	dapi.DAPIResponse Add(1:NeighborsAddArgs arguments)
	dapi.DAPIResponse AreNeighbors(1:NeighborsAreNeighborsArgs arguments)
	dapi.DAPIResponse Confirm(1:NeighborsConfirmArgs arguments)
	dapi.DAPIResponse Deny(1:NeighborsDenyArgs arguments)
	dapi.DAPIResponse Get(1:NeighborsGetArgs arguments)
	dapi.DAPIResponse GetAll(1:NeighborsGetAllArgs arguments)
	dapi.DAPIResponse GetMutualNeighbors(1:NeighborsGetMutualNeighborsArgs arguments)
	dapi.DAPIResponse Remove(1:NeighborsRemoveArgs arguments)
	dapi.DAPIResponse Set(1:NeighborsSetArgs arguments)
	dapi.DAPIResponse AdminSet(1:NeighborsAdminSetArgs arguments)
	dapi.DAPIResponse AdminDeleteAll(1:NeighborsAdminDeleteAllArgs arguments)

}
