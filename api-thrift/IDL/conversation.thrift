include "dapi.thrift"

struct ConversationJoinArgs {
	1: string id	//Join the conversation with a specific Conversation ID.
	2: optional string meta	//Arbitrary data that is stored along with the conversation if it needs to be created.
	3: optional list<string> subscribers	//A set of ZIDs who are subscribers to this conversation if the conversation needs to be created.
	4: optional string accessType	//A string that identifies the type of access permitted for this conversation: one of ['public', 'private', 'moderated']. This will determine the permission settings of whether to use an empty blacklist for public conversations or a whitelist of the initial subscribers for private and moderated conversations. Default to 'public'.
	5: optional string filters	//A set of filter information used to remove fields from the result when not needed.
	6: optional list<string> filters_dot_attrs	//Attributes to get: 'subscribers', 'likes', 'readers', 'messages', 'unread', 'meta', 'creator'. Leave it out to grab everything.
	7: optional list<string> filters_dot_zids	//An array of zids to use for further filtering the each of the attributes. Leave it out to include all zids.
	8: optional string filters_dot_range	//A set of information that describes the range of messages to return.
	9: optional string filters_dot_range_dot_begin	//Start from this Message ID. Default to the first available message.
	10: optional string filters_dot_range_dot_end	//Finish on this Message ID. Default to the most recent message.
	11: optional string filters_dot_range_dot_limit	//Constrain the number of messages returned. Always include the most recent messages unless the 'begin' filter is used. In that case, start counting from the beginning of the list instead of backwards from the end of the list.
	12: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	13: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationCreateArgs {
	1: optional string id	//Create the conversation with a specific Conversation ID or generate one randomly.
	2: optional string meta	//Arbitrary data that is stored along with the conversation.
	3: optional list<string> subscribers	//A set of ZIDs who are subscribers to this conversation.
	4: optional string accessType	//A string that identifies the type of access permitted for this conversation: one of ['public', 'private', 'moderated']. This will determine the permission settings of whether to use an empty blacklist for public conversations or a whitelist of the initial subscribers for private and moderated conversations. Default to 'public'.
	5: optional list<string> permissions	//A set of permission instructions for zids after the initial permissions are set. The structure is set up as a two-tiered array. The first tier of keys are zids. The second tier of keys are valid permission types: ['read', 'write', 'delete', 'grant', 'revoke']. Each permission type points to a boolean that represents whether the user has the given permission or not. It looks like this: array(zid => array(permissionType => boolean)).
	6: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	7: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationGetArgs {
	1: list<string> ids	//A list of Conversation IDs to fetch. Passing a single conversation ID will return the data about that conversation. Passing an array will return an array of (conversationID : conversation) pairs.
	2: optional string filters	//A set of filter information used to remove fields from the result when not needed.
	3: optional list<string> filters_dot_attrs	//Attributes to get: 'subscribers', 'likes', 'readers', 'messages', 'unread', 'meta', 'creator'. Leave it out to grab everything.
	4: optional list<string> filters_dot_zids	//An array of zids to use for further filtering the each of the attributes. Leave it out to include all zids.
	5: optional string filters_dot_range	//A set of information that describes the range of messages to return.
	6: optional string filters_dot_range_dot_begin	//Start from this Message ID. Default to the first available message.
	7: optional string filters_dot_range_dot_end	//Finish on this Message ID. Default to the most recent message.
	8: optional string filters_dot_range_dot_limit	//Constrain the number of messages returned. Always include the most recent messages unless the 'begin' filter is used. In that case, start counting from the beginning of the list instead of backwards from the end of the list.
	9: optional string debug	//A set of flags used to return extra developer information in the response.
	10: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	11: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationGetLastMessageIDArgs {
	1: list<string> ids	//A list of Conversation IDs.
}
struct ConversationRemoveArgs {
	1: string id	//The Conversation ID to remove.
	2: optional string debug	//A set of flags used to return extra developer information in the response.
	3: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	4: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationSubscribeArgs {
	1: string id	//The Conversation ID.
	2: optional string debug	//A set of flags used to return extra developer information in the response.
	3: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	4: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationUnsubscribeArgs {
	1: string id	//The Conversation ID.
	2: optional string debug	//A set of flags used to return extra developer information in the response.
	3: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	4: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationLikeArgs {
	1: string id	//The Conversation ID to like.
	2: optional string debug	//A set of flags used to return extra developer information in the response.
	3: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	4: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationUnlikeArgs {
	1: string id	//The Conversation ID to unlike.
	2: optional string debug	//A set of flags used to return extra developer information in the response.
	3: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	4: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationHideUsersArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: list<string> offensiveZIDs	//The list of users to hide.
	3: optional string debug	//A set of flags used to return extra developer information in the response.
	4: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	5: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationShowUsersArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: list<string> offensiveZIDs	//The list of users to show.
	3: optional string debug	//A set of flags used to return extra developer information in the response.
	4: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	5: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationBlockUsersArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: list<string> offensiveZIDs	//The list of offensive zids that should be blocked.
	3: optional string debug	//A set of flags used to return extra developer information in the response.
	4: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	5: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationUnblockUsersArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: list<string> offensiveZIDs	//The list of offensive zids that should be unblocked.
	3: optional string debug	//A set of flags used to return extra developer information in the response.
	4: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	5: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationReportUsersArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: list<string> offensiveZIDs	//The list of offensive zids that should be blocked.
	3: list<string> categoryCodes	//The category codes of the report.
	4: optional string reason	//The reason for the report.
	5: optional bool skipBlock 	//Skip the automatic block. Default false.
	6: optional string debug	//A set of flags used to return extra developer information in the response.
	7: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	8: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationIgnoreUsersArgs {
	1: list<string> offensiveZIDs	//The list of offensive zids that should be ignored.
	2: optional string debug	//A set of flags used to return extra developer information in the response.
	3: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	4: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationAcknowledgeUsersArgs {
	1: list<string> offensiveZIDs	//The list of offensive zids that should be ignored.
	2: optional string debug	//A set of flags used to return extra developer information in the response.
	3: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	4: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationMessageArgs {
	1: string id	//The Conversation ID that will contain this message.
	2: string text	//The text of the message to send.
	3: optional string meta	//Store some metadata along with the message.
	4: optional string debug	//A set of flags used to return extra developer information in the response.
	5: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	6: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationEditMessageArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: string messageID	//The Message ID to change.
	3: string meta	//The replacement meta field. It must not be null.
	4: optional string debug	//A set of flags used to return extra developer information in the response.
	5: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	6: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationLikeMessageArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: string messageID	//The Message ID to like.
	3: optional string debug	//A set of flags used to return extra developer information in the response.
	4: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	5: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationUnlikeMessageArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: string messageID	//The Message ID to unlike.
	3: optional string debug	//A set of flags used to return extra developer information in the response.
	4: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	5: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationHideMessageArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: string messageID	//The Message ID to hide.
	3: optional string debug	//A set of flags used to return extra developer information in the response.
	4: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	5: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationShowMessageArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: string messageID	//The Message ID to show.
	3: optional string debug	//A set of flags used to return extra developer information in the response.
	4: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	5: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationBlockMessageArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: string messageID	//The Message ID to hide.
	3: optional string debug	//A set of flags used to return extra developer information in the response.
	4: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	5: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationUnblockMessageArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: string messageID	//The Message ID to show.
	3: optional string debug	//A set of flags used to return extra developer information in the response.
	4: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	5: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationReportMessageArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: string messageID	//The Message ID to report.
	3: list<string> categoryCodes	//The category codes of the report.
	4: optional string reason	//The reason for the report.
	5: optional bool skipBlock	//Skip the automatic block. Default false.
	6: optional string debug	//A set of flags used to return extra developer information in the response.
	7: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	8: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationRemoveMessageArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: string messageID	//The Message ID to remove.
	3: optional string debug	//A set of flags used to return extra developer information in the response.
	4: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	5: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationMarkReadArgs {
	1: string id	//The Conversation ID.
	2: optional string messageID	//The largest Message ID that has been read so far. Default to the most recently written message.
	3: optional string debug	//A set of flags used to return extra developer information in the response.
	4: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	5: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationListAllArgs {
	1: optional string debug	//A set of flags used to return extra developer information in the response.
	2: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	3: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationRemoveUserPreferencesArgs {
	1: optional string debug	//A set of flags used to return extra developer information in the response.
	2: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	3: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationAddPermissionArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: list<string> permissionTypes	//The set of permissions to add: ['read', 'write', 'delete', 'grant', 'revoke']
	3: list<string> permissionZIDs	//The set of zids receiving new permissions.
	4: optional string debug	//A set of flags used to return extra developer information in the response.
	5: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	6: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationRemovePermissionArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: list<string> permissionTypes	//The set of permissions to remove: ['read', 'write', 'delete', 'grant', 'revoke']
	3: list<string> permissionZIDs	//The set of zids receiving new permissions.
	4: optional string debug	//A set of flags used to return extra developer information in the response.
	5: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	6: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationHasPermissionArgs {
	1: string id	//The Conversation ID that contains the Message ID.
	2: list<string> permissionTypes	//The set of permissions to check: ['read', 'write', 'delete', 'grant', 'revoke']
	3: list<string> permissionZIDs	//The set of zids receiving new permissions.
	4: optional string debug	//A set of flags used to return extra developer information in the response.
	5: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	6: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationPluginVersionArgs {
	1: optional string debug	//A set of flags used to return extra developer information in the response.
	2: optional string debug_dot_apiTimeMS	//True to return the total server API time in milliseconds.
	3: optional string debug_dot_networkCallTimeMS	//True to return the network calls as seen from the server in milliseconds.
}
struct ConversationGetStatsArgs {
	1: string id	//The Conversation ID.
}

service Conversation{
	dapi.DAPIResponse Join(1:ConversationJoinArgs arguments)
	dapi.DAPIResponse Create(1:ConversationCreateArgs arguments)
	dapi.DAPIResponse Get(1:ConversationGetArgs arguments)
	dapi.DAPIResponse GetLastMessageID(1:ConversationGetLastMessageIDArgs arguments)
	dapi.DAPIResponse Remove(1:ConversationRemoveArgs arguments)
	dapi.DAPIResponse Subscribe(1:ConversationSubscribeArgs arguments)
	dapi.DAPIResponse Unsubscribe(1:ConversationUnsubscribeArgs arguments)
	dapi.DAPIResponse Like(1:ConversationLikeArgs arguments)
	dapi.DAPIResponse Unlike(1:ConversationUnlikeArgs arguments)
	dapi.DAPIResponse HideUsers(1:ConversationHideUsersArgs arguments)
	dapi.DAPIResponse ShowUsers(1:ConversationShowUsersArgs arguments)
	dapi.DAPIResponse BlockUsers(1:ConversationBlockUsersArgs arguments)
	dapi.DAPIResponse UnblockUsers(1:ConversationUnblockUsersArgs arguments)
	dapi.DAPIResponse ReportUsers(1:ConversationReportUsersArgs arguments)
	dapi.DAPIResponse IgnoreUsers(1:ConversationIgnoreUsersArgs arguments)
	dapi.DAPIResponse AcknowledgeUsers(1:ConversationAcknowledgeUsersArgs arguments)
	dapi.DAPIResponse Message(1:ConversationMessageArgs arguments)
	dapi.DAPIResponse EditMessage(1:ConversationEditMessageArgs arguments)
	dapi.DAPIResponse LikeMessage(1:ConversationLikeMessageArgs arguments)
	dapi.DAPIResponse UnlikeMessage(1:ConversationUnlikeMessageArgs arguments)
	dapi.DAPIResponse HideMessage(1:ConversationHideMessageArgs arguments)
	dapi.DAPIResponse ShowMessage(1:ConversationShowMessageArgs arguments)
	dapi.DAPIResponse BlockMessage(1:ConversationBlockMessageArgs arguments)
	dapi.DAPIResponse UnblockMessage(1:ConversationUnblockMessageArgs arguments)
	dapi.DAPIResponse ReportMessage(1:ConversationReportMessageArgs arguments)
	dapi.DAPIResponse RemoveMessage(1:ConversationRemoveMessageArgs arguments)
	dapi.DAPIResponse MarkRead(1:ConversationMarkReadArgs arguments)
	dapi.DAPIResponse ListAll(1:ConversationListAllArgs arguments)
	dapi.DAPIResponse RemoveUserPreferences(1:ConversationRemoveUserPreferencesArgs arguments)
	dapi.DAPIResponse AddPermission(1:ConversationAddPermissionArgs arguments)
	dapi.DAPIResponse RemovePermission(1:ConversationRemovePermissionArgs arguments)
	dapi.DAPIResponse HasPermission(1:ConversationHasPermissionArgs arguments)
	dapi.DAPIResponse PluginVersion(1:ConversationPluginVersionArgs arguments)
	dapi.DAPIResponse GetStats(1:ConversationGetStatsArgs arguments)

}
