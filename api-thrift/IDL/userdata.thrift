include "dapi.thrift"

struct UserDataGetUserAttributesArgs {
	1: list<string> zids	//Array of user ids for which attributes are required.
	2: string snid	//The social network id.
	3: string appId	//The app id for which neighbors are required.
	4: optional list<string> userAttrs	//Array of attribute names whose values are required e.g. first_name, score_predicted_payer. if not passed then all attributes are returned.
}
struct UserDataGetFriendsByAppArgs {
	1: list<string> zids	//Array of user the ids of the users/friends that need to be filtered.
	2: string snid	//The social network id.
	3: string includedApps	//Comma-separated list of appIds that have to be included, pass -2 if you want to check any app.
	4: optional string excludedApps	//Comma-separated list of appIds that should be excluded from the check.
}
struct UserDataGetPredictedPayerPropertiesArgs {
	1: string snid	//The social network id.
}
struct UserDataGetASNArgs {
	1: string snid	//The social network id if the authentication type is application, otherwise it is derived from request
	2: string appId	//The app id if the authentication type is application, otherwise it is derived from request
}
struct UserDataGetASNInfoArgs {
	1: string snid	//The social network id if the authentication type is application, otherwise it is derived from request
	2: string appId	//The app id if the authentication type is application, otherwise it is derived from request
}
struct UserDataGetLikelyToShareFriendsArgs {
	1: string snid	//The social network id if the authentication type is application, otherwise it is derived from request
	2: string appId	//The app id if the authentication type is application, otherwise it is derived from request
}
struct UserDataGetTopHelpersArgs {
	1: string snid	//The social network id if the authentication type is application, otherwise it is derived from request
}
struct UserDataGetRecommendedAppsWebArgs {
	1: string snid	//The social network id if the authentication type is application, otherwise it is derived from request
	2: string appId	//The app id if the authentication type is application, otherwise it is derived from request
}
struct UserDataGetRecommendedAppsMobileArgs {
	1: string snid	//The social network id if the authentication type is application, otherwise it is derived from request
	2: string appId	//The app id if the authentication type is application, otherwise it is derived from request
}
struct UserDataGetOutOfNetworkFriendsArgs {
	1: string snid	//The social network id if the authentication type is application, otherwise it is derived from request
	2: string appId	//The app id if the authentication type is application, otherwise it is derived from request
}
struct UserDataGetNonASNArgs {
	1: string snid	//The social network id if the authentication type is application, otherwise it is derived from request
	2: string appId	//The app id if the authentication type is application, otherwise it is derived from request
}
struct UserDataGetOptimalGiftRecipientsArgs {
	1: list<string> zids	//String array of the ids of the friends from which to find the recipient list.
	2: string snid	//The social network id.
	3: string appId	//The app id.
	4: optional string experimentName	//Argument with empty default value, can be used to specify the experiment name, if any.
	5: optional i32 bucketSize	//Argument with default value of 40, specifies maximum number of entries in output.
	6: optional i32 Version	//Version of the list of gift recipients (default value is 3).
}
struct UserDataGetOptimalInviteRecipientsArgs {
	1: list<string> friends	//The ids of the friends from which to find the recipient list.
	2: list<string> neighbors	//The ids of neighbors.
	3: string snid	//The social network id.
	4: string appId	//The app id.
	5: optional string experimentName	//Argument with empty default value.
	6: optional string type	//Argument for rank type with default value of SGASN_INVITE_RETENTION. SGASN_INVITE_NEW_INSTALLS: neighbor suggestions of ASN,non-ASN senders, non-asn clickers, priors out of app who have installed this app since you last played (or same day) (lastplayed date). For users whose installdate < 2days, suggestion expaned to friends who installed same app < 7days ago. SGASN_INVITE_LAPSED:neighbor suggestions ASN,non-ASN senders, non-asn clickers, priors in app but not neighbors who have not played in the last 15 days. SGASN_INVITE_NEW_TO_NETWORK:invite suggestions fo users NOT playing any zynga app yet SGASN_INVITE_RETENTION:same logic as get_optimal_gift except we weed out neighbors. SGASN_INVITE_OUTOFGAME_ASN: same as SGASN_INVITE_NEW_INSTALLS except limit to friends who haven't installed the app SGASN_INVITE_OUTOFGAME_ASN_LAPSED: same as SGASN_INVITE_OUTOFGAME_ASN, but limit to friends whose last play date of any other apps is over 15 days
	7: optional list<string> otherApps	//Argument with default value of empty array. This is an array of other app Ids, which is used when type is one of SGASN_INVITE_NEW_INSTALLS, SGASN_INVITE_OUTOFGAME_ASN, or SGASN_INVITE_OUTOFGAME_ASN_LAPSED
	8: optional i32 bucketSizeAargument	//Size of the argument (default value is 60).
	9: optional i32 version	//Argument with default value of 3.
}
struct UserDataGetASNOfFriendsArgs {
	1: string snid	//The social network id.
	2: string appId	//The app id.
	3: list<string> friendIds //Array of sn friends zids supplied by app.
	4: optional list<string> allFriends	//Default value of nmtry array. These are all user's sn friends. If the app doesn't supply this list, we don't perform a check whether your friend's friends are your sn friends.
	5: optional bool allowNonFriend	//Argument with default value of false. If set to true, then it'll filter out the friend's friend who are your friends.
	6: optional string wdDays	//Defaults to 'all'. It controls up to what time period asn data to use, allowed values are 7, 14, 21 and 28.
	7: optional string rankByEngagementTypes	//Defaults to 'all'. If it is 0, it won't use engagement score to break even asn values. 'all' means always and int just to ensure the top N list are ranked using engagement score to break even asn values.
	8: optional string includeTypes	//argument with default value array with one element DS_TYPE_ASN. This is type of app friends (DS_TYPE_ASN DS_TYPE_NONASN_CLICKER DS_TYPE_NONASN_SENDER) to include in the return list, the order of type is important, it decides how the final list is ranked
	9: optional list<string> excludeTypes	//Default value of array with one element DS_TYPE_ASN. Specifies which types don't include friends' friends of these types.
	10: optional i32 bucketSize	//Default value of 60.
}
struct UserDataGetLevelArgs {
	1: string snid	//The social network id.
	2: string appId	//The app id.
}
struct UserDataSetLevelArgs {
	1: string snid	//The social network id.
	2: string appId	//The app id.
	3: string level	//The level to be set.
}
struct UserDataGetVisitArgs {
	1: string snid	//The social network id.
	2: string appId	//The app id.
}
struct UserDataGetSmartSystemsBucketNumberArgs {
	1: string snid	//The social network id.
	2: string appId	//The app id.
	3: string key1	//The threshold type: feed/request
	4: i32 numActiveFriends	//Number of active friends for current (zid,snid,appId)
	5: i32 numNeighbors	//Number of neighbors for current (zid,snid,appId)
	6: optional string key2,	//key3, key4, key5, featureIdentifier, gameData
}
struct UserDataGetPlayCountArgs {
	1: string snid	//the social network id
	2: optional list<string> opponentZids	//List of zids of opponents, if not passed then it would return count of plays for all the opponents
	3: optional string appIds	//The application ids.
	4: optional i32 minLoopSize	//minimum number of completed loops between user and opponent.
	5: optional i32 numDays	//Number of days to look back into history of game sessions
}
struct UserDataGetGamePlayHistoryArgs {
	1: string snid	//the social network id
	2: optional list<string> opponentZids	//Array of opponent zids against which the history is required. If not passed history would be aggregated across all opponents
	3: optional list<string> appIds	//Array of app ids for which history is required, if not passed would return history for every app.
	4: optional i32 numPlays	//Number of play session to look back into history
	5: optional i32 numDays	//Number of days to look back into history of game sessions
}
struct UserDataSetGamePlayResultArgs {
	1: string snid	//The social network id.
	2: string appId	//The application id.
	3: string opponentZid	//opponent zid.
	4: string outcome	//The game outcome won=1, lost=3, draw=3
	5: optional string timeOfCompletion	//time the game is completed, defaults to current time.
	6: optional string status	//status of the game, if any, defaults to blank
	7: optional i32 userScore	//score of the user
	8: optional i32 opponentScore	//score of the opponent
}
struct UserDataSetVisitArgs {
	1: string snid	//The social network id.
	2: string appId	//The app id.
	3: optional i32 version	//Argument with default value of 0.
	4: optional i32 clientid	//Argument with default value of 1.
}
struct UserDataGetInstallArgs {
	1: string snid	//The social network id.
	2: string appId	//The app id .
}
struct UserDataSetInstallArgs {
	1: string snid	//The social network id.
	2: string appId	//The app id.
	3: optional i32 version	//Argument with default value of 0.
	4: optional i32 clientid	//Argument with default value of 1.
}
struct UserDataGetAchievementArgs {
	1: string snid	//The social network id.
	2: string appId	//The app id.
}
struct UserDataSetAchievementArgs {
	1: string snid	//The social network id.
	2: string appId	//The app id.
	3: string achievement	//Pre-registered achievement name with Achievements Manager.
	4: i32 count	//The value to be set for achievement.
}
struct UserDataUpdateAchievementArgs {
	1: string snid	//The social network id.
	2: string appId	//The app id.
	3: string achievement	//Pre-registered achievement name with Achievements Manager.
	4: i32 delta	//The delta value for achievement.
	5: optional bool add	//Argument with default value true, set it to false if you want to use this as delete.
}
struct UserDataGetPredictedPayerPercentileArgs {
	1: string appId	//The app id for which predicted payer percentile is required.
	2: double predictedPayerScore	//Score which predicts probability of a payer likely to pay. This is available as a user attribute.
}
struct UserDataGetAppObjectArgs {
	1: string snid	//The social network id.
	2: string appId	//The app id for which the object is required.
}
struct UserDataSetAppObjectArgs {
	1: string snid	//The social network id.
	2: string appId	//The app id for which the object has to be stored.
	3: list<string> appObjectData	//Array array with app object data that need to be stored.
}
struct UserDataGetHashForZidArgs {
	1: string vendor	//The vendor for which the hash has to be created. Only first 45 characters are used.
}
struct UserDataGetZidForHashArgs {
	1: string hash	//The hash that needs to be converted to a zid.
}
service UserData{
	dapi.DAPIResponse GetUserAttributes(1:UserDataGetUserAttributesArgs arguments)
	dapi.DAPIResponse GetFriendsByApp(1:UserDataGetFriendsByAppArgs arguments)
	dapi.DAPIResponse GetPredictedPayerProperties(1:UserDataGetPredictedPayerPropertiesArgs arguments)
	dapi.DAPIResponse GetASN(1:UserDataGetASNArgs arguments)
	dapi.DAPIResponse GetASNInfo(1:UserDataGetASNInfoArgs arguments)
	dapi.DAPIResponse GetLikelyToShareFriends(1:UserDataGetLikelyToShareFriendsArgs arguments)
	dapi.DAPIResponse GetTopHelpers(1:UserDataGetTopHelpersArgs arguments)
	dapi.DAPIResponse GetRecommendedAppsWeb(1:UserDataGetRecommendedAppsWebArgs arguments)
	dapi.DAPIResponse GetRecommendedAppsMobile(1:UserDataGetRecommendedAppsMobileArgs arguments)
	dapi.DAPIResponse GetOutOfNetworkFriends(1:UserDataGetOutOfNetworkFriendsArgs arguments)
	dapi.DAPIResponse GetNonASN(1:UserDataGetNonASNArgs arguments)
	dapi.DAPIResponse GetOptimalGiftRecipients(1:UserDataGetOptimalGiftRecipientsArgs arguments)
	dapi.DAPIResponse GetOptimalInviteRecipients(1:UserDataGetOptimalInviteRecipientsArgs arguments)
	dapi.DAPIResponse GetASNOfFriends(1:UserDataGetASNOfFriendsArgs arguments)
	dapi.DAPIResponse GetLevel(1:UserDataGetLevelArgs arguments)
	dapi.DAPIResponse SetLevel(1:UserDataSetLevelArgs arguments)
	dapi.DAPIResponse GetVisit(1:UserDataGetVisitArgs arguments)
	dapi.DAPIResponse GetSmartSystemsBucketNumber(1:UserDataGetSmartSystemsBucketNumberArgs arguments)
	dapi.DAPIResponse GetPlayCount(1:UserDataGetPlayCountArgs arguments)
	dapi.DAPIResponse GetGamePlayHistory(1:UserDataGetGamePlayHistoryArgs arguments)
	dapi.DAPIResponse SetGamePlayResult(1:UserDataSetGamePlayResultArgs arguments)
	dapi.DAPIResponse SetVisit(1:UserDataSetVisitArgs arguments)
	dapi.DAPIResponse GetInstall(1:UserDataGetInstallArgs arguments)
	dapi.DAPIResponse SetInstall(1:UserDataSetInstallArgs arguments)
	dapi.DAPIResponse GetAchievement(1:UserDataGetAchievementArgs arguments)
	dapi.DAPIResponse SetAchievement(1:UserDataSetAchievementArgs arguments)
	dapi.DAPIResponse UpdateAchievement(1:UserDataUpdateAchievementArgs arguments)
	dapi.DAPIResponse GetPredictedPayerPercentile(1:UserDataGetPredictedPayerPercentileArgs arguments)
	dapi.DAPIResponse GetAppObject(1:UserDataGetAppObjectArgs arguments)
	dapi.DAPIResponse SetAppObject(1:UserDataSetAppObjectArgs arguments)
	dapi.DAPIResponse GetHashForZid(1:UserDataGetHashForZidArgs arguments)
	dapi.DAPIResponse GetZidForHash(1:UserDataGetZidForHashArgs arguments)
}
