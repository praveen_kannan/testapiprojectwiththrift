include "dapi.thrift"

struct ExternalSnGetFriendListArgs {
	1: string zid //The ZID to get information for
	2: string snId //The social network Id to get information for
	3: optional list<string> fields //a list of fields to return, zid is always included. Default name snUid, name, firstName, lastName, locale, lastLogin, appUser, dob, email, sex
	4: optional i32 limit //the max amount of friend ids to return, used for pagination
	5: optional i32 offset //the number of friend ids to skip, used for pagination
}

struct ExternalSnGetAppUsingFriendListArgs {
	1: string zid //The ZID to get information for
	2: string snId //The social network Id to get information for
	3: optional list<string> fields //a list of fields to return, zid is always included. Default name snUid, name, firstName, lastName, locale, lastLogin, appUser, dob, email, sex
	4: optional i32 limit //the max amount of friend ids to return, used for pagination
	5: optional i32 offset //the number of friend ids to skip, used for pagination
}

struct ExternalSnGetNonAppUsingFriendListArgs {
	1: string zid //The ZID to get information for
	2: string snId //The social network Id to get information for
	3: optional list<string> fields //a list of fields to return, zid is always included. Default name snUid, name, firstName, lastName, locale, lastLogin, appUser, dob, email, sex
	4: optional i32 limit //the max amount of friend ids to return, used for pagination
	5: optional i32 offset //the number of friend ids to skip, used for pagination
}

struct ExternalSnGetUserInfoListArgs {
	1: list<string> zids //A single zid or list of zids
	2: string snId //The social network Id to get information for
	3: optional list<string> fields //a list of fields to return, zid is always included. Default name snUid, name, firstName, lastName, locale, lastLogin, appUser, dob, email, sex
}

service ExternalSn {
	dapi.DAPIResponse GetFriendList(1:ExternalSnGetFriendListArgs arguments)
	dapi.DAPIResponse GetAppUsingFriendList(1:ExternalSnGetAppUsingFriendListArgs arguments)
	dapi.DAPIResponse GetNonAppUsingFriendList(1:ExternalSnGetNonAppUsingFriendListArgs arguments)
	dapi.DAPIResponse GetUserInfoList(1:ExternalSnGetUserInfoListArgs arguments)
}
