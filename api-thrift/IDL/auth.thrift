include "dapi.thrift"

struct AuthRegisterDeviceArgs
{
	1: string password // The password to register for the new device	
}

struct AuthIssueTokenArgs
{
	1: string password	// The password that matches the userId
	2: string appId	// The application ID to issue a token for. Optional and ignored in app auth context.
	3: optional string email	// The email address to issue a token for (public auth context)
	4: optional string username	// The username to issue a token for (public auth context)
	5: optional string userId	// The user id to issue an anonymous token for (public auth context)
	6: optional string zid	//The zid to issue a user token for (required for app auth context)
}

service Auth {
	dapi.DAPIResponse RegisterDevice(1:AuthRegisterDeviceArgs arguments)
	dapi.DAPIResponse IssueToken(1:AuthIssueTokenArgs arguments)
}