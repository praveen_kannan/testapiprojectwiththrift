#! /bin/sh

#######################################################################################
## build_all.sh
## Description: This script is a convenient script to build static library and framework 
## Usage: build_all.sh 
########################################################################################
rebuild=""
archive=0
while getopts "n:b:a" opt; do
	case $opt in
    n)
		NDKDIR=${OPTARG}
		;;
	b)
		rebuild=" -B"
		;;
	a)
		archive=1
		;;
	esac
done

getopts s: n

if [ -z "$NDKDIR" ] || [ ! -x "$NDKDIR/ndk-build" ]
then
	echo 
	echo "Android NDK directory $NDKDIR is invalid!"
	echo Specify Android NDK directory with -n /absolute/path/to/ndk
	echo 
	exit 1
fi

########################################################################################
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "\n*** Building ZyngaAPI-Thrift library and dependencies:"
pushd . > /dev/null 2>&1
cd ../../ZyngaAPI-Thrift/cpp/Android
"$NDKDIR"/ndk-build NDK_MODULE_PATH=${DIR}/../..${rebuild}

echo "*** Exporting ZyngaAPI-Thrift library"
mkdir -p ${DIR}/exported/ZyngaAPI-Thrift/
cp -v ./../../../boost/Android/libboost_atomic-gcc-mt-1_53.a ${DIR}/exported/ZyngaAPI-Thrift/ 
cp -v ./../../../boost/Android/libboost_system-gcc-mt-1_53.a ${DIR}/exported/ZyngaAPI-Thrift/ 
cp -v ./../../../boost/Android/libboost_thread-gcc-mt-1_53.a ${DIR}/exported/ZyngaAPI-Thrift/ 
cp -vr ./libs ${DIR}/exported/ZyngaAPI-Thrift/
cp -v ./obj/local/armeabi/libzyngaapi-thrift_static.a ${DIR}/exported/ZyngaAPI-Thrift/libs/armeabi/
cp -v ./obj/local/armeabi-v7a/libzyngaapi-thrift_static.a ${DIR}/exported/ZyngaAPI-Thrift/libs/armeabi-v7a/
cp -v ./obj/local/x86/libzyngaapi-thrift_static.a ${DIR}/exported/ZyngaAPI-Thrift/libs/x86/
popd > /dev/null 2>&1

# Build an archive with the exported library 
# (game teams will not be able to build this themselves without thrift)
if [ ${archive} -eq 1 ]
then
	echo "\n*** Building release archive"
	cd exported
	mkdir -p ../release
	tar cfvz ../release/ZyngaAPI-Thrift.tgz ./ZyngaAPI-Thrift > /dev/null 2>&1
fi

# No longer used -- the ZyngaAPI-Thrift module builds the thrift source code
# echo "*** Exporting thrift library"
# mkdir -p ${DIR}/exported/thrift/
# cd ../../thrift/cpp/Android/
# cp -v ./libs/armeabi/libthrift_shared.so ${DIR}/exported/thrift/
# cp -v ./obj/local/armeabi/libthrift_static.a ${DIR}/exported/thrift/
