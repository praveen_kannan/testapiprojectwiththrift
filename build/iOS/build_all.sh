#! /bin/sh

#######################################################################################
## build_all.sh
## Description: This script is a convenient script to build static library and framework 
## Usage: build_all.sh 
########################################################################################

########################################################################################
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "\n*** Building ZyngaAPI-Thrift framework and dependencies:"
pushd . > /dev/null 2>&1
cd ../../ZyngaAPI-Thrift/cpp/iOS
rm -rf build/*
./build_all.sh

echo "*** Exporting ZyngaAPI-Thrift framework"
mkdir -p ${DIR}/exported_debug/ZyngaAPI-Thrift/
rm -rf ${DIR}/exported_debug/ZyngaAPI-Thrift/ZyngaAPI-Thrift.framework

mkdir -p ${DIR}/exported_release/ZyngaAPI-Thrift/
rm -rf ${DIR}/exported_release/ZyngaAPI-Thrift/ZyngaAPI-Thrift.framework

cd build/Debug/internal-frameworks/
pax -wr ZyngaAPI-Thrift.framework ${DIR}/exported_debug/ZyngaAPI-Thrift/

cd ../../Release/internal-frameworks/
pax -wr ZyngaAPI-Thrift.framework ${DIR}/exported_release/ZyngaAPI-Thrift/

popd > /dev/null 2>&1
pushd . > /dev/null 2>&1

echo "*** Exporting thrift framework"
mkdir -p ${DIR}/exported_debug/thrift/
rm -rf ${DIR}/exported_debug/thrift/thrift.framework

mkdir -p ${DIR}/exported_release/thrift/
rm -rf ${DIR}/exported_release/thrift/thrift.framework

cd ../../thrift/cpp/iOS/build/Release/internal-frameworks/
pax -wr thrift.framework ${DIR}/exported_release/thrift/

cd ../../Debug/internal-frameworks/
pax -wr thrift.framework ${DIR}/exported_debug/thrift/

popd > /dev/null 2>&1

echo "*** Exporting openssl lib and includes"
mkdir -p ${DIR}/exported_debug/openssl/
rm -rf ${DIR}/exported_debug/openssl/include
mkdir -p ${DIR}/exported_debug/openssl/include/

mkdir -p ${DIR}/exported_release/openssl/
rm -rf ${DIR}/exported_release/openssl/include
mkdir -p ${DIR}/exported_release/openssl/include/

cd ../../openssl/iOS/lib/
pax -wr libssl.a ${DIR}/exported_debug/openssl
pax -wr libcrypto.a ${DIR}/exported_debug/openssl
pax -wr libssl.a ${DIR}/exported_release/openssl
pax -wr libcrypto.a ${DIR}/exported_release/openssl
cd ..
pax -wr include ${DIR}/exported_debug/openssl
pax -wr include ${DIR}/exported_release/openssl

echo "*** Exporting boost framework"
mkdir -p ${DIR}/exported_debug/boost/
mkdir -p ${DIR}/exported_release/boost/
cd ../../boost/iOS/
pax -wr boost.framework ${DIR}/exported_debug/boost
pax -wr boost.framework ${DIR}/exported_release/boost

echo "*** Building archive"
cd ${DIR}/exported_release
mkdir -p ${DIR}/release/
tar cfvz ${DIR}/release/ZyngaAPI-Thrift_release.tgz . > /dev/null 2>&1

cd ${DIR}/exported_debug
tar cfvz ${DIR}/release/ZyngaAPI-Thrift_debug.tgz . > /dev/null 2>&1

